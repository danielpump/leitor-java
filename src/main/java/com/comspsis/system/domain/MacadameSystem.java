/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 10/07/2014<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.comspsis.system.domain;

import com.compsis.macadame.domain.DomainSpecificEntity;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Representa um sistema. <br>
 * Ex: SGAP, autorizador, interop, CV, OSA
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 10/07/2014 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class MacadameSystem extends DomainSpecificEntity {
	
	/**
	 * Código para sistema CV
	 */
	public static final String CODE_CV = "CV";
	
	/**
	 * Código para sistema SGAP
	 */
	public static final String CODE_SGAP = "SGAP";
	
	/**
	 * Código para sistema Autorizador
	 */
	public static final String CODE_AUTORIZER = "AUTZD";
	
	/**
	 * Código para sistema Interop
	 */
	public static final String CODE_INTEROP = "INTEROP";
	
	
	
	/** 
	 * Construtor padrão da classe
	 */
	public MacadameSystem() {
	}
	
	/**
	 * Construtor alternativo da classe
	 * @param code
	 */
	public MacadameSystem(final String code) {
		super.setCode(code);
	}
}
