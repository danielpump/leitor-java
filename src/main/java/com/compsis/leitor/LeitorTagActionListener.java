/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto - leitor-java<br>
 *
 * Data de Criação: 19/03/2015<br>
 * <br>
 * Todos os direitos reservados.
 */
package com.compsis.leitor;

import java.io.BufferedWriter;
import java.io.IOException;

import javax.swing.JOptionPane;

import com.compsis.leitor.filter.Filter;
import com.compsis.leitor.filter.TagFilter;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO: Definir documentação da classe.<br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 19/03/2015 - @author ssano - Primeira versão da classe. <br>
 * <br>
 */
public class LeitorTagActionListener extends LeitorActionListener {
	protected LerMensagem createLeitor() {
		return new LerTags();
	}

	protected Filter createFilter(BufferedWriter bw){
		try {
			bw.write("Tags");
			bw.newLine();
			bw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String[] types = {"Tag Id", "Enviar tags", "Sequencial"};
		return createFilterForTags(types);
		
//		}else if(MessageTypes.Imagens.equals(tipo)){
//			bw.write(tipo);
//			bw.newLine();
//			
//			lerMensagem = new LerImagens();
//			filter = createFilterForImagens();
//		}
	}
	
	private static Filter createFilterForTags(String[] types) {
		TagFilter filter = null;
		String enteredValue;
		Integer choosedType = JOptionPane.showOptionDialog(null, "Quer filtrar por ?", "Leitor", JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, types, null);
		if("0".equals(choosedType.toString())){
			filter = new TagFilter();
			do{
				enteredValue = JOptionPane.showInputDialog("Insira o Tag Id: "); 
			}while("".equals(enteredValue));
			
			filter.setTagId(Long.valueOf(enteredValue));
			filter.setEnviarTag(false);
		} else if("1".equals(choosedType.toString())){
			filter = new TagFilter();
			String enteredValue2;
			String enteredValue3;
			do{
				enteredValue = JOptionPane.showInputDialog("Insira o sequencial: ");								
			}while("".equals(enteredValue));
			do{
				enteredValue2 = JOptionPane.showInputDialog("Insira o OSA ID: ");								
			}while("".equals(enteredValue));
			do{
				enteredValue3 = JOptionPane.showInputDialog("Nome da fila: ");								
			}while("".equals(enteredValue));
			
			filter.setTagId(Long.valueOf(enteredValue));
			filter.setOsaId(Integer.valueOf(enteredValue2));
			filter.setFila(enteredValue3);
			filter.setEnviarTag(true);
		} else if("2".equals(choosedType.toString())){
			filter = new TagFilter();
			do{
				enteredValue = JOptionPane.showInputDialog("Insira o sequencial: ");								
			}while("".equals(enteredValue));
			
			filter.setSequencial(Long.valueOf(enteredValue));
			filter.setEnviarTag(false);
		}
		
		return filter;
	}
}
