package com.compsis.leitor.filter;

public class ProcessedFilter extends Filter {

	private Long passageId;
	private Long tagId;
	private boolean enviarProcessadas;
	private Long sequencial;
	private String fila;

	public Long getPassageId() {
		return passageId;
	}

	public void setPassageId(Long passageId) {
		this.passageId = passageId;
	}

	public Long getTagId() {
		return tagId;
	}

	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}
	
	

	/**
	 * M�todo de recupera��o do campo enviarProcessadas
	 *
	 * @return valor do campo enviarProcessadas
	 */
	public boolean isEnviarProcessadas() {
		return enviarProcessadas;
	}

	/**
	 * Valor de enviarProcessadas atribu�do a enviarProcessadas
	 *
	 * @param enviarProcessadas Atributo da Classe
	 */
	public void setEnviarProcessadas(boolean enviarProcessadas) {
		this.enviarProcessadas = enviarProcessadas;
	}
	
	

	/**
	 * M�todo de recupera��o do campo sequencial
	 *
	 * @return valor do campo sequencial
	 */
	public Long getSequencial() {
		return sequencial;
	}

	/**
	 * Valor de sequencial atribu�do a sequencial
	 *
	 * @param sequencial Atributo da Classe
	 */
	public void setSequencial(Long sequencial) {
		this.sequencial = sequencial;
	}
	
	

	/**
	 * M�todo de recupera��o do campo fila
	 *
	 * @return valor do campo fila
	 */
	public String getFila() {
		return fila;
	}

	/**
	 * Valor de fila atribu�do a fila
	 *
	 * @param fila Atributo da Classe
	 */
	public void setFila(String fila) {
		this.fila = fila;
	}

	public ProcessedFilter clone(){
		ProcessedFilter filter = new ProcessedFilter();
		filter.setPath(this.getPath());
		filter.setPassageId(this.getPassageId());
		filter.setTagId(this.getTagId());
		filter.setOsaId(this.getOsaId());
		filter.setEnviarProcessadas(this.isEnviarProcessadas());
		filter.setSequencial(this.getSequencial());
		filter.setFila(this.getFila());
		return filter;
	}
	
}
