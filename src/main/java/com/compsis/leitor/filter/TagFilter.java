package com.compsis.leitor.filter;

public class TagFilter extends Filter {
	
	private Long tagId;
	private boolean enviarTag;
	private String fila;
	private Long sequencial;

	public Long getTagId() {
		return tagId;
	}

	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}
	

	public TagFilter clone(){
		TagFilter filter = new TagFilter();
		filter.setPath(this.getPath());
		filter.setTagId(this.getTagId());
		filter.setEnviarTag(this.isEnviarTag());
		filter.setOsaId(this.getOsaId());
		filter.setSequencial(this.getSequencial());
		return filter;
	}

	/**
	 * M�todo de recupera��o do campo enviarTag
	 *
	 * @return valor do campo enviarTag
	 */
	public boolean isEnviarTag() {
		return enviarTag;
	}

	/**
	 * Valor de enviarTag atribu�do a enviarTag
	 *
	 * @param enviarTag Atributo da Classe
	 */
	public void setEnviarTag(boolean enviarTag) {
		this.enviarTag = enviarTag;
	}

	/**
	 * M�todo de recupera��o do campo fila
	 *
	 * @return valor do campo fila
	 */
	public String getFila() {
		return fila;
	}

	/**
	 * Valor de fila atribu�do a fila
	 *
	 * @param fila Atributo da Classe
	 */
	public void setFila(String fila) {
		this.fila = fila;
	}

	/**
	 * M�todo de recupera��o do campo sequencial
	 *
	 * @return valor do campo sequencial
	 */
	public Long getSequencial() {
		return sequencial;
	}

	/**
	 * Valor de sequencial atribu�do a sequencial
	 *
	 * @param sequencial Atributo da Classe
	 */
	public void setSequencial(Long sequencial) {
		this.sequencial = sequencial;
	}

	
	
	
	
}
