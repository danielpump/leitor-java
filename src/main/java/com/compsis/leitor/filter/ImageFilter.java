package com.compsis.leitor.filter;

public class ImageFilter extends Filter {

	public ImageFilter clone(){
		ImageFilter filter = new ImageFilter();
		filter.setPath(this.getPath());
		filter.setOsaId(this.getOsaId());
		return filter;
	}
}
