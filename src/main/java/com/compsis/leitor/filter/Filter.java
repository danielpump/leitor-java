package com.compsis.leitor.filter;

public class Filter {

	private String path;
	private Integer osaId;
	
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public Integer getOsaId() {
		return osaId;
	}
	public void setOsaId(Integer osaId) {
		this.osaId = osaId;
	}
}
