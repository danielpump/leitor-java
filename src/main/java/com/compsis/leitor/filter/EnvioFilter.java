package com.compsis.leitor.filter;

public class EnvioFilter extends Filter {
	
	
	private String fila;
		

	public EnvioFilter clone(){
		EnvioFilter filter = new EnvioFilter();
		filter.setPath(this.getPath());
		filter.setOsaId(this.getOsaId());
		filter.setFila(this.getFila());
		return filter;
	}

	

	/**
	 * M�todo de recupera��o do campo fila
	 *
	 * @return valor do campo fila
	 */
	public String getFila() {
		return fila;
	}

	/**
	 * Valor de fila atribu�do a fila
	 *
	 * @param fila Atributo da Classe
	 */
	public void setFila(String fila) {
		this.fila = fila;
	}

	
	
	
}
