package com.compsis.leitor.filter;

public class PassageFilter extends Filter {
	
	private Long passageId;
	private Long tagId;
	private boolean breakMessage;

	public Long getPassageId() {
		return passageId;
	}

	public void setPassageId(Long passageId) {
		this.passageId = passageId;
	}

	public Long getTagId() {
		return tagId;
	}

	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}

	public PassageFilter clone(){
		PassageFilter filter = new PassageFilter();
		filter.setPath(this.getPath());
		filter.setPassageId(this.getPassageId());
		filter.setTagId(this.getTagId());
		filter.setBreakMessage(this.isBreakMessage());
		return filter;
	}

	/**
	 * M�todo de recupera��o do campo breakMessage
	 *
	 * @return valor do campo breakMessage
	 */
	public boolean isBreakMessage() {
		return breakMessage;
	}

	/**
	 * Valor de breakMessage atribu�do a breakMessage
	 *
	 * @param breakMessage Atributo da Classe
	 */
	public void setBreakMessage(boolean breakMessage) {
		this.breakMessage = breakMessage;
	}
	
	
	
}
