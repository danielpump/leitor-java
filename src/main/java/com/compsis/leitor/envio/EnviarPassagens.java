package com.compsis.leitor.envio;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import javax.jms.BytesMessage;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.vonbraunlabs.novoprotocolo.mensagemproto.PassagemProto.Passagem;
import org.vonbraunlabs.novoprotocolo.mensagemproto.TagProto.Tag;

import com.compsis.leitor.core.jms.ConnectionFactoryManager;

/**
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 25/02/2015 - @author Daniel Ferraz - Primeira versão da classe. <br>
 * <br>
 * <br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
public class EnviarPassagens {
	
	public static void main(String[] args) throws Exception {
		
		ObjectInputStream jmsStream = new ObjectInputStream(new GZIPInputStream(new FileInputStream("D:\\Servico\\Clientes\\ViaNorte\\TransacoesJMS\\1\\2015\\02\\20\\00\\1424397622754.jms")));		
		Map<String, Object> cabecalhoMensagem = (Map<String, Object>) jmsStream.readObject();
		byte[] mensagemProto = (byte[]) jmsStream.readObject();
		
		Passagem tag = Passagem.newBuilder().mergeFrom(mensagemProto).build();
		
		send(tag,  cabecalhoMensagem, "ETPTRANSACTIONS");
		
	}

	private static void send(final Passagem tag, final Map<String, Object> cabecalhoMensagem, String fila) {

		Map<String, Object> aParams = new HashMap<String, Object>();
		aParams.put("host", "CONCENTRADOR");
		aParams.put("port", "5445");
		ConnectionFactory connectionFactory = ConnectionFactoryManager
				.getConnectionFactory(aParams);
		JmsTemplate jmsTemplate = new JmsTemplate(connectionFactory);
		jmsTemplate.setDefaultDestinationName(fila);
		System.out.println(cabecalhoMensagem);
		MessageCreator messageCreator = new MessageCreator() {
			@Override
			public Message createMessage(Session session) throws JMSException {
				BytesMessage message = session.createBytesMessage();
				message.setStringProperty("pracaId", cabecalhoMensagem.get("pracaId").toString());
				message.setStringProperty("TYPE", "ERROR.ETP");
				message.setStringProperty("osaId", cabecalhoMensagem.get("osaId").toString());
				message.writeBytes(tag.toByteArray());
				return message;
			}
		};
		jmsTemplate.send(messageCreator);
		System.out.println("Enviado");
	}

}
