/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 20/02/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.leitor;

import java.io.BufferedWriter;
import java.util.HashMap;
import java.util.Map;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;

import org.springframework.jms.core.JmsTemplate;

import com.compsis.leitor.core.jms.ConnectionFactoryManager;
import com.compsis.leitor.filter.EnvioFilter;
import com.compsis.leitor.filter.Filter;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 20/02/2015 - @author Daniel Ferraz - Primeira vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class LerEnvio implements LerMensagem {
	
	

	@Override
	public void lerMensagem(Filter filter, BufferedWriter bw) throws Exception {
		

		Map<String, Object> aParams = new HashMap<String, Object>();
		aParams.put("host", "CONCENTRADOR");
		aParams.put("port", "5445");
		ConnectionFactory connectionFactory = ConnectionFactoryManager.getConnectionFactory(aParams);
		JmsTemplate jmsTemplate = new JmsTemplate(connectionFactory);
		jmsTemplate.setDefaultDestinationName(((EnvioFilter) filter).getFila());		
		Message message = jmsTemplate.receive();
		if( message !=null ) {
			bw.write("Destination: "+message.getJMSDestination());
			bw.newLine();
			bw.write("Data Postagem: "+message.getJMSTimestamp());
		}
	}
}
