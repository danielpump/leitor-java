/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto - leitor-java<br>
 *
 * Data de Criação: 19/03/2015<br>
 * <br>
 * Todos os direitos reservados.
 */
package com.compsis.leitor;

import java.io.BufferedWriter;
import java.io.IOException;

import com.compsis.leitor.filter.Filter;
import com.compsis.leitor.filter.ImageFilter;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO: Definir documentação da classe.<br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 19/03/2015 - @author ssano - Primeira versão da classe. <br>
 * <br>
 */
public class LeitorImagemActionListener extends LeitorActionListener {
	protected LerMensagem createLeitor() {
		return new LerImagens();
	}

	protected Filter createFilter(BufferedWriter bw){
		try {
			bw.write("Imagens");
			bw.newLine();
			bw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return createFilterForImagens();
	}
	
	private static Filter createFilterForImagens() {
		ImageFilter filter = new ImageFilter();
		return filter;
	}
}
