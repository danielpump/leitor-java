/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 20/02/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.leitor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.zip.GZIPInputStream;

import javax.jms.BytesMessage;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.vonbraunlabs.novoprotocolo.mensagemproto.ImagemProto;
import org.vonbraunlabs.novoprotocolo.mensagemproto.TagProto.Tag;
import org.vonbraunlabs.novoprotocolo.mensagemproto.TagProto.TagEntrada;

import com.compsis.leitor.core.jms.ConnectionFactoryManager;
import com.compsis.leitor.filter.Filter;
import com.compsis.leitor.filter.ImageFilter;
import com.compsis.leitor.filter.TagFilter;
import com.google.protobuf.InvalidProtocolBufferException;

/**
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Leitor de mensagens de imagens <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 18/03/2015 - @author ssano - Primeira vers�o da classe. <br>
 * <br>
 */
public class LerImagens implements LerMensagem {

	@Override
	public void lerMensagem(Filter filter, BufferedWriter bw) throws Exception {
		ImageFilter imageFilter = (ImageFilter) filter;

		bw.write("Iniciando leitura com parametros: ");
		bw.newLine();
		bw.write("Diretório raiz: " + imageFilter.getPath());
		bw.newLine();
		bw.flush();

		recursiveSearch(imageFilter, bw);

		bw.write("Finalizando leitura das imagens");
		bw.newLine();
	}

	private void recursiveSearch(ImageFilter imageFilter, BufferedWriter bw) 
			throws IOException, FileNotFoundException, ClassNotFoundException, InvalidProtocolBufferException {
		bw.write("Lendo mensagens do diretório: " + imageFilter.getPath());
		bw.newLine();
		bw.flush();

		for (File arquivoJMS : new File(imageFilter.getPath()).listFiles()) {
			if (arquivoJMS.isFile()) {
				read(imageFilter, bw, arquivoJMS);
			} else if (arquivoJMS.isDirectory()) {
				ImageFilter newFilter = imageFilter.clone();
				newFilter.setPath(arquivoJMS.getAbsolutePath());
				recursiveSearch(newFilter, bw);
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void read(ImageFilter imageFilter, BufferedWriter bw, File arquivoJMS) throws IOException {
		ObjectInputStream jmsStream = null;
		try {
			jmsStream = new ObjectInputStream(new GZIPInputStream(new FileInputStream(arquivoJMS)));

			Map<String, Object> header = (Map<String, Object>) jmsStream.readObject();
			byte[] mensagemProto = (byte[]) jmsStream.readObject();
			bw.write("Found message with " + mensagemProto.length + " bytes");
			bw.newLine();
			
			ImagemProto.Imagem image = ImagemProto.Imagem.newBuilder().mergeFrom(mensagemProto).build();
			
			writeImage(header, image, bw);
			bw.flush();
		} catch (Exception e) {
			bw.write("Erro Lendo arquivo: " + arquivoJMS.getName());
			bw.newLine();
			if (e.getMessage() != null) {
				bw.write(e.getMessage());
				bw.newLine();
			}
		} finally {
			if (jmsStream != null) {
				jmsStream.close();
			}
		}
	}

	private static void writeImage(Map<String, Object> header, ImagemProto.Imagem image, BufferedWriter bw) {
//		try {
//			bw.write("---------------------------------------Header--------------------------------");
//			bw.newLine();
//			for (String key : header.keySet()) {
//				bw.write(key + ": " + header.get(key));
//				bw.newLine();
//			}
//			bw.write("---------------------------------------Mensagem--------------------------------");
//			bw.newLine();
//			bw.write(image.toString());
//			bw.newLine();
//			bw.flush();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
	}
}
