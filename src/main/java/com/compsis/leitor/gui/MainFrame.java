/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto - leitor-java<br>
 *
 * Data de Criação: 19/03/2015<br>
 * <br>
 * Todos os direitos reservados.
 */
package com.compsis.leitor.gui;

import java.awt.FlowLayout;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

import com.compsis.leitor.LeitorImagemActionListener;
import com.compsis.leitor.LeitorPassagemActionListener;
import com.compsis.leitor.LeitorProcessadaActionListener;
import com.compsis.leitor.LeitorTagActionListener;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Frame com os botões de funcionalidades de busca<br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 19/03/2015 - @author ssano - Primeira versão da classe. <br>
 * <br>
 */
@SuppressWarnings("serial")
public class MainFrame extends JFrame {
	public MainFrame() {
		setTitle("Leitor");
		setLayout(new FlowLayout());
		URL res = getClass().getResource("/logo.png");
		if (res != null) {
			setIconImage(new ImageIcon(res).getImage());
		}
		
		initialize();
		pack();
		setVisible(true);
	}
	
	/**
	 * Enum com os tipos de mensagens
	 * */
	enum MessageTypes {
		Passagem,
		Processadas,
		Tags,
		Imagens;
		
		public boolean equals(Integer typeOrdinal){
			return Integer.valueOf(this.ordinal()).equals(typeOrdinal);
		}
	}
	
	private void initialize() {
		JButton btnPassagem = new JButton(MessageTypes.Passagem.toString());
		btnPassagem.addActionListener(new LeitorPassagemActionListener());
		add(btnPassagem);
		
		JButton btnProcessadas = new JButton(MessageTypes.Processadas.toString());
		btnProcessadas.addActionListener(new LeitorProcessadaActionListener());
		add(btnProcessadas);
		
		JButton btnTags = new JButton(MessageTypes.Tags.toString());
		btnTags.addActionListener(new LeitorTagActionListener());
		add(btnTags);
		
		JButton btnImagens = new JButton(MessageTypes.Imagens.toString());
		btnImagens.addActionListener(new LeitorImagemActionListener());
		add(btnImagens);
	}
}
