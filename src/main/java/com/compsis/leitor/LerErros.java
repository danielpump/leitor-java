/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 20/02/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.leitor;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import com.compsis.etp.register.TransactionRegister;
import com.compsis.leitor.filter.Filter;
import com.google.protobuf.InvalidProtocolBufferException;

/**
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 20/02/2015 - @author Daniel Ferraz - Primeira vers�o da classe. <br>
 * <br>
 * <br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class LerErros implements LerMensagem {

	public static void main(String[] args) throws Exception {
		new LerErros().lerMensagem(null, null);
	}

	@Override
	public void lerMensagem(Filter filter, BufferedWriter bw) throws Exception {

		
		File arquivoRaiz = new File(
				"D:\\Servico\\Clientes\\ViaRondon\\Erros\\ERROR\\ETP\\1\\2015");
		System.out.println("Come�ou");
		recursiveSearch(arquivoRaiz, bw);

		System.out.println("Lido");
	}

	private void recursiveSearch(File arquivo, BufferedWriter bw)
			throws IOException, FileNotFoundException, ClassNotFoundException,
			InvalidProtocolBufferException {

		try {
			for (File arquivoJMS : arquivo.listFiles()) {				
			if(arquivoJMS.isFile()){
				ObjectInputStream jmsStream = new ObjectInputStream(
						new GZIPInputStream(new FileInputStream(arquivoJMS)));
	
				Object header = jmsStream.readObject();
				Object ob = jmsStream.readObject();
	
				//System.out.println(header);
				byte[] byteObj = (byte[]) ob;
				ByteArrayInputStream in = new ByteArrayInputStream(byteObj);
			    ObjectInputStream is = new ObjectInputStream(in);
			    TransactionRegister object = (TransactionRegister) is.readObject();
				
				//System.out.println(object);
				//System.out.println(object.getTransactionMessage());
				//System.out.println(object.getTransaction().getInteropTransaction().getId());
			    System.out.println(object.getTransaction().getInteropTransaction().getInteropConfig().getMeanOfPayment());
			    System.out.println(object.getTransaction().getInteropTransaction().getMedia());
				if(object.getExceptionError() != null) object.getExceptionError().printStackTrace();
				//System.out.println(object.getErrorDate());
			} else {
				recursiveSearch( arquivoJMS,  bw);
			}
			
		}
		} catch (Exception e) {
			e.printStackTrace();
		}
		

	}
}
