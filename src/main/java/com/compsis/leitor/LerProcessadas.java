/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 20/02/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.leitor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.zip.GZIPInputStream;

import javax.jms.BytesMessage;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.vonbraunlabs.novoprotocolo.mensagemproto.PassagemProcessadaProto.PassagemProcessada;
import org.vonbraunlabs.novoprotocolo.mensagemproto.PassagemProcessadaProto.PassagemProcessada.PassagemEntrada;

import com.compsis.leitor.core.jms.ConnectionFactoryManager;
import com.compsis.leitor.filter.Filter;
import com.compsis.leitor.filter.ProcessedFilter;
import com.google.protobuf.InvalidProtocolBufferException;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 20/02/2015 - @author Daniel Ferraz - Primeira vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class LerProcessadas implements LerMensagem {
	
	TreeMap<Long, String> treeMap = new TreeMap<Long, String>();

	@Override
	public void lerMensagem(Filter filter, BufferedWriter bw) throws Exception {

		ProcessedFilter processedFilter = (ProcessedFilter) filter;
		
		bw.write("Iniciando leitura com parametros: ");
		bw.newLine();
		bw.write("Diret�rio raiz: " +  processedFilter.getPath());
		bw.newLine();
		if(processedFilter.getPassageId() != null && !"".equals(processedFilter.getPassageId())){
			bw.write("Passage ID: " + processedFilter.getPassageId());
			bw.newLine();
		}else if(processedFilter.getTagId() != null &&  !"".equals(processedFilter.getTagId())){
			bw.write("Tag ID: " + processedFilter.getTagId());
			bw.newLine();
		}else if(processedFilter.getOsaId() != null &&  !"".equals(processedFilter.getOsaId())){
			bw.write("Osa ID: " + processedFilter.getOsaId());
			bw.newLine();
		}
		bw.flush();
		
		recursiveSearch(processedFilter, bw);
		
		if(processedFilter.isEnviarProcessadas()){
			try{
				enviarComSequencial(processedFilter.getSequencial(), processedFilter.getOsaId(), processedFilter.getFila(), bw);
			} catch (Throwable e){
				//Orientado pelo Lucas Amancio
				bw.write("Erro no envio, o envio foi parado!");
				bw.write(">>>>>>>> O erro que ocorreu foi: " + e.getMessage());
				bw.newLine();
			}
		}
		
		bw.write("Finalizando leitura das passagens.");
		bw.newLine();
	}
	
	private void enviarComSequencial(Long sequentialId, Integer osaId, String fila, BufferedWriter bw) throws Exception {
		if(sequentialId==null) {
			sequentialId = 0L;
		}
		for (Entry<Long, String> entry : treeMap.entrySet()) {
			
			if(entry.getKey() >= sequentialId){
				ObjectInputStream jmsStream = new ObjectInputStream(new GZIPInputStream(new FileInputStream(new File(entry.getValue()))));
				
				Map<String, Object> cabecalhoMensagem = (Map<String, Object>) jmsStream.readObject();
				byte[] mensagemProto = (byte[]) jmsStream.readObject();
				
				PassagemProcessada processada = PassagemProcessada.newBuilder().mergeFrom(mensagemProto).build();
				
				if(processada.getOsaId() == osaId){
					bw.write("Enviando com sequencial: " + processada.getSequencial());
					bw.newLine();
					send(processada, bw, cabecalhoMensagem, fila);
					bw.write("Enviado mensagem com sequencial: " + processada.getSequencial());
					bw.newLine();
				}
			}
		}
	}
	
	
	private static void send(final PassagemProcessada tag, BufferedWriter bw, Map<String, Object> cabecalhoMensagem, String fila) {
		Map<String, Object> aParams = new HashMap<String, Object>();
		aParams.put("host", "CONCENTRADOR");
		aParams.put("port", "5445");
		ConnectionFactory connectionFactory = ConnectionFactoryManager.getConnectionFactory(aParams);
		JmsTemplate jmsTemplate = new JmsTemplate(connectionFactory);
		jmsTemplate.setDefaultDestinationName(fila);		
		
		MessageCreator messageCreator = new MessageCreator() {
			@Override
			public Message createMessage(Session session) throws JMSException {
				BytesMessage message = session.createBytesMessage();
				message.writeBytes(tag.toByteArray());
				return message;
			
			}
		};
		jmsTemplate.send(messageCreator);
		
	}
	
	private void recursiveSearch(ProcessedFilter processedFilter, BufferedWriter bw)
			throws IOException, FileNotFoundException, ClassNotFoundException, InvalidProtocolBufferException {
		
		bw.write("Lendo passagens do diret�rio: " + processedFilter.getPath());
		bw.newLine();
		bw.flush();
		
		for (File arquivoJMS : new File(processedFilter.getPath()).listFiles()) {
			
			if(arquivoJMS.isFile()){
				try{
					ObjectInputStream jmsStream = new ObjectInputStream(new GZIPInputStream(new FileInputStream(arquivoJMS)));
					
					Map<String, Object> cabecalhoMensagem = (Map<String, Object>) jmsStream.readObject();
					byte[] mensagemProto = (byte[]) jmsStream.readObject();
					
					PassagemProcessada passagem = PassagemProcessada.newBuilder().mergeFrom(mensagemProto).build();
									
					if(processedFilter.getPassageId() != null && !"".equals(processedFilter.getPassageId())  && processedFilter.isEnviarProcessadas() == false){
						lerComPassagemID(processedFilter.getPassageId(), passagem, bw, cabecalhoMensagem, arquivoJMS.getAbsolutePath());
					}else if(processedFilter.getOsaId() != null && !"".equals(processedFilter.getOsaId()) && processedFilter.isEnviarProcessadas() == false){
						lerComOsaID(processedFilter.getOsaId(), passagem, bw, cabecalhoMensagem, arquivoJMS.getAbsolutePath());
					} else if(processedFilter.isEnviarProcessadas() == true ){
						treeMap.put(passagem.getSequencial(), arquivoJMS.getAbsolutePath());
					}

				}catch(Exception e){
					bw.write("Erro Lendo arquivo: " + arquivoJMS.getName());
					bw.newLine();
					if(e.getMessage() != null){
						bw.write(e.getMessage());
						bw.newLine();
					}
				}
			}else if(arquivoJMS.isDirectory()){
				ProcessedFilter newFilter = processedFilter.clone();
				newFilter.setPath(arquivoJMS.getAbsolutePath());
				recursiveSearch(newFilter, bw);
			}
		}			
			
	}
	
	private static void lerComPassagemID(Long passageId, PassagemProcessada passagem, BufferedWriter bw, Map<String, Object> cabecalhoMensagem, String nomeArquivo) throws IOException {
		for (PassagemEntrada pEntrada : passagem.getPassagemList()) {
			if(pEntrada.getPassagemId() == passageId){
				bw.write("Encontrado no arquivo com nome: " + nomeArquivo);
				bw.newLine();
				write(passagem, bw, pEntrada, cabecalhoMensagem);
			}
		}
	}
	
	private static void lerComOsaID(Integer osaId, PassagemProcessada passagem, BufferedWriter bw, Map<String, Object> cabecalhoMensagem, String nomeArquivo) throws IOException {
		if(passagem.getOsaId() == osaId){
			bw.write("Encontrado no arquivo com nome: " + nomeArquivo);
			bw.newLine();
			writePassage(passagem, bw, cabecalhoMensagem);
		}	
	}
	
	private static void write(PassagemProcessada passagem, BufferedWriter bw,
			PassagemEntrada pEntrada, Map<String, Object> cabecalhoMensagem) {
		try {
			bw.write("---------------------------------------Passagem--------------------------------");
			bw.newLine();
			bw.write(pEntrada.toString());
			bw.newLine();
			bw.write("---------------------------------------Header--------------------------------");
			bw.newLine();
			for (String key : cabecalhoMensagem.keySet()) {
				bw.write(key + ": " + cabecalhoMensagem.get(key));
				bw.newLine();	
			}
			bw.write("---------------------------------------Mensagem--------------------------------");
			bw.newLine();
			bw.write(passagem.toString());
			bw.newLine();
			bw.flush();
		} catch (IOException e) {
			try {
				if(e.getMessage() != null){
					bw.write(passagem.toString());
					bw.newLine();
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	private static void writePassage(PassagemProcessada passagem, BufferedWriter bw, Map<String, Object> cabecalhoMensagem) {
		try {
			bw.write("---------------------------------------Header--------------------------------");
			bw.newLine();
			for (String key : cabecalhoMensagem.keySet()) {
				bw.write(key + ": " + cabecalhoMensagem.get(key));
				bw.newLine();	
			}
			bw.write("---------------------------------------Mensagem--------------------------------");
			bw.newLine();
			bw.write(passagem.toString());
			bw.newLine();
			bw.flush();
		} catch (IOException e) {
			try {
				if(e.getMessage() != null){
					bw.write(passagem.toString());
					bw.newLine();
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
}
