/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 20/02/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.leitor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.zip.GZIPInputStream;

import javax.jms.BytesMessage;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.vonbraunlabs.novoprotocolo.mensagemproto.TagProto.Tag;
import org.vonbraunlabs.novoprotocolo.mensagemproto.TagProto.TagEntrada;

import com.compsis.leitor.core.jms.ConnectionFactoryManager;
import com.compsis.leitor.filter.Filter;
import com.compsis.leitor.filter.TagFilter;
import com.google.protobuf.InvalidProtocolBufferException;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 20/02/2015 - @author Daniel Ferraz - Primeira vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class LerTags implements LerMensagem {
	TreeMap<Long, String> treeMap = new TreeMap<Long, String>();

	@Override
	public void lerMensagem(Filter filter, BufferedWriter bw) throws Exception {

		TagFilter tagFilter = (TagFilter) filter;
		
		bw.write("Iniciando leitura com parametros: ");
		bw.newLine();
		bw.write("Diret�rio raiz: " +  tagFilter.getPath());
		bw.newLine();
		if(tagFilter.getTagId() != null &&  !"".equals(tagFilter.getTagId())){
			bw.write("Tag ID: " + tagFilter.getTagId());
			bw.newLine();
		}
		bw.flush();
		
		recursiveSearch(tagFilter, bw);	
		
		if(tagFilter.isEnviarTag()){
			try{
				enviarComSequencial(tagFilter.getTagId(), tagFilter.getOsaId(), tagFilter.getFila(), bw);
			} catch (Throwable e){
				//Orientado pelo Lucas Amancio
				bw.write("Erro no envio, o envio foi parado!");
				bw.write(">>>>>>>> O erro que ocorreu foi: " + e.getMessage());
				bw.newLine();
			}
		}
		
		bw.write("Finalizando leitura das tags.");
		bw.newLine();
		
	}

	private void recursiveSearch(TagFilter tagFilter, BufferedWriter bw)
			throws IOException, FileNotFoundException, ClassNotFoundException, InvalidProtocolBufferException {
		
		bw.write("Lendo tags do diret�rio: " + tagFilter.getPath());
		bw.newLine();
		bw.flush();
		
		for (File arquivoJMS : new File(tagFilter.getPath()).listFiles()) {
			
			if(arquivoJMS.isFile()){
				try{
					ObjectInputStream jmsStream = new ObjectInputStream(new GZIPInputStream(new FileInputStream(arquivoJMS)));
					
					Map<String, Object> cabecalhoMensagem = (Map<String, Object>) jmsStream.readObject();
					byte[] mensagemProto = (byte[]) jmsStream.readObject();
					
					Tag tag = Tag.newBuilder().mergeFrom(mensagemProto).build();
									
					if(tagFilter.getTagId() != null && !"".equals(tagFilter.getTagId()) && tagFilter.isEnviarTag() == false ){
						lerComTagID(tagFilter.getTagId(), tag, bw, cabecalhoMensagem);
					} else if(tagFilter.getTagId() != null && !"".equals(tagFilter.getTagId()) && tagFilter.isEnviarTag() == true ){
						treeMap.put(tag.getSequencial(), arquivoJMS.getAbsolutePath());
					} else if(tagFilter.getSequencial() != null && tagFilter.isEnviarTag() == false ){
						lerComSequencial(tagFilter.getSequencial(), tag, bw, cabecalhoMensagem);
					}

				}catch(Exception e){
					bw.write("Erro Lendo arquivo: " + arquivoJMS.getName());
					bw.newLine();
					if(e.getMessage() != null){
						bw.write(e.getMessage());
						bw.newLine();
					}
				}
			}else if(arquivoJMS.isDirectory()){
				TagFilter newFilter = tagFilter.clone();
				newFilter.setPath(arquivoJMS.getAbsolutePath());
				recursiveSearch(newFilter, bw);
			}
		}			
			
	}
	
	private static void lerComTagID(Long tagId, Tag tag, BufferedWriter bw, Map<String, Object> cabecalhoMensagem) {
		for (TagEntrada tEntrada : tag.getTagList()) {
			if(tEntrada.getTagId() == tagId){
				write(tag, bw, tEntrada, cabecalhoMensagem);
			}
		}
	}
	
	private static void lerComSequencial(Long tagId, Tag tag, BufferedWriter bw, Map<String, Object> cabecalhoMensagem) {
		if(tag.getSequencial() == tagId){
			writeTag(tag, bw, cabecalhoMensagem);
		}	
	}
	
	@SuppressWarnings("unchecked")
	private void enviarComSequencial(Long sequentialId, Integer osaId, String fila, BufferedWriter bw) throws Exception {
		
		for (Entry<Long, String> entry : treeMap.entrySet()) {
			
			if(entry.getKey() >= sequentialId){
				ObjectInputStream jmsStream = new ObjectInputStream(new GZIPInputStream(new FileInputStream(new File(entry.getValue()))));
				
				Map<String, Object> cabecalhoMensagem = (Map<String, Object>) jmsStream.readObject();
				byte[] mensagemProto = (byte[]) jmsStream.readObject();
				
				Tag tag = Tag.newBuilder().mergeFrom(mensagemProto).build();
				
				if(tag.getOsaId() == osaId){
					bw.write("Enviando com sequencial: " + tag.getSequencial() + " e s�rie: " + tag.getSerie());
					bw.newLine();
					send(tag, bw, cabecalhoMensagem, fila);
					bw.write("Enviado mensagem com sequencial: " + tag.getSequencial() + " e s�rie: " + tag.getSerie());
					bw.newLine();
					bw.flush();
				}
			}
		}
	}
	
	private static void write(Tag tag, BufferedWriter bw, TagEntrada tEntrada, Map<String, Object> cabecalhoMensagem) {
		try {
			bw.write("---------------------------------------Header--------------------------------");
			bw.newLine();
			for (String key : cabecalhoMensagem.keySet()) {
				bw.write(key + ": " + cabecalhoMensagem.get(key));
				bw.newLine();	
			}
			bw.write("---------------------------------------Tag--------------------------------");
			bw.newLine();
			bw.write(tEntrada.toString());
			bw.newLine();
			bw.write("---------------------------------------Mensagem--------------------------------");
			bw.newLine();
			bw.write(tag.toString());
			bw.newLine();
			bw.flush();
		} catch (IOException e) {
			try {
				if(e.getMessage() != null){
					bw.write(tag.toString());
					bw.newLine();
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	private static void writeTag(Tag tag, BufferedWriter bw, Map<String, Object> cabecalhoMensagem) {
		try {
			bw.write("---------------------------------------Header--------------------------------");
			bw.newLine();
			for (String key : cabecalhoMensagem.keySet()) {
				bw.write(key + ": " + cabecalhoMensagem.get(key));
				bw.newLine();	
			}
			bw.write("---------------------------------------Mensagem--------------------------------");
			bw.newLine();
			bw.write(tag.toString());
			bw.newLine();
			bw.flush();
		} catch (IOException e) {
			try {
				if(e.getMessage() != null){
					bw.write(tag.toString());
					bw.newLine();
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	private static void send(final Tag tag, BufferedWriter bw, Map<String, Object> cabecalhoMensagem, String fila) {
		Map<String, Object> aParams = new HashMap<String, Object>();
		aParams.put("host", "CONCENTRADOR");
		aParams.put("port", "5445");
		ConnectionFactory connectionFactory = ConnectionFactoryManager.getConnectionFactory(aParams);
		JmsTemplate jmsTemplate = new JmsTemplate(connectionFactory);
		jmsTemplate.setDefaultDestinationName(fila);		
		
		MessageCreator messageCreator = new MessageCreator() {
			@Override
			public Message createMessage(Session session) throws JMSException {
				BytesMessage message = session.createBytesMessage();
				message.writeBytes(tag.toByteArray());
				return message;
			}
		};
		
		jmsTemplate.send(messageCreator);				
	}
}
