/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto ${product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 05/03/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.leitor.core.jms;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.jms.BytesMessage;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;

import org.apache.poi.util.IOUtils;
import org.springframework.jms.core.JmsTemplate;
import org.vonbraunlabs.novoprotocolo.mensagemproto.PassagemProto.Passagem;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 05/03/2015 - @author Lucas Israel - Primeira vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class ReceberMensagem {

	public static void receber(String fila) {
		Map<String, Object> aParams = new HashMap<String, Object>();
		aParams.put("host", "CONCENTRADOR");
		aParams.put("port", "5445");
		ConnectionFactory connectionFactory = ConnectionFactoryManager.getConnectionFactory(aParams);
		JmsTemplate jmsTemplate = new JmsTemplate(connectionFactory);
		jmsTemplate.setDefaultDestinationName(fila);		
		Message message = jmsTemplate.receive();
		if(message !=null) {
			
		}
	}
	
	public static void main(String[] args) throws Exception {
		File messageFile = new File("D:\\temp\\message\\message1425581975971.JMS2");
		FileInputStream stream = new FileInputStream(messageFile);
		byte[] array = IOUtils.toByteArray(stream);
		System.out.println(Passagem.newBuilder().mergeFrom(array).build());
	}
	
	public static void receber(String[] args) throws JMSException, Exception {
		
		Map<String, Object> aParams = new HashMap<String, Object>();
		aParams.put("host", "CONCENTRADOR");
		aParams.put("port", "5445");
		ConnectionFactory connectionFactory = ConnectionFactoryManager.getConnectionFactory(aParams);
		JmsTemplate jmsTemplate = new JmsTemplate(connectionFactory);
		jmsTemplate.setDefaultDestinationName(args[0]);		
		Message message = jmsTemplate.receive();
		if(message !=null) {
			if(message instanceof BytesMessage) {
				BytesMessage bm = (BytesMessage) message;
				byte [] b = new byte[(int) bm.getBodyLength()];
				bm.readBytes(b);
				
				File root = new File("d:\\temp\\msgs");
				root.mkdirs();
				File file = new File(root, "message"+System.currentTimeMillis()+".JMS2");
				FileOutputStream stream = new FileOutputStream(file);
				stream.write(b);
				stream.flush();
				stream.close();
			}
		}
	}
}
