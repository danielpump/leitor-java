/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto $(product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 11/06/2014<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.leitor.core.jms;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.jms.ConnectionFactory;

import org.hornetq.api.core.TransportConfiguration;
import org.hornetq.jms.client.HornetQJMSConnectionFactory;
import org.springframework.jms.connection.CachingConnectionFactory;

/**
 * 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Cria a {@link ConnectionFactory}
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 12/02/2015 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
public class ConnectionFactoryManager {
	private static final Map<String, ConnectionFactory> mapaConnectionFactory = Collections.synchronizedMap(new HashMap<String, ConnectionFactory>());
	
	public static ConnectionFactory getConnectionFactory(Map<String, Object> params) {
		ConnectionFactory connectionFactory =  null;
		synchronized (mapaConnectionFactory) {
			String key = new StringBuilder((String)params.get("host")).append((String)params.get("port")).toString();
			connectionFactory = mapaConnectionFactory.get(key);
			if(connectionFactory == null){
				HornetQJMSConnectionFactory hornetQJMSConnectionFactory = new HornetQJMSConnectionFactory(false, new TransportConfiguration("org.hornetq.core.remoting.impl.netty.NettyConnectorFactory", params));
				hornetQJMSConnectionFactory.setProducerMaxRate(-1);
				hornetQJMSConnectionFactory.setConsumerWindowSize(0);
				hornetQJMSConnectionFactory.setProducerWindowSize(1024*1024);
				hornetQJMSConnectionFactory.setConfirmationWindowSize(1024*1024);
				hornetQJMSConnectionFactory.setConnectionTTL(2 * 1000 * 60);
				hornetQJMSConnectionFactory.setClientFailureCheckPeriod(1000 * 60);
				hornetQJMSConnectionFactory.setReconnectAttempts(-1);
				
				CachingConnectionFactory cachingConnectionFactory = new CachingConnectionFactory(hornetQJMSConnectionFactory);
				cachingConnectionFactory.setClientId("AUTORIZADOR");
				cachingConnectionFactory.setReconnectOnException(true);
				cachingConnectionFactory.setSessionCacheSize(1);
				
				connectionFactory = cachingConnectionFactory;
				mapaConnectionFactory.put(key, connectionFactory);
			}			
		}
		
		return connectionFactory;
	}
}
