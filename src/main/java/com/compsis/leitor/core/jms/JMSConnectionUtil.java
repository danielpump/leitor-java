/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto ${product_name} - ${product_description}<br>
 *
 * Data de Criação: 24/12/2014<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.leitor.core.jms;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 24/12/2014 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class JMSConnectionUtil {
	/**
	 * Porta padrao para conexao JMS
	 */
	public static Integer PORTA_PADRAO = 5445;
	public final static Map<String, JMSConnection> JMS_CONNECTIONS = Collections.synchronizedMap(new HashMap<String, JMSConnection>());
	
	public synchronized static ConnectionFactory getConnectionFactory(final String host, final int port) {
		Map<String, Object> aParams = new HashMap<String, Object>();
		aParams.put("host", host);
		aParams.put("port", String.valueOf(port));

		ConnectionFactory connectionFactory = ConnectionFactoryManager.getConnectionFactory(aParams);
		return connectionFactory;
		
	}
	
	public static Connection criarConexao(final String host) throws JMSException {
		ConnectionFactory connectionFactory = JMSConnectionUtil.getConnectionFactory(host, JMSConnectionUtil.PORTA_PADRAO);
		Connection connection = connectionFactory.createConnection();
		connection.start();
		return connection;
	}
	
	
	public static JMSConnection criarJMSConnection(final String host, final String destino, final boolean isTopic) throws JMSException {
		String chave = host + destino;
		synchronized (JMS_CONNECTIONS) {
			JMSConnection jmsConnection = JMS_CONNECTIONS.get(chave);
			if(jmsConnection==null) {
				jmsConnection = new JMSConnection();
				jmsConnection.setChavePoolObjeto(chave);
				jmsConnection.setConnection(criarConexao(host));
				jmsConnection.setDestinationName(destino, isTopic); 			
			}
			return jmsConnection;			
		}
	}
	
}
