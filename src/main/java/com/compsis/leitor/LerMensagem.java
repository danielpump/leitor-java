/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 20/02/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.leitor;

import java.io.BufferedWriter;

import com.compsis.leitor.filter.Filter;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 20/02/2015 - @author Daniel Ferraz - Primeira vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
public interface LerMensagem {
	void lerMensagem(Filter filter, BufferedWriter bw) throws Exception;
}
