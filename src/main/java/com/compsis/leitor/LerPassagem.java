/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 20/02/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.leitor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import org.vonbraunlabs.novoprotocolo.mensagemproto.PassagemProto.Passagem;
import org.vonbraunlabs.novoprotocolo.mensagemproto.PassagemProto.PassagemEntrada;

import com.compsis.leitor.filter.Filter;
import com.compsis.leitor.filter.PassageFilter;
import com.google.protobuf.InvalidProtocolBufferException;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 20/02/2015 - @author Daniel Ferraz - Primeira vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class LerPassagem implements LerMensagem {

	@Override
	public void lerMensagem(Filter filter, BufferedWriter bw) throws Exception {

		PassageFilter passageFilter = (PassageFilter) filter;
		
		bw.write("Iniciando leitura com parametros: ");
		bw.newLine();
		bw.write("Diret�rio raiz: " + passageFilter.getPath());
		bw.newLine();
		if(passageFilter.getPassageId() != null &&  !"".equals(passageFilter.getPassageId())){
			bw.write("Passage ID: " + passageFilter.getPassageId());
			bw.newLine();
		}else if(passageFilter.getTagId() != null &&  !"".equals(passageFilter.getTagId())){
			bw.write("Tag ID: " + passageFilter.getTagId());
			bw.newLine();
		}else if(passageFilter.getOsaId() != null &&  !"".equals(passageFilter.getOsaId()) && passageFilter.isBreakMessage()){
			bw.write("Osa ID: " + passageFilter.getTagId());
			bw.newLine();
		}else if(passageFilter.getOsaId() != null &&  !"".equals(passageFilter.getOsaId())){
			bw.write("Osa ID: " + passageFilter.getTagId());
			bw.newLine();
		}
		bw.flush();
		
		recursiveSearch(passageFilter, bw);
		
		bw.write("Finalizando leitura das passagens.");
		bw.newLine();
		bw.flush();
	}
	
	@SuppressWarnings("unchecked")
	private void recursiveSearch(PassageFilter passageFilter, BufferedWriter bw)
			throws IOException, FileNotFoundException, ClassNotFoundException,
			InvalidProtocolBufferException {
		
		bw.write("Lendo passagens do diret�rio: " + passageFilter.getPath());
		bw.newLine();
		bw.flush();
		
		for (File arquivoJMS : new File(passageFilter.getPath()).listFiles()) {
			
			if(arquivoJMS.isFile()){
				try{
					ObjectInputStream jmsStream = new ObjectInputStream(new GZIPInputStream(new FileInputStream(arquivoJMS)));
					
					Map<String, Object> cabecalhoMensagem = (Map<String, Object>) jmsStream.readObject();
					byte[] mensagemProto = (byte[]) jmsStream.readObject();
					
					Passagem passagem = Passagem.newBuilder().mergeFrom(mensagemProto).build();
									
					if(passageFilter.getPassageId() != null && !"".equals(passageFilter.getPassageId())){
						lerComPassagemID(passageFilter.getPassageId(), passagem, bw, cabecalhoMensagem);
					}else if(passageFilter.getTagId() != null && !"".equals(passageFilter.getTagId())){
						lerComTagId(passageFilter.getTagId(), passagem, bw, cabecalhoMensagem);
					}else if(passageFilter.getOsaId() != null &&  !"".equals(passageFilter.getOsaId()) && passageFilter.isBreakMessage()){
						lerComOsaIDTrue(passageFilter.getOsaId(), passagem, bw, cabecalhoMensagem);
					}else if(passageFilter.getOsaId() != null && !"".equals(passageFilter.getOsaId())){
						lerComOsaID(passageFilter.getOsaId(), passagem, bw, cabecalhoMensagem);
					}
				}catch(Exception e){
					bw.write("Erro Lendo arquivo: " + arquivoJMS.getName());
					bw.newLine();
					if(e.getMessage() != null){
						bw.write(e.getMessage());
						bw.newLine();
					}
				}
			}else if(arquivoJMS.isDirectory()){
				PassageFilter newFilter = passageFilter.clone();
				newFilter.setPath(arquivoJMS.getAbsolutePath());
				recursiveSearch(newFilter, bw);
			}
		}			
			
	}
	
	private static void lerComTagId(Long tagId, Passagem passagem, BufferedWriter bw, Map<String, Object> cabecalhoMensagem) {				
			for (PassagemEntrada pEntrada : passagem.getPassagemList()) {
				if(pEntrada.getTagId() == tagId){
					write(passagem, bw, pEntrada, cabecalhoMensagem);
				}
			}
	}

	private static void lerComPassagemID(Long passageId, Passagem passagem, BufferedWriter bw, Map<String, Object> cabecalhoMensagem) {
					
			for (PassagemEntrada pEntrada : passagem.getPassagemList()) {
				if(pEntrada.getPassagemId() == passageId){
					write(passagem, bw, pEntrada, cabecalhoMensagem);
				}
			}
	}
	
	private static void lerComOsaIDTrue(Integer osaId, Passagem passagem, BufferedWriter bw, Map<String, Object> cabecalhoMensagem) {
		if(passagem.getOsaId() == osaId){
			writePassageBean(passagem, bw, cabecalhoMensagem);
		}
}
	
	private static void lerComOsaID(Integer osaId, Passagem passagem, BufferedWriter bw, Map<String, Object> cabecalhoMensagem) {
			if(passagem.getOsaId() == osaId){
				writePassage(passagem, bw, cabecalhoMensagem);
			}
}

	private static void write(Passagem passagem, BufferedWriter bw,
			PassagemEntrada pEntrada, Map<String, Object> cabecalhoMensagem) {
		try {
			bw.write("---------------------------------------Passagem--------------------------------");
			bw.newLine();
			bw.write(pEntrada.toString());
			bw.newLine();
			bw.write("---------------------------------------Header--------------------------------");
			bw.newLine();
			for (String key : cabecalhoMensagem.keySet()) {
				bw.write(key + ": " + cabecalhoMensagem.get(key));
				bw.newLine();	
			}
			bw.write("---------------------------------------Mensagem--------------------------------");
			bw.newLine();
			bw.write(passagem.toString());
			bw.newLine();
			bw.flush();
		} catch (IOException e) {
			try {
				if(e.getMessage() != null){
					bw.write(passagem.toString());
					bw.newLine();
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	private static void writePassageBean(Passagem passagem, BufferedWriter bw, Map<String, Object> cabecalhoMensagem) {
		File file = new File("D:\\beans\\bean" + passagem.getSerie() + "_" + passagem.getSequencial() + ".bin");
		FileOutputStream fileWriter = null;
		try {
			
			
			new File("D:\\beans").mkdirs();
			file.createNewFile();
			
			fileWriter = new FileOutputStream(file);
			
			fileWriter.write(passagem.toByteArray());
						
			fileWriter.flush();
			fileWriter.close();
		} catch (IOException e) {
			try {
				if(e.getMessage() != null){
					bw.write(passagem.toString());
					bw.newLine();
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}finally{
			if(fileWriter != null){
				try {
					fileWriter.flush();
					fileWriter.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}
		}
	}
	
	private static void writePassage(Passagem passagem, BufferedWriter bw, Map<String, Object> cabecalhoMensagem) {
		try {
			bw.write("---------------------------------------Header--------------------------------");
			bw.newLine();
			for (String key : cabecalhoMensagem.keySet()) {
				bw.write(key + ": " + cabecalhoMensagem.get(key));
				bw.newLine();	
			}
			bw.write("---------------------------------------Mensagem--------------------------------");
			bw.newLine();
			bw.write(passagem.toString());
			bw.newLine();
			bw.flush();
		} catch (IOException e) {
			try {
				if(e.getMessage() != null){
					bw.write(passagem.toString());
					bw.newLine();
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
}
