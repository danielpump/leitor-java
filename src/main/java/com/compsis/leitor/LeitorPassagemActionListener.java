/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto - leitor-java<br>
 *
 * Data de Criação: 23/03/2015<br>
 * <br>
 * Todos os direitos reservados.
 */
package com.compsis.leitor;

import java.io.BufferedWriter;
import java.io.IOException;

import javax.swing.JOptionPane;

import com.compsis.leitor.filter.Filter;
import com.compsis.leitor.filter.PassageFilter;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO: Definir documentação da classe.<br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 23/03/2015 - @author ssano - Primeira versão da classe. <br>
 * <br>
 * <br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
public class LeitorPassagemActionListener extends LeitorActionListener {

	@Override
	protected LerMensagem createLeitor() {
		return new LerPassagem();
	}

	@Override
	protected Filter createFilter(BufferedWriter bw) {
		try {
			bw.write("Passagem");
			bw.newLine();
			bw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}

		String enteredValue;
		String[] types = {"Passage Id", "Tag Id", "OSA Id","OSA Byte ID"};
		Integer choosedType = JOptionPane.showOptionDialog(null, "Quer filtrar por ?", "Leitor", JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, types, null);
		
		PassageFilter filter = null;
		
		if("0".equals(choosedType.toString())){
			filter = new PassageFilter();
			do{
				enteredValue = JOptionPane.showInputDialog("Insira o Passage Id: "); 
			}while("".equals(enteredValue));
			
			filter.setPassageId(Long.valueOf(enteredValue));
			
		}else if("1".equals(choosedType.toString())){
			filter = new PassageFilter();
			do{
				enteredValue = JOptionPane.showInputDialog("Insira o Tag Id: "); 
			}while("".equals(enteredValue));
			
			filter.setTagId(Long.valueOf(enteredValue));
		}else if("2".equals(choosedType.toString())){
			filter = new PassageFilter();
			enteredValue = JOptionPane.showInputDialog("Insira o OSA Id: ");
			filter.setOsaId(Integer.valueOf(enteredValue));
		}else if("3".equals(choosedType.toString())){
			filter = new PassageFilter();
			filter.setBreakMessage(true);
			enteredValue = JOptionPane.showInputDialog("Insira o OSA Id: ");
			filter.setOsaId(Integer.valueOf(enteredValue));
		}
		
		return filter;
	}
}
