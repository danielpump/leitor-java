/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto - leitor-java<br>
 *
 * Data de Criação: 19/03/2015<br>
 * <br>
 * Todos os direitos reservados.
 */
package com.compsis.leitor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import com.compsis.leitor.filter.Filter;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO: Definir documentação da classe.<br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 19/03/2015 - @author ssano - Primeira versão da classe. <br>
 * <br>
 */
public abstract class LeitorActionListener implements ActionListener {
	@Override
	public void actionPerformed(ActionEvent evt) {
		Filter filter = null;
		JFileChooser chooser = new JFileChooser();
		String sicatHome = System.getenv("SICAT_HOME");
		File file = null;
		File arquivo = null;
		if (sicatHome != null) {
			file = new File(sicatHome);
			arquivo = new File(sicatHome + "/resultado" + System.currentTimeMillis() + ".txt" );
		} else {
			file = new File("D:/");
			arquivo = new File(sicatHome + "/resultado" + System.currentTimeMillis() + ".txt" );
		}
		
		chooser.setCurrentDirectory(file);
		chooser.setDialogTitle("Selecione o diretório raiz:");
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		Integer returnVal = chooser.showOpenDialog(null);
		
		if(returnVal == JFileChooser.APPROVE_OPTION) {
			if(!arquivo.exists()){
				try {
					arquivo.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			FileWriter fw = null;
			BufferedWriter bw = null;
			try {
				fw = new FileWriter( arquivo );
				bw = new BufferedWriter( fw );
				
				LerMensagem lerMensagem = createLeitor();
				filter = createFilter(bw);
				
				if(lerMensagem != null && filter != null){
					filter.setPath(chooser.getSelectedFile().getAbsolutePath());
					lerMensagem.lerMensagem(filter, bw);
					bw.write("Busca finalizada");
					bw.newLine();
					JOptionPane.showMessageDialog(null, "Busca Finalizada com Sucesso");
				} else {
					bw.write("Busca cancelada");
					bw.newLine();
					JOptionPane.showMessageDialog(null, "Busca cancelada");
				}
				
			} catch (Throwable e) {
				if (bw != null) {
					for (StackTraceElement element : e.getStackTrace()) {
						try {
							bw.write(element.toString());
							bw.newLine();
						} catch (IOException e1) {
							e1.printStackTrace();
						}
					}
				}
				JOptionPane.showMessageDialog(null, "Erro no sistema, verifique o log");
			} finally {
				if(bw != null){
					try {
						bw.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if(fw != null){
					try {
						fw.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	abstract protected LerMensagem createLeitor();

	abstract protected Filter createFilter(BufferedWriter bw);
}
