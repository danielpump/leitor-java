/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto ${product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 06/03/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.utilitario;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jms.BytesMessage;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.vonbraunlabs.novoprotocolo.mensagemproto.PassagemProcessadaProto.PassagemProcessada;

import com.compsis.leitor.core.jms.ConnectionFactoryManager;
import com.compsis.macadame.util.Strings;
import com.compsis.utilitario.protos.TxtToProcessadas;
import com.compsis.utilitario.protos.TxtToProcessadasCGMP2;
import com.compsis.utilitario.protos.TxtToProcessadasCGMP3;
import com.compsis.utilitario.protos.TxtToProcessadasCGMP4;
import com.compsis.utilitario.protos.TxtToProcessadasConectcar2;
import com.compsis.utilitario.protos.TxtToProcessadasDBTrans;
import com.compsis.utilitario.protos.TxtToProcessadasDBTrans2;
import com.compsis.utilitario.protos.TxtToProcessadasDBTrans3;
import com.compsis.utilitario.protos.TxtToProcessadasPSIS;
import com.compsis.utilitario.protos.TxtToProcessadasVonBraun;
import com.compsis.utilitario.protos.TxtToProcessadasVonBraun2;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 06/03/2015 - @author Lucas Israel - Primeira vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class ExecutorLoadProtoProcessadas extends Executor {

	@Override
	public void executar(Map<String, String> parametros) {
		TxtToProcessadas txtProcessadas = null;
		String path = parametros.get("path");
		String pendentes = parametros.get("pendentes");
		String fila = parametros.get("fila");
		String adapter = parametros.get("adapter");
		if(Strings.isNullOrEmpty(path)) {
			System.err.println("Arquivo nao definido corretamente. Definir parametro path=[arquivo]");
			return;
		}
		if(Strings.isNullOrEmpty(fila)) {
			System.err.println("Fila nao definida corretamente. Definir fila para realizar o envio. Parametro fila=[NOME_FILA]");
			return;
		}
		if(Strings.isNullOrEmpty(adapter) || "cgmp".equals(adapter)) {
			txtProcessadas = new TxtToProcessadas();
		} else if(Strings.isNullOrEmpty(adapter) || "cgmp2".equals(adapter)) {
			txtProcessadas = new TxtToProcessadasCGMP2();
		} else if(Strings.isNullOrEmpty(adapter) || "cgmp3".equals(adapter)) {
			txtProcessadas = new TxtToProcessadasCGMP3();
		} else if(Strings.isNullOrEmpty(adapter) || "cgmp4".equals(adapter)) {
			txtProcessadas = new TxtToProcessadasCGMP4();
		} else if(Strings.isNullOrEmpty(adapter) || "psis".equals(adapter)) {
			txtProcessadas = new TxtToProcessadasPSIS();
		} else if("dbtrans".equals(adapter)){
			txtProcessadas = new TxtToProcessadasDBTrans();
		} else if("dbtrans2".equals(adapter)){
			txtProcessadas = new TxtToProcessadasDBTrans2();
		} else if("dbtrans3".equals(adapter)){
			txtProcessadas = new TxtToProcessadasDBTrans3();
		} else if("cc2".equals(adapter)){
			txtProcessadas = new TxtToProcessadasConectcar2();
		} else if("vb".equals(adapter)){
			txtProcessadas = new TxtToProcessadasVonBraun();
		} else if("vb2".equals(adapter)){
			txtProcessadas = new TxtToProcessadasVonBraun2();
		}
		if(!Strings.isNullOrEmpty(pendentes)) {
			txtProcessadas.carregarPendentes(new File(pendentes));
		}
		File file = new File(path);
		txtProcessadas.setParametros(parametros);
		List<PassagemProcessada> processadas = txtProcessadas.adapter(file);
		
		
		//ENVIAR MENSAGENS
		Map<String, Object> aParams = new HashMap<String, Object>();
		aParams.put("host", "CONCENTRADOR");
		aParams.put("port", "5445");
		ConnectionFactory connectionFactory = ConnectionFactoryManager.getConnectionFactory(aParams);
		JmsTemplate jmsTemplate = new JmsTemplate(connectionFactory);
		jmsTemplate.setDefaultDestinationName(fila);		
		
		for (final PassagemProcessada passagemProcessada : processadas) {
			MessageCreator messageCreator = new MessageCreator() {
				@Override
				public Message createMessage(Session session) throws JMSException {
					BytesMessage message = session.createBytesMessage();
					message.writeBytes(passagemProcessada.toByteArray());
					return message;
					
				}};
				jmsTemplate.send(messageCreator);							
				System.out.println("Enviando mensagem com sequencial "+passagemProcessada.getSequencial()+" para "+fila);
		}
	}

	/** 
	 * TODO Descri��o do M�todo
	 * @see com.compsis.utilitario.Executor#carregarHelp()
	 */
	@Override
	protected void carregarHelp() {
		addHelpLine("path=[arquivo]			Arquivo a ser convertido em mensagens de PassagemProcessadas");
		addHelpLine("fila=[NOME_FILA]		Nome da fila para se enviar o conteudo adaptado");
	}
}
