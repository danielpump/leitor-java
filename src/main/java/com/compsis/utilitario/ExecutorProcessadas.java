/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto ${product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 05/03/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.utilitario;

import java.util.Map;

import com.compsis.leitor.LerProcessadas;
import com.compsis.leitor.filter.ProcessedFilter;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 05/03/2015 - @author Lucas Israel - Primeira vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class ExecutorProcessadas extends Executor {
	
	
	@Override
	public void executar(Map<String, String> parametros) {
		System.out.println("Tratando parametros: "+parametros);
		ProcessedFilter filter = adapter(parametros);
		LerProcessadas leitorProcessadas = new LerProcessadas();
		try {
			leitorProcessadas.lerMensagem(filter, getWriter());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	private ProcessedFilter adapter(Map<String, String> parametros) {
		ProcessedFilter filter = new ProcessedFilter();
		filter.setEnviarProcessadas(isValue(parametros, "enviar"));
		filter.setFila(parametros.get("fila"));
		filter.setOsaId(getInteger(parametros, "osa"));
		filter.setPassageId(getLong(parametros, "passagemid"));
		filter.setPath(parametros.get("path"));
		filter.setSequencial(getLong(parametros, "sequencial"));
		filter.setTagId(getLong(parametros, "tag"));
		return filter;
	}

	/** 
	 * TODO Descri��o do M�todo
	 * @see com.compsis.utilitario.Executor#carregarHelp()
	 */
	@Override
	protected void carregarHelp() {
		addHelpLine("enviar=true/false			Indica se vai ou nao enviar o conteudo filtrado para a fila");
		addHelpLine("fila=[NOME_FILA]			Nome da fila para se enviar o conteudo filtrado(Somente se enviar=true)");
		addHelpLine("osa=[osaId]				ID da OSA");
		addHelpLine("passagemid=[passagemId]	passagemId que se deseja filtrar");
		addHelpLine("path=[diretorio]			Arquivo a ser consultado ou diretorio para iniciar as consultas");
		addHelpLine("sequencial=[sequencial]	Sequencial que se deseja consultar");
		addHelpLine("tag=[tagId]				Identificacao do TAG que se deseja consultar");
	}
}
