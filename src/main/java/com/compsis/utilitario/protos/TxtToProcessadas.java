/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto ${product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 06/03/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.utilitario.protos;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.vonbraunlabs.novoprotocolo.mensagemproto.PassagemProcessadaProto.PassagemProcessada;
import org.vonbraunlabs.novoprotocolo.mensagemproto.PassagemProcessadaProto.PassagemProcessada.Builder;
import org.vonbraunlabs.novoprotocolo.mensagemproto.PassagemProcessadaProto.PassagemProcessada.PassagemEntrada;
import org.vonbraunlabs.novoprotocolo.mensagemproto.PassagemProcessadaProto.PassagemProcessada.PassagemEntrada.MotivoNaoComp;
import org.vonbraunlabs.novoprotocolo.mensagemproto.PassagemProcessadaProto.PassagemProcessada.PassagemEntrada.MotivoOutroValor;
import org.vonbraunlabs.novoprotocolo.mensagemproto.PassagemProcessadaProto.PassagemProcessada.PassagemEntrada.MotivoProv;
import org.vonbraunlabs.novoprotocolo.mensagemproto.PassagemProcessadaProto.PassagemProcessada.PassagemEntrada.Resultado;

import com.compsis.macadame.util.Strings;
import com.compsis.macadame.util.date.CalendarUtil;
import com.google.common.base.Charsets;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 06/03/2015 - @author Lucas Israel - Primeira vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class TxtToProcessadas {
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	protected static final Set<Long> PENDENTES = new HashSet<Long>();
	protected Map<String, String> parametros;
	
	public List<PassagemProcessada> adapter(File file) {
		List<PassagemProcessada> processadas = new ArrayList<PassagemProcessada>();
		Map<Long, List<String[]>> mensagens = transformarArquivo(file);
		for (Long numeroMensagem : mensagens.keySet()) {
			PassagemProcessada processada = adapter(mensagens.get(numeroMensagem));
			if(processada!=null) {
				processadas.add(processada);				
			}
		}
		
		return processadas;
	}
	
	protected PassagemProcessada adapter (List<String[]> registros) {
		Builder builder = PassagemProcessada.newBuilder();
		int concessionariaID=0;
		int osaID = 0;
		long sequencial = 0L;
		boolean executado = false;
		for (String[] registro : registros) {
			if(concessionariaID==0)
				concessionariaID = adapterConcessionaria(registro);
			if(osaID==0)
				osaID = adaterOsa(registro);
			if(sequencial==0l)
				sequencial = adapterSequencial(registro);
			
			long passagemId = adapterPassagemId(registro);
			if(!PENDENTES.isEmpty() && !PENDENTES.contains(passagemId)) {
				continue;
			}
			executado = true;
			PassagemEntrada.Builder passagemEntrada = PassagemEntrada.newBuilder()
				.setMotivoNaoComp(adapterMotivoNaoComp(registro))
				.setMotivoOutroValor(adapterMotivoOutroValor(registro))
				.setMotivoProv(adapterMotivoProv(registro))
//				.setPagamento(adapterPagamento(registro))
				.setPassagemId(passagemId)
				.setReenvio(adapterReenvio(registro))
				.setResultado(adapterResultado(registro))
				.setValePedagio(adapterFlagValePedagio(registro))
				.setValor(adapterValor(registro));
			
			if(Resultado.COMPENSADO.equals(passagemEntrada.getResultado())) {
				
				Long pagamento = adapterPagamento(registro);
				if(pagamento!=null) {
					passagemEntrada.setPagamento(pagamento.longValue());
				}				
			}
			
			builder.addPassagem(passagemEntrada);
		}
		if(!executado) {
			return null;
		}
		builder.setConcessionariaId(concessionariaID)
			.setOsaId(osaID)
			.setSequencial(sequencial);
		return builder.build();
	}
	
	protected int adaterOsa(String[] registro) {
		return Integer.parseInt(registro[INDEX.OSAID]);
	}

	public void carregarPendentes(File file) {
		try {
			List<String> lines = com.google.common.io.Files.readLines(file, Charsets.UTF_8);
			for (String line : lines) {
				PENDENTES.add(Long.valueOf(line));				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
			
	}
	
	protected long adapterSequencial(String[] registro) {
		return Long.parseLong(registro[INDEX.NUMERO_MENSAGEM]);
	}
	
	protected int adapterConcessionaria(String[] registro) {
		return Integer.parseInt(registro[INDEX.CONCESSIONARIAID]);
	}
	
	protected int adapterValor(String[] registro) {
		String valor = registro[INDEX.VALOR];
		valor = valor.replace(",", ".");
		int cents = new BigDecimal(valor).multiply(new BigDecimal(100)).intValue();
		return cents;
	}

	protected boolean adapterFlagValePedagio(String[] registro) {
		return "1".equals(registro[INDEX.VALEPEDAGIO]);
	}

	protected Resultado adapterResultado(String[] registro) {
		String resultadoStr = registro[INDEX.RESULTADO];
		return Resultado.valueOf(Integer.parseInt(resultadoStr));
	}

	protected int adapterReenvio(String[] registro) {
		return Integer.parseInt(registro[INDEX.REENVIO]);
	}

	protected long adapterPassagemId(String[] registro) {
		return Long.parseLong(registro[INDEX.PASSAGEMID]);
	}

	/** 
	 * Retorna a data de pagamento em segundos (Unix) baseado no registro
	 * @param registro
	 * @return Long
	 */
	protected Long adapterPagamento(String[] registro) {
		Long dataPagamento = null;
		String pagamentoStr = registro[INDEX.PAGAMENTO];
		
		if(!Strings.isNullOrEmpty(pagamentoStr)) {
			try {
				Date parse = DATE_FORMAT.parse(pagamentoStr);
				Calendar calendar = CalendarUtil.toCalendar(parse);
				dataPagamento = CalendarUtil.toUTCUnixTime(calendar);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		return dataPagamento;
	}
	
	
	/** 
	 * Retorna o enum de MotivoProvisionado baseado no registro
	 * @param registro
	 * @return MotivoProv
	 */
	protected MotivoProv adapterMotivoProv(String[] registro) {
		int motivo = 0;
		String motivoStr = registro[INDEX.MOTIVOPROV];
		if(!Strings.isNullOrEmpty(motivoStr)) {
			motivo = Integer.parseInt(motivoStr);
		}
		return MotivoProv.valueOf(motivo);
	}

	protected MotivoOutroValor adapterMotivoOutroValor(String[] registro) {
		int motivo = 0;
		String motivoStr = registro[INDEX.MOTIVOOUTROVALOR];
		if(!Strings.isNullOrEmpty(motivoStr)) {
			motivo = Integer.parseInt(motivoStr);
		}
		return MotivoOutroValor.valueOf(motivo);
	}

	protected MotivoNaoComp adapterMotivoNaoComp(String[] registro) {
		int motivo = 0;
		String motivoStr = registro[INDEX.MOTIVONAOCOMP];
		if(!Strings.isNullOrEmpty(motivoStr)) {
			motivo = Integer.parseInt(motivoStr);
		}
		return MotivoNaoComp.valueOf(motivo);
	}
	
	protected Map<Long, List<String[]>> transformarArquivo(File file) {
		Map<Long, List<String[]>> mensagens = new HashMap<Long, List<String[]>>();
		try {
			List<String> lines = com.google.common.io.Files.readLines(file, Charsets.US_ASCII);
			String line = lines.remove(0);
			System.out.println("Layout: "+line);
			while(!lines.isEmpty()) {
				line = lines.remove(0);
				String[] colunas = line.split(getDelimitadorArquivo());
				try{
					indexar(colunas, mensagens);					
				}catch(Exception e) {
					System.out.println(line);
					e.printStackTrace();
				}
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return mensagens;
	}
	
	protected void indexar(String[]colunas, Map<Long, List<String[]>> mensagens) {
		Long numeroMensagem = Long.valueOf(colunas[INDEX.NUMERO_MENSAGEM]);
		List<String[]> processadas = mensagens.get(numeroMensagem);
		if(processadas==null) {
			processadas = new ArrayList<String[]>();
			mensagens.put(numeroMensagem, processadas);
		}
		processadas.add(colunas);
	}
	
	protected String getDelimitadorArquivo() {
		return "\\|";
	}
	
	private static class INDEX {
		public static final int NUMERO_MENSAGEM = 0;
		public static final int DATA_ENVIO = 1;
		public static final int CONCESSIONARIAID = 2;
		public static final int OSAID = 3;
		public static final int SEQUENCIAL = 4;
		public static final int PASSAGEMID = 5;
		public static final int REENVIO = 6;
		public static final int RESULTADO = 7;
		public static final int MOTIVOOUTROVALOR = 8;
		public static final int MOTIVONAOCOMP = 9;
		public static final int MOTIVOPROV = 10;
		public static final int VALOR = 11;
		public static final int PAGAMENTO = 12;
		public static final int VALEPEDAGIO = 13;
	}

	protected String getValor(final String[] registros, int index) {
		if(registros == null) {
			return null;
		}
		if(registros.length > index) {
			return registros[index];
		}
		return null;
	}
	
	/**
	 * Valor de parametros atribu�do a parametros
	 *
	 * @param parametros Atributo da Classe
	 */
	public void setParametros(Map<String, String> parametros) {
		this.parametros = parametros;
	}
}
