/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto ${product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 27/04/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.utilitario.protos;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.vonbraunlabs.novoprotocolo.mensagemproto.PassagemProcessadaProto.PassagemProcessada.PassagemEntrada.MotivoNaoComp;
import org.vonbraunlabs.novoprotocolo.mensagemproto.PassagemProcessadaProto.PassagemProcessada.PassagemEntrada.MotivoOutroValor;
import org.vonbraunlabs.novoprotocolo.mensagemproto.PassagemProcessadaProto.PassagemProcessada.PassagemEntrada.MotivoProv;
import org.vonbraunlabs.novoprotocolo.mensagemproto.PassagemProcessadaProto.PassagemProcessada.PassagemEntrada.Resultado;

import com.compsis.macadame.util.Strings;
import com.compsis.macadame.util.date.CalendarUtil;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 27/04/2015 - @author Lucas Israel - Primeira vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class TxtToProcessadasVonBraun extends TxtToProcessadas {
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
	
	protected String getDelimitadorArquivo() {
		return "\t";
	}
	
	protected int adaterOsa(String[] registro) {
		String osaStr = getValor(registro, INDEX.OSAID);
		return Integer.parseInt(osaStr);
	}
	
	protected long adapterSequencial(String[] registro) {
		String sequencialStr = getValor(registro, INDEX.SEQUENCIAL);
		return Long.parseLong(sequencialStr);
	}
	
	protected int adapterConcessionaria(String[] registro) {
		String concessionariaStr = getValor(registro, INDEX.CONCESSIONARIAID);
		return Integer.valueOf(concessionariaStr);
	}
	
	protected int adapterValor(String[] registro) {
		String valor = getValor(registro, INDEX.VALOR);
		int cents = Integer.parseInt(valor);
		return cents;
	}

	protected boolean adapterFlagValePedagio(String[] registro) {
		String valePedagioStr = getValor(registro, INDEX.VALEPEDAGIO);
		return !Strings.isNullOrEmpty(valePedagioStr) && !"f".equals(valePedagioStr);
	}
	
	protected Resultado adapterResultado(String[] registro) {
		String resultadoStr = getValor(registro, INDEX.RESULTADO);
		int resultado = Integer.parseInt(resultadoStr);
		return Resultado.valueOf(resultado);
	}
	
	protected int adapterReenvio(String[] registro) {
		return Integer.parseInt(registro[INDEX.REENVIO]);
	}
	
	protected long adapterPassagemId(String[] registro) {
		return Long.parseLong(registro[INDEX.PASSAGEMID]);
	}
	/** 
	 * Retorna a data de pagamento em segundos (Unix) baseado no registro
	 * @param registro
	 * @return Long
	 */
	protected Long adapterPagamento(String[] registro) {
		Long dataPagamento = null;
		String pagamentoStr = getValor(registro, INDEX.PAGAMENTO);
		
		if(!Strings.isNullOrEmpty(pagamentoStr)) {
			try {
				Date parse = DATE_FORMAT.parse(pagamentoStr);
				Calendar calendar = CalendarUtil.toCalendar(parse);
				dataPagamento = CalendarUtil.toUTCUnixTime(calendar);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		return dataPagamento;
	}
	
	protected MotivoProv adapterMotivoProv(String[] registro) {
		String motivoProvStr = getValor(registro, INDEX.MOTIVOPROV);
		if(!Strings.isNullOrEmpty(motivoProvStr)) {
			return MotivoProv.valueOf(Integer.parseInt(motivoProvStr));
		}
		return MotivoProv.SEM_MOTIVO_PROV;
	}
	
	protected MotivoOutroValor adapterMotivoOutroValor(String[] registro) {
		String motivoOutroValorStr = getValor(registro, INDEX.MOTIVOOUTROVALOR);
		if(!Strings.isNullOrEmpty(motivoOutroValorStr)) {
			MotivoOutroValor.valueOf(Integer.parseInt(motivoOutroValorStr));
		}
		return MotivoOutroValor.SEM_MOTIVO_OUTRO_VALOR;
	}
	
	protected MotivoNaoComp adapterMotivoNaoComp(String[] registro) {
		String motivoNaoCompStr = getValor(registro, INDEX.MOTIVONAOCOMP);
		if(!Strings.isNullOrEmpty(motivoNaoCompStr)) {
			return MotivoNaoComp.valueOf(Integer.parseInt(motivoNaoCompStr));
		}
		
		return MotivoNaoComp.SEM_MOTIVO_NAO_COMP;
	}
	
	private static class INDEX {
		public static final int NUMERO_MENSAGEM = 0;
		public static final int DATA_ENVIO = 1;
		public static final int CONCESSIONARIAID = 2;
		public static final int OSAID = 3;
		public static final int SEQUENCIAL = 4;
		public static final int PASSAGEMID = 10;
		public static final int REENVIO = 11;
		public static final int RESULTADO = 12;
		public static final int MOTIVOOUTROVALOR = 13;
		public static final int MOTIVONAOCOMP = 14;
		public static final int MOTIVOPROV = 15;
		public static final int VALOR = 16;
		public static final int PAGAMENTO = 17;
		public static final int VALEPEDAGIO = 18;
	}
}
