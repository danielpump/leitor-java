/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto ${product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 27/04/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.utilitario.protos;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.vonbraunlabs.novoprotocolo.mensagemproto.PassagemProcessadaProto.PassagemProcessada.PassagemEntrada.MotivoNaoComp;
import org.vonbraunlabs.novoprotocolo.mensagemproto.PassagemProcessadaProto.PassagemProcessada.PassagemEntrada.MotivoOutroValor;
import org.vonbraunlabs.novoprotocolo.mensagemproto.PassagemProcessadaProto.PassagemProcessada.PassagemEntrada.MotivoProv;
import org.vonbraunlabs.novoprotocolo.mensagemproto.PassagemProcessadaProto.PassagemProcessada.PassagemEntrada.Resultado;

import com.compsis.macadame.util.Strings;
import com.compsis.macadame.util.date.CalendarUtil;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 27/04/2015 - @author Lucas Israel - Primeira vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class TxtToProcessadasCGMP2 extends TxtToProcessadas {
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
	
	protected String getDelimitadorArquivo() {
		return "\t";
	}
	
	protected void indexar(String[]colunas, Map<Long, List<String[]>> mensagens) {
		Long numeroMensagem = Long.valueOf(-1);
		List<String[]> processadas = mensagens.get(numeroMensagem);
		if(processadas==null) {
			processadas = new ArrayList<String[]>();
			mensagens.put(numeroMensagem, processadas);
		}
		processadas.add(colunas);
	}
	
	protected int adaterOsa(String[] registro) {
		return 1;
	}
	
	protected long adapterSequencial(String[] registro) {
		return -1L;
	}
	
	protected int adapterConcessionaria(String[] registro) {
		return Integer.valueOf(parametros.get("concessionaria"));
	}
	
	protected int adapterValor(String[] registro) {
		String valor = registro[INDEX.VALOR];
		valor = valor.replace(",", ".");
		int cents = new BigDecimal(valor).multiply(new BigDecimal(100)).intValue();
		return cents;
	}

	protected boolean adapterFlagValePedagio(String[] registro) {
		return false;
	}
	
	protected Resultado adapterResultado(String[] registro) {
		String resultadoStr = getValor(registro, INDEX.RESULTADO);
		int resultado = 0;
		if("ACEITA".equals(resultadoStr.toUpperCase()) || "COMPENSADA".equals(resultadoStr.toUpperCase())) {
			resultado = 1;
		} else if("REJEITADA".equals(resultadoStr)) {
			resultado = 3;			
		} else {
			//Rejeitada eh padrao
			resultado = 3;						
		}
		return Resultado.valueOf(resultado);
	}
	
	protected int adapterReenvio(String[] registro) {
		return Integer.parseInt(registro[INDEX.REENVIO]);
	}
	
	protected long adapterPassagemId(String[] registro) {
		return Long.parseLong(registro[INDEX.PASSAGEMID]);
	}
	/** 
	 * Retorna a data de pagamento em segundos (Unix) baseado no registro
	 * @param registro
	 * @return Long
	 */
	protected Long adapterPagamento(String[] registro) {
		Long dataPagamento = null;
		String pagamentoStr = getValor(registro, INDEX.PAGAMENTO);
		
		if(!Strings.isNullOrEmpty(pagamentoStr)) {
			try {
				Date parse = DATE_FORMAT.parse(pagamentoStr);
				Calendar calendar = CalendarUtil.toCalendar(parse);
				dataPagamento = CalendarUtil.toUTCUnixTime(calendar);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		return dataPagamento;
	}
	
	protected MotivoProv adapterMotivoProv(String[] registro) {
		return MotivoProv.SEM_MOTIVO_PROV;
	}
	
	protected MotivoOutroValor adapterMotivoOutroValor(String[] registro) {
		return MotivoOutroValor.SEM_MOTIVO_OUTRO_VALOR;
	}
	
	protected MotivoNaoComp adapterMotivoNaoComp(String[] registro) {
		String resultadoStr = getValor(registro, INDEX.RESULTADO);
		if("REJEITADA".equals(resultadoStr.toUpperCase())) {
			String valor = registro[INDEX.VALOR];
			if("0".equals(valor)) {
//				return MotivoNaoComp.TAG_BLOQUEADO;			
				return MotivoNaoComp.DADOS_INVALIDOS;			
			}
			return MotivoNaoComp.DADOS_INVALIDOS;			
		}
		
		return MotivoNaoComp.SEM_MOTIVO_NAO_COMP;
	}
	
	private static class INDEX {
		public static final int PASSAGEMID = 0;
		public static final int REENVIO = 1;
		public static final int RESULTADO = 2;
		public static final int PAGAMENTO = 3;
		public static final int VALOR = 4;
	}
}
