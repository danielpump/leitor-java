/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto ${product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 13/03/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.utilitario.protos;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.vonbraunlabs.novoprotocolo.mensagemproto.PassagemProcessadaProto.PassagemProcessada.PassagemEntrada.MotivoNaoComp;
import org.vonbraunlabs.novoprotocolo.mensagemproto.PassagemProcessadaProto.PassagemProcessada.PassagemEntrada.MotivoOutroValor;
import org.vonbraunlabs.novoprotocolo.mensagemproto.PassagemProcessadaProto.PassagemProcessada.PassagemEntrada.MotivoProv;
import org.vonbraunlabs.novoprotocolo.mensagemproto.PassagemProcessadaProto.PassagemProcessada.PassagemEntrada.Resultado;

import com.compsis.macadame.util.Strings;
import com.compsis.macadame.util.date.CalendarUtil;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 13/03/2015 - @author Lucas Israel - Primeira vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class TxtToProcessadasCGMP4 extends TxtToProcessadas {
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
	
	protected String getDelimitadorArquivo() {
		return "\t";
	}
	
	protected void indexar(String[]colunas, Map<Long, List<String[]>> mensagens) {
		
		Long numeroMensagem = adapterSequencial(colunas);
		List<String[]> processadas = mensagens.get(numeroMensagem);
		if(processadas==null) {
			processadas = new ArrayList<String[]>();
			mensagens.put(numeroMensagem, processadas);
		}
		processadas.add(colunas);			
		
	}
	
	protected int adaterOsa(String[] registro) {
		return Integer.parseInt(parametros.get("osa"));
	}

	protected long adapterSequencial(String[] registro) {
		return (((new Random().nextInt(20000)) + 100) * -1);
	}
	
	protected int adapterConcessionaria(String[] registro) {
		return Integer.valueOf(parametros.get("concessionaria"));
	}
	
	protected int adapterValor(String[] registro) {
		String valor = registro[INDEX.VALOR];
		valor = valor.replace(",", ".");
		int cents = new BigDecimal(valor).multiply(new BigDecimal(100)).intValue();
		return cents;
	}

	protected boolean adapterFlagValePedagio(String[] registro) {
		return "SIM".equals(getValor(registro, INDEX.VALEPEDAGIO));
	}

	protected Resultado adapterResultado(String[] registro) {
		String resultadoStr = getValor(registro, INDEX.RESULTADO);
		int resultado = 0;
		if("1".equals(resultadoStr.substring(0, resultadoStr.indexOf(" ")))) {
			resultado = 1;
		} else if("3".equals(resultadoStr.substring(0, resultadoStr.indexOf(" ")))) {
			resultado = 3;			
		} else {
			//Rejeitada eh padrao
			resultado = 3;						
		}
		return Resultado.valueOf(resultado);
	}

	protected int adapterReenvio(String[] registro) {
		return Integer.parseInt(registro[INDEX.REENVIO]);
	}

	protected long adapterPassagemId(String[] registro) {
		return Long.parseLong(registro[INDEX.PASSAGEMID]);
	}

	/** 
	 * Retorna a data de pagamento em segundos (Unix) baseado no registro
	 * @param registro
	 * @return Long
	 */
	protected Long adapterPagamento(String[] registro) {
		Long dataPagamento = null;
		String pagamentoStr = getValor(registro, INDEX.PAGAMENTO);
		
		if(!Strings.isNullOrEmpty(pagamentoStr)) {
			try {
				Date parse = DATE_FORMAT.parse(pagamentoStr);
				Calendar calendar = CalendarUtil.toCalendar(parse);
				dataPagamento = CalendarUtil.toUTCUnixTime(calendar);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		return dataPagamento;
	}
	
	
	/** 
	 * Retorna o enum de MotivoProvisionado baseado no registro
	 * @param registro
	 * @return MotivoProv
	 */
	protected MotivoProv adapterMotivoProv(String[] registro) {
		int motivo = 0;
		String motivoStr = getValor(registro, INDEX.MOTIVOPROV);
		if(!Strings.isNullOrEmpty(motivoStr)) {
			motivo = Integer.parseInt(motivoStr);
		}
		return MotivoProv.valueOf(motivo);
	}

	protected MotivoOutroValor adapterMotivoOutroValor(String[] registro) {
		int motivo = 0;
		String motivoStr = getValor(registro, INDEX.MOTIVOOUTROVALOR);
		if(!Strings.isNullOrEmpty(motivoStr)) {
			if("DESCONTO".equals(motivoStr.toUpperCase())) {
				return MotivoOutroValor.DESCONTO;
			}
			motivo = Integer.parseInt(motivoStr);
		}
		return MotivoOutroValor.valueOf(motivo);
	}

	protected MotivoNaoComp adapterMotivoNaoComp(String[] registro) {		
		String motivoStr = getValor( registro , INDEX.MOTIVONAOCOMP);
		if(!Strings.isNullOrEmpty(motivoStr.substring(0, motivoStr.indexOf(" ")))) {
			return MotivoNaoComp.valueOf(Integer.parseInt(motivoStr.substring(0, motivoStr.indexOf(" "))));
		}
		return MotivoNaoComp.valueOf(0);
	}
	
	private int parseMotivoNaoComp(String motivo) {
		motivo = motivo.trim().toLowerCase();
		if("tag bloqueado".equals(motivo)) {
			return 1;
		}
		if("pra�a bloqueada".equals(motivo)) {
			return 2;
		}
		if("isento".equals(motivo)) {
			return 3;
		}
		if("dados inv�lidos".equals(motivo)) {
			return 4;
		}
		if(motivo.contains("repetid")) {
			return 5;
		}
		if("passagem enviada fora do prazo".equals(motivo)) {
			return 6;
		}
		return 0;
	}
	
	private static class INDEX {
		public static final int NUMERO_MENSAGEM = 0;		
		public static final int PASSAGEMID = 1;
		public static final int REENVIO = 2;
		public static final int RESULTADO = 3;
		public static final int MOTIVOOUTROVALOR = 4;
		public static final int MOTIVONAOCOMP = 5;
		public static final int MOTIVOPROV = 6;
		public static final int VALOR = 7;
		public static final int PAGAMENTO = 8;
		public static final int VALEPEDAGIO = 9;
	}
}
