/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto ${product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 19/03/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.utilitario;

import org.vonbraunlabs.novoprotocolo.mensagemproto.PassagemProcessadaProto.PassagemProcessada;
import org.vonbraunlabs.novoprotocolo.mensagemproto.PassagemProcessadaProto.PassagemProcessada.PassagemEntrada;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 19/03/2015 - @author Lucas Israel - Primeira vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class Main2 {
	public static void main(String[] args) throws Exception {
		PassagemProcessada passagemProcessada = PassagemProcessada.newBuilder().addPassagem(PassagemEntrada.newBuilder().setReenvio(-8454)).build();
		System.out.println(passagemProcessada.getPassagemList().get(0));
		System.out.println(passagemProcessada.getPassagemList().get(0).getReenvio());
		System.out.println(passagemProcessada.toString());
		byte[] array = passagemProcessada.toByteArray();
		
		
		PassagemProcessada processada = PassagemProcessada.newBuilder().mergeFrom(array).build();
		
		System.out.println(processada);
		System.out.println(processada.getPassagemList().get(0));
		System.out.println(processada.getPassagemList().get(0).getReenvio());
		System.out.println(processada.toString());
	}
}
