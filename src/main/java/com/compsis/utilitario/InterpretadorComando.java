/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto ${product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 05/03/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.utilitario;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 05/03/2015 - @author Lucas Israel - Primeira vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class InterpretadorComando {
	private static final Map<String, Comando> COMANDOS = new HashMap<String, Comando>();
	static {
//		COMANDOS.put("passagem", new Comando("passagem", new ExecutorPassagem()));
		COMANDOS.put("processadas", new Comando("processadas", new ExecutorProcessadas()));
		COMANDOS.put("loadproto", new Comando("loadproto", new ExecutorLoadProtoProcessadas()));
	}
	
	public static void processarComando(String...args) throws Exception{
		String comandoChamado = args[0].trim().toLowerCase();
		
		if(Arrays.asList(new String[]{"help", "?", "ajuda"}).contains(comandoChamado)) {
			
			System.out.println("Comandos Disponiveis:");
			System.out.println(COMANDOS.keySet().toString());
			
		} else {
			
			Comando comando = COMANDOS.get(comandoChamado);
			if(!verificarPedidoAjuda(args, comando) ) {
				Map<String, String> parametros = montarParametros(args);
				//EXECUTOR
				comando . executar ( parametros )  ;
				
			}
			
		}
		System.out.println("Programa concluido!!!");
		System.out.println(Arrays.toString(args));
		System.out.println();System.out.println();
	}
	
	private static boolean verificarPedidoAjuda(String[] args, Comando comando) {
		if(args.length>1) {
			String help = args[1].trim().toLowerCase();
			if(Arrays.asList(new String[]{"help", "?", "ajuda"}).contains(help)) {
				System.out.println("===========================================");
				System.out.println("HELP DE "+comando.getComando());
				System.out.println("===========================================");
				System.out.println(comando.getHelp());
				System.out.println("===========================================");
				return true;
			}
		}
		return false;
	}

	private static Map<String, String> montarParametros(String...strings) {
		Map<String, String> parametros = new HashMap<String, String>();
		for (String string : strings) {
			String[] keyPair = string.split("=");
			String key = keyPair[0];
			String value = null;
			if(keyPair.length>1) {
				value = keyPair[1];
			}
			parametros.put(key.toLowerCase(), value);
		}
		return parametros;
	}
	
	public static void main(String[] args) {
		String a = null;
		try {
			throw new Exception();
		} catch(Exception e) {
			testeEx(e);			
		}
	}
	
	
	public static void testeEx(Exception e) {
		
		StringBuffer buffer = new StringBuffer("[TASK: ");
		if(e instanceof NullPointerException){
			buffer.append(NullPointerException.class.getName());			
		} else {
			buffer.append(e.getMessage());			
		}
		
		buffer.append(" - ");
		StackTraceElement[] stcs = null;
		if(e.getCause()!=null){
			stcs = e.getCause().getStackTrace();
		} else {
			stcs = e.getStackTrace();
		}
		StackTraceElement st = stcs[0];
		buffer.append(st.getClassName()).append(".").append(st.getMethodName()).append(":").append(st.getLineNumber()).append(" -> ");
		st = stcs[1];
		buffer.append(st.getClassName()).append(".").append(st.getMethodName()).append(":").append(st.getLineNumber());
		
	}
}
