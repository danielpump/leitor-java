/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto ${product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 05/03/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.utilitario;

import java.util.Map;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 05/03/2015 - @author Lucas Israel - Primeira vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class Comando {
	private String comando;
	private String help;
	private Executor executor;
	
	public Comando(String comando, Executor executor) {
		super();
		this.comando = comando;
		this.executor = executor;
		this.executor.comando = this;
		this.executor.carregarHelp();
	}
	
	public void executar(Map<String, String> parametros) {
		executor.executar(parametros);
	}
	
	/**
	 * M�todo de recupera��o do campo comando
	 *
	 * @return valor do campo comando
	 */
	public String getComando() {
		return comando;
	}
	/**
	 * Valor de comando atribu�do a comando
	 *
	 * @param comando Atributo da Classe
	 */
	public void setComando(String comando) {
		this.comando = comando;
	}
	/**
	 * M�todo de recupera��o do campo help
	 *
	 * @return valor do campo help
	 */
	public String getHelp() {
		return help;
	}
	/**
	 * Valor de help atribu�do a help
	 *
	 * @param help Atributo da Classe
	 */
	public void setHelp(String help) {
		this.help = help;
	}
	/**
	 * M�todo de recupera��o do campo executor
	 *
	 * @return valor do campo executor
	 */
	public Executor getExecutor() {
		return executor;
	}
	/**
	 * Valor de executor atribu�do a executor
	 *
	 * @param executor Atributo da Classe
	 */
	public void setExecutor(Executor executor) {
		this.executor = executor;
	}
	/** 
	 * TODO Descri��o do M�todo
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Comando [comando=").append(comando).append(", help=")
				.append(help).append(", executor=").append(executor)
				.append("]");
		return builder.toString();
	}

	public void addHelpLine(String line) {
		if(help==null) {
			help = line;
		} else {
			help += "\n"+line;
		}
	}
}
