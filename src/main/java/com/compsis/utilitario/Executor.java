/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto ${product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 05/03/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.utilitario;

import java.io.BufferedWriter;
import java.io.PrintWriter;
import java.util.Map;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 05/03/2015 - @author Lucas Israel - Primeira vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public abstract class Executor {
	Comando comando;
	
	public abstract void executar(Map<String, String> parametros);
	
	protected void addHelpLine(String line) {
		comando.addHelpLine(line);
	}
	
	protected boolean isValue(Map<String, String> parametros, String key) {
		return parametros.containsKey(key) && "true".equals(parametros.get(key));
	}
	
	protected Integer getInteger(Map<String, String> parametros, String key) {
		return parametros.containsKey(key) ? Integer.valueOf(parametros.get(key)) : null;
	}

	protected Long getLong(Map<String, String> parametros, String key) {
		return parametros.containsKey(key) ? Long.valueOf(parametros.get(key)) : null;
	}
	
	protected BufferedWriter getWriter() {
		return new BufferedWriter(new PrintWriter(System.out));
	}
	
	protected abstract void carregarHelp();
}
