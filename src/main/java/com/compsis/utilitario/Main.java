/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 06/02/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.utilitario;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import com.compsis.leitor.gui.MainFrame;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 06/02/2015 - @author Daniel Ferraz - Primeira vers�o da classe. <br>
 * <br>
 */
public class Main {
	public static void main(String[] args) throws Exception {
		if(args==null || args.length==0) {
			Runnable runnable = new Runnable() {
				@Override
				public void run() {
					MainFrame frame = new MainFrame();
					frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					frame.setVisible(true);
				}
			};
	    	SwingUtilities.invokeLater(runnable);
		} else {
			InterpretadorComando.processarComando(args);
		}
	}
}
