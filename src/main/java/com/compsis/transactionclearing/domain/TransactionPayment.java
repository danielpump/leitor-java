/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 20/05/2014<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.transactionclearing.domain;

import java.math.BigDecimal;
import java.util.Calendar;

import com.compsis.macadame.domain.MacadameEntity;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Representa o pagamento de uma transmição de transação. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 20/05/2014 - @author dferraz - Primeira versão da classe. <br>
 * 06/01/2015 - @author dcarvalho - Inserção do campo scheduleDayNumeric utilizado para geração do relatório 1506. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
@SuppressWarnings("serial")
public class TransactionPayment extends MacadameEntity {
	
	private Calendar paymentDay;
	private Calendar scheduledDay;
	private Long scheduledDayNumeric; /** Valor numérico de scheduledDay. Utilizado em relatório. */
	private BigDecimal value;
	
	/**
	 * Método de recuperação do campo paymentDay
	 *
	 * @return valor do campo paymentDay
	 */
	public Calendar getPaymentDay() {
		return paymentDay;
	}

	/**
	 * Valor de paymentDay atribuído a paymentDay
	 *
	 * @param paymentDay Atributo da Classe
	 */
	public void setPaymentDay(Calendar paymentDay) {
		this.paymentDay = paymentDay;
	}

	/**
	 * Método de recuperação do campo scheduledDay
	 *
	 * @return valor do campo scheduledDay
	 */
	public Calendar getScheduledDay() {
		return scheduledDay;
	}

	/**
	 * Valor de scheduledDay atribuído a scheduledDay
	 *
	 * @param scheduledDay Atributo da Classe
	 */
	public void setScheduledDay(Calendar scheduledDay) {
		this.scheduledDay = scheduledDay;
	}

	/**
	 * Método de recuperação do campo value
	 *
	 * @return valor do campo value
	 */
	public BigDecimal getValue() {
		return value;
	}

	/**
	 * Valor de value atribuído a value
	 *
	 * @param value Atributo da Classe
	 */
	public void setValue(BigDecimal value) {
		this.value = value;
	}

	/**
	 * Método de recuperação do campo scheduledDayNumeric
	 *
	 * @return valor do campo scheduledDayNumeric
	 */
	public Long getScheduledDayNumeric() {
		return scheduledDayNumeric;
	}

	/**
	 * Valor de scheduledDayNumeric atribuído a scheduledDayNumeric
	 *
	 * @param scheduledDayNumeric Atributo da Classe
	 */
	public void setScheduledDayNumeric(Long scheduledDayNumeric) {
		this.scheduledDayNumeric = scheduledDayNumeric;
	}
	
}
