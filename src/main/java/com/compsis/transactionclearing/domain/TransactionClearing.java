/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 20/05/2014<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.transactionclearing.domain;

import com.compsis.communication.domain.Communication;
import com.compsis.interop.domain.ReturnCode;
import com.compsis.interop.domain.Transaction;
import com.compsis.interop.domain.Clearing;
import com.compsis.interop.domain.TransactionStatus;
import com.compsis.macadame.collections.MacadameList;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Representa a compensação de uma transmissão de transação. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 20/05/2014 - @author dferraz - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
@SuppressWarnings("serial")
public class TransactionClearing extends Clearing {

		private TransactionStatus status;
		
		private ReturnCode returnCode;
		
		private Integer sentNumber;
		
		private TransactionPayment transactionPayment;
		
		/**
		 * Transação que será transmitida.
		 */
		private Transaction transaction;
		
		private MacadameList<Communication> communications;

		/**
		 * Método de recuperação do campo status
		 *
		 * @return valor do campo status
		 */
		public TransactionStatus getStatus() {
			return status;
		}

		/**
		 * Valor de status atribuído a status
		 *
		 * @param status Atributo da Classe
		 */
		public void setStatus(TransactionStatus status) {
			this.status = status;
		}

		/**
		 * Método de recuperação do campo returnCode
		 * @return valor do campo returnCode
		 */
		public ReturnCode getReturnCode() {
			return returnCode;
		}

		/**
		 * Valor de returnCode atribuído a returnCode
		 * @param returnCode Atributo da Classe
		 */
		public void setReturnCode(ReturnCode returnCode) {
			this.returnCode = returnCode;
		}

		/**
		 * Método de recuperação do campo sentNumber
		 * @return valor do campo sentNumber
		 */
		public Integer getSentNumber() {
			return sentNumber;
		}

		/**
		 * Valor de sentNumber atribuído a sentNumber
		 * @param sentNumber Atributo da Classe
		 */
		public void setSentNumber(Integer sentNumber) {
			this.sentNumber = sentNumber;
		}

		/**
		 * Método de recuperação do campo transactionPayment
		 * @return valor do campo transactionPayment
		 */
		public TransactionPayment getTransactionPayment() {
			return transactionPayment;
		}

		/**
		 * Valor de transactionPayment atribuído a transactionPayment
		 * @param transactionPayment Atributo da Classe
		 */
		public void setTransactionPayment(TransactionPayment transactionPayment) {
			this.transactionPayment = transactionPayment;
		}

		/**
		 * Método de recuperação do campo transaction
		 * @return valor do campo transaction
		 */
		public Transaction getTransaction() {
			return transaction;
		}

		/**
		 * Valor de transaction atribuído a transaction
		 * @param transaction Atributo da Classe
		 */
		public void setTransaction(Transaction transaction) {
			this.transaction = transaction;
		}

		/**
		 * Método de recuperação do campo communications
		 * @return valor do campo communications
		 */
		public MacadameList<Communication> getCommunications() {			
			return communications;
		}

		/**
		 * Valor de communications atribuído a communications
		 * @param communications Atributo da Classe
		 */
		public void setCommunications(MacadameList<Communication> communications) {
			this.communications = communications;
		}
		
		@Override
		public String getClearingCode() {
			if(getSentNumber() != null)
				return getSentNumber().toString();
			return "";
		}
		
}
