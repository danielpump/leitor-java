/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 24/06/2014<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.etp.exceptions;

import com.compsis.macadame.exception.MacadameControllerException;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Exception do ETP
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 24/06/2014 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class ETPException extends MacadameControllerException {

	/** 
	 * Construtor padrão da classe
	 */
	public ETPException(String aErrorMessage) {
		super(MACADAMEEXCEPTIONCODE.BUSINESS_RULES_ERROR, aErrorMessage);
	}
}
