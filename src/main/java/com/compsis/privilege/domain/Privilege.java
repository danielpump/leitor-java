/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto Interop - Domain<br>
 *
 * Data de Criação: 01/10/2013<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.privilege.domain;

import com.compsis.macadame.domain.DomainSpecificEntity;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Entidade representa que representa o privilégio de uma transação. <br>
 * O privilégio é utilizado na definição da tarifa de uma transação
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 01/10/2013 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class Privilege extends DomainSpecificEntity {
	private static final long serialVersionUID = 8380182572730302739L;

	/**
	 * Privilégio de isentado = -2
	 */
	public static final String EXEMPTED = "-2";
	
	/**
	 * Privilégio de isento = 15
	 */
	public static final String EXEMPT = "15";
	/**
	 * Privilégio pagante = 0
	 */
	public static final String PAYING = "0";
	/**
	 * Construtor padrão da classe
	 */
	public Privilege(){}
	
	/**
	 * Construtor alternativo da classe
	 * informando o código da entidade
	 * @param aCode
	 */
	public Privilege(final String aCode){
		super.setCode(aCode);
	}
}
