/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 17/10/2012<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.message.domain;

import java.util.Calendar;

import com.compsis.communication.domain.Communication;
import com.compsis.macadame.domain.DomainSpecificEntity;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Representa uma abstração da mensagem. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 17/10/2012 - @author ntrivedi - Primeiva vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class Message extends DomainSpecificEntity implements Communication {
	
	/**
	 * Define o tipo de mensagem que a entidade está representando
	 */
	private MessageType messageType;
	
	/**
	 * Sequencial da mensagem
	 */
	private Long sequential;	
	
	/**
	 * Numero serial da mensagem
	 */
	private Integer serialNumber;
	
	/**
	 * Data de envio ou recebimento mensagem
	 */
	private Calendar messageDate;
	
	public MessageType getMessageType() {
		return messageType;
	}

	public void setMessageType(MessageType messageType) {
		this.messageType = messageType;
	}

	/**
	 * Construtor padrão/alternativo da classe
	 */
	public Message() {
		super();
	}
	
	/**
	 * Construtor padrão/alternativo da classe
	 * @param code
	 */
	public Message(String code) {
		setCode(code);
	}

	/**
	 * Método de recuperação do campo sequential
	 *
	 * @return valor do campo sequential
	 */
	public Long getSequential() {
		return sequential;
	}

	/**
	 * Valor de sequential atribuído a sequential
	 *
	 * @param sequential Atributo da Classe
	 */
	public void setSequential(Long sequential) {
		this.sequential = sequential;
	}

	/**
	 * Método de recuperação do campo serialNumber
	 *
	 * @return valor do campo serialNumber
	 */
	public Integer getSerialNumber() {
		return serialNumber;
	}

	/**
	 * Valor de serialNumber atribuído a serialNumber
	 *
	 * @param serialNumber Atributo da Classe
	 */
	public void setSerialNumber(Integer serialNumber) {
		this.serialNumber = serialNumber;
	}

	/**
	 * Método de recuperação do campo messageDate
	 *
	 * @return valor do campo messageDate
	 */
	public Calendar getMessageDate() {
		return messageDate;
	}

	/**
	 * Valor de messageDate atribuído a messageDate
	 *
	 * @param messageDate Atributo da Classe
	 */
	public void setMessageDate(Calendar messageDate) {
		this.messageDate = messageDate;
	}
	
	

}
