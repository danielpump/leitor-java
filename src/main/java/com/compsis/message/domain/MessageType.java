/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * SICAT - MESSAGE<br>
 *
 * Data de Criação: 25/03/2013<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.message.domain;

import com.compsis.macadame.domain.DomainSpecificEntity;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Representa os tipos de mensagem de uma transmissão. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 25/03/2013 - @author wcarvalho - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class MessageType extends DomainSpecificEntity{
	
	/**	 * 
	 * Código do tipo de mensagem transação - CODE: <code>TRAN</code>
	 */
	public static final String TRANSACTION = "TRAN";	
	
	/**	 * 
	 * Código do tipo de mensagem imagem - CODE: <code>IMG</code>
	 */
	public static final String IMAGE = "IMG";
	
	/**	 * 
	 * Código do tipo de compensação de transação - CODE: <code>TRANCOMP</code>
	 */
	public static final String TRANSACTION_CLEARING = "TRANCOMP";
	
	/**	 * 
	 * Código do tipo de descontos - CODE: <code>DISCOUNT</code>
	 */
	public static final String DISCOUNTS = "DISCOUNT";
	
	/**	 
	 * Código do tipo de mensagem recebimento de TAG - CODE: <code>TAG</code>
	 */
	public static final String TAG = "TAG";
	
	/**	 
	 * Código do tipo de mensagem recebimento de requisição de imagem - CODE: <code>IMGRQ</code>
	 */
	public static final String IMAGE_REQUEST = "IMGRQ";
	
	
	public MessageType() {
	
	}
	public MessageType(String code) {
		this.setCode(code);
	}
}
