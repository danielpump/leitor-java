/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 16/07/2014<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interopmessage.domain;

import com.compsis.macadame.domain.DomainSpecificEntity;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Representa os motivos de requisição de imagem do protolo ARTESP. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 16/07/2014 - @author doug - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
@SuppressWarnings("serial")
public class ImageRequestReason extends DomainSpecificEntity{
	
	public static final String CONTESTATION = "CONT";	
	
	public static final String AUDITION = "AUDIT";	
	
	public ImageRequestReason() {
	}
	
	public ImageRequestReason(String code) {
		this.setCode(code);
	}
}
