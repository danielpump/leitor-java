/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 20/05/2014<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interopmessage.domain;

import com.compsis.image.domain.Image;
import com.compsis.macadame.collections.MacadameList;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Representa a mensagem do agrupamento de imagens que serão emitidas. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 20/05/2014 - @author dferraz - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
@SuppressWarnings("serial")
public class ImageMessage extends TransactionMessage {
					
	private MacadameList<Image> images;

	/**
	 * Método de recuperação do campo images
	 * @return valor do campo images
	 */
	public MacadameList<Image> getImages() {
		return images;
	}

	/**
	 * Valor de images atribuído a images
	 * @param images Atributo da Classe
	 */
	public void setImages(MacadameList<Image> images) {
		this.images = images;
	}
	
}
