/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 29/05/2014<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interopmessage.domain;

import com.compsis.interop.domain.AbstractInteropConfig;
import com.compsis.interop.domain.ExecutionDetail;
import com.compsis.macadame.collections.MacadameList;
import com.compsis.macadame.domain.MacadameEntity;
import com.compsis.macadame.domain.MacadameFile;
import com.compsis.message.domain.Message;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 29/05/2014 - @author dferraz - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class MessageExecutionDetail<M extends Message> extends ExecutionDetail {
	
	private AbstractInteropConfig interopConfig;
	
	private M message;
	
	/**
	 * Lista de Entidades utilizadas durante a execução da tarefa.
	 */
	private MacadameList<? extends MacadameEntity> entities;

	/**
	 * Método de recuperação do campo message
	 *
	 * @return valor do campo message
	 */
	public M getMessage() {
		return message;
	}

	/**
	 * Valor de message atribuído a message
	 *
	 * @param message Atributo da Classe
	 */
	public void setMessage(M message) {
		this.message = message;
	} 

	/**
	 * Valor de entities atribuído a entities
	 *
	 * @param entities Atributo da Classe
	 */
	public void setEntities(MacadameList<? extends MacadameEntity> entities) {
		this.entities = entities;
	}

	/**
	 * Método de recuperação do campo entities
	 *
	 * @return valor do campo entities
	 */
	@SuppressWarnings("unchecked")
	public <E extends MacadameEntity> MacadameList<E> getEntities() {
		return (MacadameList<E>) entities;
	}

	/**
	 * Método de recuperação do campo interopConfig
	 *
	 * @return valor do campo interopConfig
	 */
	public AbstractInteropConfig getInteropConfig() {
		return interopConfig;
	}

	/**
	 * Valor de interopConfig atribuído a interopConfig
	 *
	 * @param interopConfig Atributo da Classe
	 */
	public void setInteropConfig(AbstractInteropConfig interopConfig) {
		this.interopConfig = interopConfig;
	}

}
