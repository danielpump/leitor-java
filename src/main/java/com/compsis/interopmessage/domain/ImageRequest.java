/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 15/07/2014<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interopmessage.domain;

import com.compsis.interop.domain.Transaction;
import com.compsis.macadame.domain.MacadameEntity;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Representa os campos das mensagem de requisição de imagem. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 15/07/2014 - @author doug - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class ImageRequest extends MacadameEntity {
	private Transaction transaction;
	private ImageRequestReason imageRequestReason;
	private ImageRequestMessage imageRequestMessage;
	
	public ImageRequest() {}
	
	public ImageRequest(ImageRequestMessage imageRequestMessage) {
		this.imageRequestMessage = imageRequestMessage;
		this.imageRequestMessage.setImageRequest(this);
	}
	
	/**
	 * Método de recuperação do campo transaction
	 * @return valor do campo transaction
	 */
	public Transaction getTransaction() {
		return transaction;
	}
	/**
	 * Valor de transaction atribuído a transaction
	 * @param transaction Atributo da Classe
	 */
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}
	/**
	 * Método de recuperação do campo imageRequestReason
	 * @return valor do campo imageRequestReason
	 */
	public ImageRequestReason getImageRequestReason() {
		return imageRequestReason;
	}
	/**
	 * Valor de imageRequestReason atribuído a imageRequestReason
	 * @param imageRequestReason Atributo da Classe
	 */
	public void setImageRequestReason(ImageRequestReason imageRequestReason) {
		this.imageRequestReason = imageRequestReason;
	}
	/**
	 * Método de recuperação do campo imageRequestMessage
	 * @return valor do campo imageRequestMessage
	 */
	public ImageRequestMessage getImageRequestMessage() {
		return imageRequestMessage;
	}
	/**
	 * Valor de imageRequestMessage atribuído a imageRequestMessage
	 * @param imageRequestMessage Atributo da Classe
	 */
	public void setImageRequestMessage(ImageRequestMessage imageRequestMessage) {
		this.imageRequestMessage = imageRequestMessage;
	}
}
