/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 20/05/2014<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interopmessage.domain;

import com.compsis.interop.domain.Transaction;
import com.compsis.macadame.collections.MacadameList;
import com.compsis.message.domain.Message;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Representa a mensagem do agrupamento de transações que serão emitidas. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 20/05/2014 - @author dferraz - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
@SuppressWarnings("serial")
public class TransactionMessage extends Message {
	private boolean reprocessing = false;
	private MacadameList<Transaction> transactions;

	/**
	 * Método de recuperação do campo transactions
	 * @return valor do campo transactions
	 */
	public MacadameList<Transaction> getTransactions() {
		return transactions;
	}

	/**
	 * Valor de transactions atribuído a transactions
	 * @param transactions Atributo da Classe
	 */
	public void setTransactions(MacadameList<Transaction> transactions) {
		this.transactions = transactions;
	}
	
	/**
	 * Método de recuperação do campo reprocessing
	 * @return valor do campo reprocessing
	 */
	public boolean isReprocessing() {
		return reprocessing;
	}
	
	public void notifyReprocessing() {
		reprocessing = true;
	}
}
