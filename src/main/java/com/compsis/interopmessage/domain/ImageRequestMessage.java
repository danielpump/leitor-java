/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 15/07/2014<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interopmessage.domain;

import com.compsis.message.domain.Message;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Representa a mensagem do de requisição de imagem que foi recebido. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 15/07/2014 - @author doug - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
@SuppressWarnings("serial")
public class ImageRequestMessage extends Message{
	
	private ImageRequest imageRequest;
	
	
	/**
	 * Método de recuperação do campo imageRequest
	 * @return valor do campo imageRequest
	 */
	public ImageRequest getImageRequest() {
		return imageRequest;
	}

	/**
	 * Valor de imageRequest atribuído a imageRequest
	 * @param imageRequest Atributo da Classe
	 */
	public void setImageRequest(ImageRequest imageRequest) {
		this.imageRequest = imageRequest;
	}
		
}