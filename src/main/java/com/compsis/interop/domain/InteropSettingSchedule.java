/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto Interop - Domain<br>
 *
 * Data de Criação: 21/01/2013<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import java.util.Calendar;

import com.compsis.macadame.collections.Collections;
import com.compsis.macadame.collections.MacadameArrayList;
import com.compsis.macadame.collections.MacadameList;
import com.compsis.macadame.domain.MacadameEntity;
import com.compsis.operator.domain.OperatorDetailToll;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Classe de domínio para 'TITP_INTEROPSETTINGSCHEDULE'.
 * A finalidade desta classe é manter histórico de alterações da InteropSetting e agendamento de configurações
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 21/01/2013 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class InteropSettingSchedule extends MacadameEntity implements Cloneable{
	private AbstractInteropConfig interopConfig;

	/**
	 * Operador do SICAT responsável pela alteração
	 */
	private OperatorDetailToll operator;
	
	/**
	 * Data inicio da vigencia
	 */
	private Calendar effectiveDate;
	
	/**
	 * Configurações deste schedule
	 */
	private MacadameList<InteropSetting> settings;

	/**
	 * Método de recuperação do campo operator
	 *
	 * @return valor do campo operator
	 */
	public OperatorDetailToll getOperator() {
		return operator;
	}

	/**
	 * Valor de operator atribuído a operator
	 *
	 * @param operator Atributo da Classe
	 */
	public void setOperator(OperatorDetailToll operator) {
		this.operator = operator;
	}

	/**
	 * Método de recuperação do campo effectiveDate
	 *
	 * @return valor do campo effectiveDate
	 */
	public Calendar getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * Valor de scheduleDate atribuído a effectiveDate
	 *
	 * @param scheduleDate Atributo da Classe
	 */
	public void setEffectiveDate(Calendar effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	/**
	 * Método de recuperação do campo settings
	 *
	 * @return valor do campo settings
	 */
	public MacadameList<InteropSetting> getSettings() {
		if(settings == null){
			settings = Collections.newArrayList();
		}
		return settings;
	}

	/**
	 * Valor de settings atribuído a settings
	 *
	 * @param settings Atributo da Classe
	 */
	public void setSettings(MacadameList<InteropSetting> settings) {
		this.settings = settings;
	}
	
	/**
	 * Valor de interopConfig atribuído a interopConfig
	 *
	 * @param interopConfig Atributo da Classe
	 */
	public void setInteropConfig(AbstractInteropConfig interopConfig) {
		this.interopConfig = interopConfig;
	}
	
	/**
	 * Método de recuperação do campo interopConfig
	 *
	 * @return valor do campo interopConfig
	 */
	public AbstractInteropConfig getInteropConfig() {
		return interopConfig;
	}
	
	public InteropSetting getSettingBySettingType(final String aCode){
		if(settings != null){
			for (InteropSetting setting : settings) {
				if(setting.getInteropSettingType() != null){
					if(setting.getInteropSettingType().getCode().equals(aCode)){
						return setting;
					}
				}
			}
		}
		return null;
	}
	
	/**
	 * 
	 * Verifica se a configuração está completa
	 * @return
	 */
	public Boolean isCompleteConfiguration(final String... typeCode){
		for (String code : typeCode) {
			InteropSetting setting = getSettingBySettingType(code);

			if(setting == null || setting.getValue() == null ||setting.getValue().isEmpty()){
				return Boolean.FALSE;	
			}

		}
		return Boolean.TRUE;


	}
	
	public void addSetting(final InteropSetting setting){
		if(settings == null) {
			settings = Collections.newArrayList();
		}
		settings.add(setting);
	}
	
	public void removeSetting(final InteropSetting setting){
		settings.remove(setting);
	}
	
	
	/** 
	 * verifica se o valor está preenchido
	 * @param aEntity
	 * @return
	 */
	public boolean isEmptyValue(final String aCode) {
		InteropSetting setting = this.getSettingBySettingType(aCode);
		if(setting.getValue() == null){
			return Boolean.TRUE;
		}
		return setting.getValue().trim().isEmpty();
	}
	
	public InteropSettingSchedule clone(){
		InteropSettingSchedule cloned = new InteropSettingSchedule();
		cloned.setId(getId());
		cloned.setInsertionDate(getInsertionDate());
		cloned.setInteropConfig(getInteropConfig());
		cloned.setOperator(getOperator());
		cloned.setEffectiveDate(getEffectiveDate());
		
		
		MacadameArrayList<InteropSetting> settings = Collections.newArrayList();
		cloned.setSettings(settings);
		if(getSettings() != null && getSettings().isEmpty() == false){
			for(InteropSetting setting : getSettings()){
				InteropSetting settingCloned = new InteropSetting();
				settingCloned = setting.clone();
				settingCloned.setInteropSettingSchedule(cloned);
				settings.add(settingCloned);				
			}
			
		}
		
		return cloned;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		InteropSettingSchedule other = (InteropSettingSchedule) obj;
		if (getId() == null) {
			if (other.getId() != null)
				return false;
		} else if (!getId().equals(other.getId()))
			return false;
		return true;
	}
	
	
	
	
}
