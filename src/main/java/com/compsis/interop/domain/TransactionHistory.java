/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * SICAT - INTEROP<br>
 *
 * Data de Criação: 28/05/2013<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import java.math.BigDecimal;
import java.util.Calendar;

import com.compsis.macadame.domain.MacadameEntity;
import com.compsis.macadame.security.domain.SecurityOperator;
import com.compsis.media.domain.Media;
import com.compsis.protocol.domain.ClearingType;
import com.compsis.vehicle.domain.VehicleClass;
import com.comspsis.system.domain.MacadameSystem;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Representa o histórico da transação. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 28/05/2013 - @author amachado - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
public class TransactionHistory extends MacadameEntity { 
	
	private static final long serialVersionUID = 5684945596781680863L;

	private Transaction transaction;
	
	private Batch batch;
	
	private Clearing clearing;
	
	private Long idClearing;
	
	private Media registeredMedia;

	private Long externalCode;
	
	private Calendar occurrenceDate; 
	
	private BigDecimal value;
	
	private SecurityOperator operator;
	
	private VehicleClass vehicleClass;
	
	private MacadameSystem system;
	
	private String licensePlate;
	
	/**
	 * Anotações realizadas pelo usuário na interface
	 */
	private String notes;

	private TransactionStatus status;
	
	/**
	 * Data da alteração do registro principal da transação,
	 * ou seja a data de alteração e decriação do historico 
	 */
	private Calendar operationDate;
	
	/**
	 * Código de retorno da transação.
	 */
	private ReturnCode returnCode;
	
	/**
	 * Identificador do número de vezes que a Transação foi associada a um lote.
	 * É incrementado a cada vez que for associado um lote para a Transação.
	 * Inicia com o valor zero
	 */
	private Integer sentNumber;
	
	/**
	 * Configuração de interoperabilidade associada a Transação
	 */
	private AbstractInteropConfig interopConfig;
	
	/**
	 * Tipo de compensação
	 */
	private ClearingType clearingType;
	
	/**
	 * Estado do envio da imagem
	 */
	private ImageSendStatus imageSendStatus;
	
	/**
	 * Tipo de passagem
	 */
	private PassageType passageType;
	
	/**
	 * Método de recuperação do campo notes
	 * Anotações realizadas pelo usuário na interface
	 * @return valor do campo notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * Valor de notes atribuído a notes
	 * Anotações realizadas pelo usuário na interface
	 * @param notes Atributo da Classe
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * Método de recuperação do campo transaction
	 *
	 * @return valor do campo transaction
	 */
	public Transaction getTransaction() {
		return transaction;
	}

	/**
	 * Valor de transaction atribuído a transaction
	 *
	 * @param transaction Atributo da Classe
	 */
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	/**
	 * Método de recuperação do campo batch
	 * Método será removido quando o componente BatchTransmission for criado.
	 *
	 * @return valor do campo batch
	 */
	@Deprecated
	public Batch getBatch() {
		return batch;
	}

	/**
	 * Valor de batch atribuído a batch
	 * Método será removido quando o componente BatchTransmission for criado.
	 *
	 * @param batch Atributo da Classe
	 */
	@Deprecated
	public void setBatch(Batch batch) {
		this.batch = batch;
	}

	/**
	 * Método de recuperação do campo registeredMedia
	 *
	 * @return valor do campo registeredMedia
	 */
	public Media getRegisteredMedia() {
		return registeredMedia;
	}

	/**
	 * Valor de registeredMedia atribuído a registeredMedia
	 *
	 * @param registeredMedia Atributo da Classe
	 */
	public void setRegisteredMedia(Media registeredMedia) {
		this.registeredMedia = registeredMedia;
	}

	/**
	 * Método de recuperação do campo seqTransactionInBatch
	 *
	 * @return valor do campo seqTransactionInBatch
	 */
	public Long getExternalCode() {
		return externalCode;
	}

	/**
	 * Valor de seqTransactionInBatch atribuído a seqTransactionInBatch
	 *
	 * @param seqTransactionInBatch Atributo da Classe
	 */
	public void setExternalCode(Long externalCode) {
		this.externalCode = externalCode;
	}

	/**
	 * Método de recuperação do campo operator
	 *
	 * @return valor do campo operator
	 */
	public SecurityOperator getOperator() {
		return operator;
	}

	/**
	 * Valor de operator atribuído a operator
	 *
	 * @param operator Atributo da Classe
	 */
	public void setOperator(SecurityOperator operator) {
		this.operator = operator;
	}

	/**
	 * Método de recuperação do campo vehicleClass
	 *
	 * @return valor do campo vehicleClass
	 */
	public VehicleClass getVehicleClass() {
		return vehicleClass;
	}

	/**
	 * Valor de vehicleClass atribuído a vehicleClass
	 *
	 * @param vehicleClass Atributo da Classe
	 */
	public void setVehicleClass(VehicleClass vehicleClass) {
		this.vehicleClass = vehicleClass;
	}

	/**
	 * Método de recuperação do campo value
	 *
	 * @return valor do campo value
	 */
	public BigDecimal getValue() {
		return value;
	}

	/**
	 * Valor de value atribuído a value
	 *
	 * @param value Atributo da Classe
	 */
	public void setValue(BigDecimal value) {
		this.value = value;
	}

	/**
	 * Método de recuperação do campo occurrenceDate
	 *
	 * @return valor do campo occurrenceDate
	 */
	public Calendar getOccurrenceDate() {
		return occurrenceDate;
	}

	/**
	 * Valor de occurrenceDate atribuído a occurrenceDate
	 *
	 * @param occurrenceDate Atributo da Classe
	 */
	public void setOccurrenceDate(Calendar occurrenceDate) {
		this.occurrenceDate = occurrenceDate;
	}
	
		/**
	 * Método de recuperação do campo status
	 *
	 * @return valor do campo status
	 */
	public TransactionStatus getStatus() {
		return status;
	}

	/**
	 * Valor de status atribuído a status
	 *
	 * @param status Atributo da Classe
	 */
	public void setStatus(TransactionStatus status) {
		this.status = status;
	}

	/**
	 * Método de recuperação do campo operationDate
	 *
	 * @return valor do campo operationDate
	 */
	public Calendar getOperationDate() {
		return operationDate;
	}

	/**
	 * Valor de operationDate atribuído a operationDate
	 *
	 * @param operationDate Atributo da Classe
	 */
	public void setOperationDate(Calendar operationDate) {
		this.operationDate = operationDate;
	}
	
	/**
	 * Método de recuperação do campo returnCode
	 *
	 * @return valor do campo returnCode
	 */
	public ReturnCode getReturnCode() {
		return returnCode;
	}

	/**
	 * Valor de returnCode atribuído a returnCode
	 *
	 * @param returnCode Atributo da Classe
	 */
	public void setReturnCode(ReturnCode returnCode) {
		this.returnCode = returnCode;
	}

	/**
	 * Valor de sentNumber atribuído a sentNumber
	 *
	 * @param sentNumber Atributo da Classe
	 */
	public void setSentNumber(Integer sentNumber) {
		this.sentNumber = sentNumber;
	}
	
	/**
	 * Método de recuperação do campo sentNumber
	 *
	 * @return valor do campo sentNumber
	 */
	public Integer getSentNumber() {
		return sentNumber;
	}
	
	/**
	 * Valor de interopConfig atribuído a interopConfig
	 * @param interopConfig Atributo da Classe
	 */
	public void setInteropConfig(AbstractInteropConfig interopConfig) {
		this.interopConfig = interopConfig;
	}
	
	/**
	 * Método de recuperação do campo interopConfig
	 * @return valor do campo interopConfig
	 */
	public AbstractInteropConfig getInteropConfig() {
		return interopConfig;
	}
		
	/**
	 * Método de recuperação do campo clearing
	 * @return valor do campo clearing
	 */
	public Clearing getClearing() {
		return clearing;
	}

	/**
	 * Valor de clearing atribuído a clearing
	 * @param clearing Atributo da Classe
	 */
	public void setClearing(Clearing clearing) {
		this.clearing = clearing;
		if(this.clearing != null){			
			this.idClearing = clearing.getId();
		}else{
			this.idClearing = null;
		}
	}

	/**
	 * Preenche os atributos do histórico a partir de um Passage
	 * @param passage
	 */
	@SuppressWarnings("deprecation")
	public void fillHistoryOnPassage(Passage passage){
		this.setNotes(passage.getNote());
		this.setBatch(passage.getCurrentBatch());		
		this.setTransaction(passage);
		this.setValue(passage.getValue());
		this.setOperator(passage.getOperator());
		this.setOccurrenceDate(passage.getTransactionDate());
		this.setRegisteredMedia(passage.getMedia());
		this.setExternalCode(passage.getExternalCode());
		this.setVehicleClass(passage.getConsideredVehicleClass());
		this.setTransaction(passage);
		this.setStatus(passage.getStatus());
		this.setReturnCode(passage.getReturnCode());
		this.setSentNumber(passage.getSentNumber());	
		this.setInteropConfig(passage.getInteropConfig());
		this.setSystem(passage.getSystem());
		this.setLicensePlate(passage.getLicensePlate());
	}

	/**
	 * Método de recuperação do campo clearingType
	 *
	 * @return valor do campo clearingType
	 */
	public ClearingType getClearingType() {
		return clearingType;
	}

	/**
	 * Valor de clearingType atribuído a clearingType
	 *
	 * @param clearingType Atributo da Classe
	 */
	public void setClearingType(ClearingType clearingType) {
		this.clearingType = clearingType;
	}

	/**
	 * Método de recuperação do campo imageSendStatus
	 *
	 * @return valor do campo imageSendStatus
	 */
	public ImageSendStatus getImageSendStatus() {
		return imageSendStatus;
	}

	/**
	 * Valor de imageSendStatus atribuído a imageSendStatus
	 *
	 * @param imageSendStatus Atributo da Classe
	 */
	public void setImageSendStatus(ImageSendStatus imageSendStatus) {
		this.imageSendStatus = imageSendStatus;
	}

	/**
	 * Método de recuperação do campo idClearing
	 *
	 * @return valor do campo idClearing
	 */
	public Long getIdClearing() {
		return idClearing;
	}

	/**
	 * Valor de idClearing atribuído a idClearing
	 *
	 * @param idClearing Atributo da Classe
	 */
	public void setIdClearing(Long idClearing) {
		this.idClearing = idClearing;
	}

	/**
	 * Valor de system atribuído a system
	 * @param system Atributo da Classe
	 */
	public void setSystem(MacadameSystem system) {
		this.system = system;
	}
	
	/**
	 * Método de recuperação do campo system
	 * @return valor do campo system
	 */
	public MacadameSystem getSystem() {
		return system;
	}
	
	/**
	 * Valor de licensePlate atribuído a licensePlate
	 * @param licensePlate Atributo da Classe
	 */
	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}
	
	/**
	 * Método de recuperação do campo licensePlate
	 * @return valor do campo licensePlate
	 */
	public String getLicensePlate() {
		return licensePlate;
	}

	/**
	 * Método de recuperação do campo passageType
	 *
	 * @return valor do campo passageType
	 */
	public PassageType getPassageType() {
		return passageType;
	}

	/**
	 * Valor de passageType atribuído a passageType
	 *
	 * @param passageType Atributo da Classe
	 */
	public void setPassageType(PassageType passageType) {
		this.passageType = passageType;
	}
	
}

