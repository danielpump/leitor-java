/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>	
 * Produto SICAT - Interop<br>
 *
 * Data de Criação: 29/05/2013<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.collections.Collections;
import com.compsis.macadame.collections.MacadameList;
import com.compsis.protocol.domain.FileInformation;


/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Classe que representa uma tarefa de transferencia de dados com uma OSA através de arquivos. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 29/05/2013 - @author Naresh Trivedi - Primeiva versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class DataFileTransferTaskConfig extends DataTransferTaskConfig {
	private TaskExecution lastExecution;
	
	/**
	 * Arquivos utilizados na execução da tarefa.
	 */
	private MacadameList<? super InteropFile> files = Collections.newArrayList();
	
	/**
	 * Arquivos inválidos identificados durante a tarefa.
	 */
	private MacadameList<? super InteropFile> invalidFiles = Collections.newArrayList();
	
	/**
	 * Representa os arquivos com sequencial anterior ao atual. Eles devem ser movidos para erro e apagados do diretório de leitura.
	 */
	private MacadameList<? super InteropFile> oldSequenceFiles = Collections.newArrayList(); 
	
	/**
	 * Representação dos objetos de informações dos arquivos.
	 */
	private MacadameList<FileInformation> filesInformations = Collections.newArrayList();

	/**
	 * Objetos que ficam em cache 
	 */	
	private CacheReceiveRecord cache;
	
	/**
	 * Método de recuperação do campo files
	 *
	 * @return valor do campo files
	 */
	public MacadameList <? super InteropFile> getFiles() {
		return files;
	}

	/**
	 * Valor de files atribuído a files
	 *
	 * @param files Atributo da Classe
	 */
	public void setFiles(MacadameList<? super InteropFile> files) {
		this.files = files;
	}

	/**
	 * Método de recuperação do campo invalidFiles
	 *
	 * @return valor do campo invalidFiles
	 */
	public MacadameList<? super InteropFile> getInvalidFiles() {
		return invalidFiles;
	}

	/**
	 * Valor de invalidFiles atribuído a invalidFiles
	 *
	 * @param invalidFiles Atributo da Classe
	 */
	public void setInvalidFiles(MacadameList<? super InteropFile> invalidFiles) {
		this.invalidFiles = invalidFiles;
	}

	/**
	 * Método de recuperação do campo filesInformations
	 *
	 * @return valor do campo filesInformations
	 */
	public MacadameList<FileInformation> getFilesInformations() {
		return filesInformations;
	}

	/**
	 * Valor de filesInformations atribuído a filesInformations
	 *
	 * @param filesInformations Atributo da Classe
	 */
	public void setFilesInformations(MacadameList<FileInformation> filesInformations) {
		this.filesInformations = filesInformations;
	}

	/**
	 * Método de recuperação do campo lastExecution
	 *
	 * @return valor do campo lastExecution
	 */
	public TaskExecution getLastExecution() {
		return lastExecution;
	}

	/**
	 * Valor de lastExecution atribuído a lastExecution
	 *
	 * @param lastExecution Atributo da Classe
	 */
	public void setLastExecution(TaskExecution lastExecution) {
		this.lastExecution = lastExecution;
	}

	/** 
	 * @return
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((files == null) ? 0 : files.hashCode());
		return result;
	}

	/** 	
	 * @param obj
	 * @return
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof DataFileTransferTaskConfig)) {
			return false;
		}
		DataFileTransferTaskConfig other = (DataFileTransferTaskConfig) obj;
		if (files == null) {
			if (other.files != null) {
				return false;
			}
		} else if (!files.equals(other.files)) {
			return false;
		}
		return true;
	}

	/** 
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName();
	}

	public MacadameList<? super InteropFile> getOldSequenceFiles() {
		return oldSequenceFiles;
	}

	public void setOldSequenceFiles(
			MacadameList<? super InteropFile> oldSequenceFiles) {
		this.oldSequenceFiles = oldSequenceFiles;
	}
	
	

	
}
