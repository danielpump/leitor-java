/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto Interop - Domain<br>
 *
 * Data de Criação: 20/02/2014<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import java.util.List;

import com.compsis.macadame.domain.MacadameObject;
import com.compsis.notification.domain.Notification;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Bean que irá armazenar as listas de notificaçoes "associadas" às tarefas. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 20/02/2014 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public final class InteropNotifications extends MacadameObject {
	private List<Notification> notifications;
	private List<DelayedTaskManagement> notificationManagement;
	
	/**
	 * Método de recuperação do campo notifications
	 *
	 * @return valor do campo notifications
	 */
	public List<Notification> getNotifications() {
		return notifications;
	}
	/**
	 * Valor de notifications atribuído a notifications
	 *
	 * @param notifications Atributo da Classe
	 */
	public void setNotifications(List<Notification> notifications) {
		this.notifications = notifications;
	}
	/**
	 * Método de recuperação do campo notificationManagement
	 *
	 * @return valor do campo notificationManagement
	 */
	public List<DelayedTaskManagement> getNotificationManagement() {
		return notificationManagement;
	}
	/**
	 * Valor de notificationManagement atribuído a notificationManagement
	 *
	 * @param notificationManagement Atributo da Classe
	 */
	public void setNotificationManagement(
			List<DelayedTaskManagement> notificationManagement) {
		this.notificationManagement = notificationManagement;
	}
}
