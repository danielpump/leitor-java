/*
 * COMPSIS - Computadores e Sistemas Ind. e Com. LTDA<br>
 * SICAT - Interop<br>
 *
 * Data de Criação: 17/10/2012<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import java.math.BigDecimal;
import java.util.Calendar;

import com.compsis.macadame.domain.MacadameEntity;
import com.compsis.macadame.security.domain.SecurityOperator;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Representa os pagamentos de lote. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 17/10/2012 - @author ntrivedi - Primeiva versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class BatchPayment extends MacadameEntity {
	
	/**
	 * Valor numérico de expectedPaymentDate. Utilizado para groupby em relatório.
	 */
	private Long expectedPaymentDateNumeric;
	
	private Batch batch;
	private Calendar expectedPaymentDate;
	private BigDecimal amount;
	private BigDecimal discount;
	private Calendar paymentDate;
	private String note;
	private SourceTransfer sourceTransfer;
	private boolean selected;
	private SecurityOperator operator;
	
	/**
	 * Método de recuperação do campo batch
	 *
	 * @return valor do campo batch
	 */
	public Batch getBatch() {
		return batch;
	}
	/**
	 * Valor de batch atribu�do a batch
	 *
	 * @param batch Atributo da Classe
	 */
	public void setBatch(Batch batch) {
		this.batch = batch;
	}
	/**
	 * M�todo de recupera��o do campo expectedPaymentDate
	 *
	 * @return valor do campo expectedPaymentDate
	 */
	public Calendar getExpectedPaymentDate() {
		return expectedPaymentDate;
	}
	/**
	 * Valor de expectedPaymentDate atribu�do a expectedPaymentDate
	 *
	 * @param expectedPaymentDate Atributo da Classe
	 */
	public void setExpectedPaymentDate(Calendar expectedPaymentDate) {
		this.expectedPaymentDate = expectedPaymentDate;
	}
	/**
	 * M�todo de recupera��o do campo amount
	 *
	 * @return valor do campo amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	/**
	 * Valor de amount atribu�do a amount
	 *
	 * @param amount Atributo da Classe
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	/**
	 * M�todo de recupera��o do campo discount
	 *
	 * @return valor do campo discount
	 */
	public BigDecimal getDiscount() {
		return discount;
	}
	/**
	 * Valor de discount atribu�do a discount
	 *
	 * @param discount Atributo da Classe
	 */
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	/**
	 * M�todo de recupera��o do campo paymentDate
	 *
	 * @return valor do campo paymentDate
	 */
	public Calendar getPaymentDate() {
		return paymentDate;
	}
	/**
	 * Valor de paymentDate atribu�do a paymentDate
	 *
	 * @param paymentDate Atributo da Classe
	 */
	public void setPaymentDate(Calendar paymentDate) {
		this.paymentDate = paymentDate;
	}
	/**
	 * M�todo de recupera��o do campo note
	 *
	 * @return valor do campo note
	 */
	public String getNote() {
		return note;
	}
	/**
	 * Valor de note atribu�do a note
	 *
	 * @param note Atributo da Classe
	 */
	public void setNote(String note) {
		this.note = note;
	}
	/**
	 * M�todo de recupera��o do campo sourceTransfer
	 *
	 * @return valor do campo sourceTransfer
	 */
	public SourceTransfer getSourceTransfer() {
		return sourceTransfer;
	}
	/**
	 * Valor de sourceTransfer atribu�do a sourceTransfer
	 *
	 * @param sourceTransfer Atributo da Classe
	 */
	public void setSourceTransfer(SourceTransfer sourceTransfer) {
		this.sourceTransfer = sourceTransfer;
	}
	/**
	 * Método de recuperação do campo selected
	 *
	 * @return valor do campo selected
	 */
	public boolean getSelected() {
		return selected;
	}
	/**
	 * Valor de selected atribuído a selected
	 *
	 * @param selected Atributo da Classe
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	/**
	 * Método de recuperação do campo operator
	 *
	 * @return valor do campo operator
	 */
	public SecurityOperator getOperator() {
		return operator;
	}
	/**
	 * Valor de operator atribuído a operator
	 *
	 * @param operator Atributo da Classe
	 */
	public void setOperator(SecurityOperator operator) {
		this.operator = operator;
	}
	
	/** 
	 *  Getter de expectedPaymentDateNumeric
	 * @return
	 */
	public Long getExpectedPaymentDateNumeric() {
		return expectedPaymentDateNumeric;
	}
	/** 
	 * Setter de expectedPaymentDateNumeric
	 * @param expectedPaymentDateNumeric
	 */
	public void setExpectedPaymentDateNumeric(Long expectedPaymentDateNumeric) {
		this.expectedPaymentDateNumeric = expectedPaymentDateNumeric;
	}

	
}
