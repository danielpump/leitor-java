/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto SICAT - Interop<br>
 *
 * Data de Criação: 29/05/2013<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.collections.MacadameList;
import com.compsis.macadame.domain.MacadameEntity;



/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Classe que representa uma tarefa de transferencia de lotes de passagens com uma OSA. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 29/05/2013 - @author Naresh Trivedi - Primeiva versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class EntityTaskConfig extends RunningInteropTask {

	 /**
	 * Lista de Entidades utilizadas durante a execução da tarefa.
	 */
	private MacadameList<? extends MacadameEntity> entities;

	/**
	 * Valor de entities atribuído a entities
	 *
	 * @param entities Atributo da Classe
	 */
	public void setEntities(MacadameList<? extends MacadameEntity> entities) {
		this.entities = entities;
	}

	/**
	 * Método de recuperação do campo entities
	 *
	 * @return valor do campo entities
	 */
	@SuppressWarnings("unchecked")
	public <M extends MacadameEntity> MacadameList<M> getEntities() {
		return (MacadameList<M>) entities;
	}
}
