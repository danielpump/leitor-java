/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 17/10/2012<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import java.math.BigDecimal;
import java.util.Calendar;

import com.compsis.macadame.domain.MacadameEntity;
import com.compsis.macadame.security.domain.SecurityOperator;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 17/10/2012 - @author ntrivedi - Primeiva vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class BatchHistory extends MacadameEntity {

	private BatchOperation operation;
	private BatchStatus status;
	private Calendar operationDate;
	private Calendar operationDay;
	private SecurityOperator operator;
	private ExecutionMode mode;
	private Integer quantity;
	private BigDecimal amount;
	
	/**
	 * batch referente ao historico
	 */
	private Batch batch;

	/**
	 * Historico do codigo de retorno do lote
	 */
	private ReturnCode returnCode;
	
	/**
	 * M�todo de recupera��o do campo operation
	 *
	 * @return valor do campo operation
	 */
	public BatchOperation getOperation() {
		return operation;
	}
	/**
	 * Valor de aOperation atribu�do a operation
	 *
	 * @param aOperation Atributo da Classe
	 */
	public void setOperation(BatchOperation aOperation) {
		operation = aOperation;
	}
	/**
	 * M�todo de recupera��o do campo status
	 *
	 * @return valor do campo status
	 */
	public BatchStatus getStatus() {
		return status;
	}
	/**
	 * Valor de aStatus atribu�do a status
	 *
	 * @param aStatus Atributo da Classe
	 */
	public void setStatus(BatchStatus aStatus) {
		status = aStatus;
	}
	/**
	 * M�todo de recupera��o do campo operationDate
	 *
	 * @return valor do campo operationDate
	 */
	public Calendar getOperationDate() {
		return operationDate;
	}
	/**
	 * Valor de aOperationDate atribu�do a operationDate
	 *
	 * @param aOperationDate Atributo da Classe
	 */
	public void setOperationDate(Calendar aOperationDate) {
		operationDate = aOperationDate;
		if(this.operationDay==null) {
			operationDay = operationDate;
		}
	}
	/**
	 * M�todo de recupera��o do campo operator
	 *
	 * @return valor do campo operator
	 */
	public SecurityOperator getOperator() {
		return operator;
	}
	/**
	 * Valor de aOperator atribu�do a operator
	 *
	 * @param aOperator Atributo da Classe
	 */
	public void setOperator(SecurityOperator aOperator) {
		operator = aOperator;
	}
	/**
	 * M�todo de recupera��o do campo mode
	 *
	 * @return valor do campo mode
	 */
	public ExecutionMode getMode() {
		return mode;
	}
	/**
	 * Valor de aMode atribu�do a mode
	 *
	 * @param aMode Atributo da Classe
	 */
	public void setMode(ExecutionMode aMode) {
		mode = aMode;
	}
	/**
	 * M�todo de recupera��o do campo quantity
	 *
	 * @return valor do campo quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}
	/**
	 * Valor de aQuantity atribu�do a quantity
	 *
	 * @param aQuantity Atributo da Classe
	 */
	public void setQuantity(Integer aQuantity) {
		quantity = aQuantity;
	}
	/**
	 * M�todo de recupera��o do campo amount
	 *
	 * @return valor do campo amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	/**
	 * Valor de aAmount atribu�do a amount
	 *
	 * @param aAmount Atributo da Classe
	 */
	public void setAmount(BigDecimal aAmount) {
		amount = aAmount;
	}

	/**
	 * Método de recuperação do campo batch
	 *
	 * @return valor do campo batch
	 */
	public Batch getBatch() {
		return batch;
	}

	/**
	 * Valor de aBatch atribuição do batch
	 *
	 * @param aBatch Atributo da Classe
	 */
	public void setBatch(Batch aBatch) {
		this.batch = aBatch;
	}
	
	/**
	 * Método de recuperação do campo returnCode
	 *
	 * @return valor do campo returnCode
	 */
	public ReturnCode getReturnCode() {
		return returnCode;
	}
	
	/**
	 * Valor de returnCode atribuído a returnCode
	 *
	 * @param returnCode Atributo da Classe
	 */
	public void setReturnCode(ReturnCode returnCode) {
		this.returnCode = returnCode;
	}

	/**
	 * Valor de operationDay atribuído a operationDay
	 *
	 * @param operationDay Atributo da Classe
	 */
	public void setOperationDay(Calendar operationDay) {
		this.operationDay = operationDay;
	}
	
	/**
	 * Método de recuperação do campo operationDay
	 *
	 * @return valor do campo operationDay
	 */
	public Calendar getOperationDay() {
		return operationDay;
	}
}
