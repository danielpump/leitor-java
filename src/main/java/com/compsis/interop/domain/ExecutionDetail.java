package com.compsis.interop.domain;

import java.util.Calendar;

import com.compsis.macadame.domain.MacadameEntity;
import com.compsis.macadame.domain.MacadameFile;
import com.compsis.protocol.domain.ExecutionDetailType;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Classe que representa os dados da tabela TITP_TRANSFERRECORD
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 07/06/2013 - @author Daniel Ferraz - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
@SuppressWarnings("serial")
public class ExecutionDetail extends MacadameEntity {
	/**
	 * Status de execução OK
	 */
	public static final Integer STATUS_OK = Integer.valueOf(0);

	/**
	 * Status de execução - Falha
	 */
	public static final Integer STATUS_FAIL = Integer.valueOf(1);
	
	/**
	 * Status de execução - Sucesso parcial do detalhe
	 */
	public static final Integer STATUS_PARTIAL_OK = Integer.valueOf(2);
	
	/**
	 * A execução de tarefa que este detalhe pertence
	 */
	private TaskExecution taskExecution;
	
	/**
	 * Código da task que processou o transfer record
	 */
	private String codeTask;
	
	/**
	 * Número sequencial do lote enviado ao gestor.
	 */
	private Long sequential;
	
	/**
	 * Data e hora do recebimento do arquivo pelo sistema
	 */
	private Calendar executionDate;
	
	/**
	 * Data e hora de geração do arquivo de cadastro (Tag, lista nela, etc)
	 */
	private Calendar generateDate;
	
	/**
	 * Versão do cadastro de mídia na pista, que contém as mídias do arquivo correspondente
	 */
	private Integer laneVersion;
	
	/**
	 * Quantidade de registros no arquivo
	 */
	private Long quantity;
	
	/**
	 * Resultado da execuação do registro 0 - ok; 1 - Erro
	 */
	private Integer status;
	
	/**
	 * Registra as mensagens de erro
	 */
	private String errorMessage;

	/**
	 * Tipo de detalhe da execução
	 */
	private ExecutionDetailType type;
	/**
	 * Referencia para o arquivo de log
	 */
	private MacadameFile logFile;
	
	/**
	 * Veja a documentação em: {@link InteropTask#getLogNames(String...)}
	 */
	public String[] getLogNames(String... actualLogNames) {
		if(getTaskExecution() != null && getTaskExecution().getTaskConfig() != null){
			return getTaskExecution().getTaskConfig().getLogNames(actualLogNames);
		}else{
			return actualLogNames;
		}
	}
	
	/**
	 * Método de recuperação do campo sequential
	 *
	 * @return valor do campo sequential
	 */
	public Long getSequential() {
		return sequential;
	}

	/**
	 * Valor de sequential atribuído a sequential
	 *
	 * @param sequential Atributo da Classe
	 */
	public void setSequential(Long sequential) {
		this.sequential = sequential;
	}

	/**
	 * Método de recuperação do campo executionDate
	 *
	 * @return valor do campo transferDate
	 */
	public Calendar getExecutionDate() {
		return executionDate;
	}

	/**
	 * Valor de transferDate atribuído a executionDate
	 *
	 * @param executionDate Atributo da Classe
	 */
	public void setExecutionDate(Calendar executionDate) {
		this.executionDate = executionDate;
	}

	/**
	 * Método de recuperação do campo generateDate
	 *
	 * @return valor do campo generateDate
	 */
	public Calendar getGenerateDate() {
		return generateDate;
	}

	/**
	 * Valor de generateDate atribuído a generateDate
	 *
	 * @param generateDate Atributo da Classe
	 */
	public void setGenerateDate(Calendar generateDate) {
		this.generateDate = generateDate;
	}

	/**
	 * Método de recuperação do campo laneVersion
	 *
	 * @return valor do campo laneVersion
	 */
	public Integer getLaneVersion() {
		return laneVersion;
	}

	/**
	 * Valor de laneVersion atribuído a laneVersion
	 *
	 * @param laneVersion Atributo da Classe
	 */
	public void setLaneVersion(Integer laneVersion) {
		this.laneVersion = laneVersion;
	}

	/**
	 * Método de recuperação do campo quantity
	 *
	 * @return valor do campo quantity
	 */
	public Long getQuantity() {
		return quantity;
	}

	/**
	 * Valor de quantity atribuído a quantity
	 *
	 * @param quantity Atributo da Classe
	 */
	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	/**
	 * Método de recuperação do campo status
	 *
	 * @return valor do campo status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * Valor de status atribuído a status
	 *
	 * @param status Atributo da Classe
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * Método de recuperação do campo errorMessage
	 *
	 * @return valor do campo errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * Valor de errorMessage atribuído a errorMessage
	 *
	 * @param errorMessage Atributo da Classe
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * Método de recuperação do campo codeTask
	 * Código da task que processou o transfer record
	 * @return valor do campo codeTask
	 */
	public String getCodeTask() {
		return codeTask;
	}

	/**
	 * Valor de codeTask atribuído a codeTask
	 * Código da task que processou o transfer record
	 * @param codeTask Atributo da Classe
	 */
	public void setCodeTask(String codeTask) {
		this.codeTask = codeTask;
	}

	/**
	 * Método de recuperação do campo type
	 * Tipo de detalhe da execução
	 * @return valor do campo type
	 */
	public ExecutionDetailType getType() {
		return type;
	}

	/**
	 * Valor de type atribuído a type
	 * Tipo de detalhe da execução
	 * @param type Atributo da Classe
	 */
	public void setType(ExecutionDetailType type) {
		this.type = type;
	}

	/**
	 * Valor de taskExecution atribuído a taskExecution
	 * A execução de tarefa que este detalhe pertence
	 * @param taskExecution Atributo da Classe
	 */
	public void setTaskExecution(TaskExecution taskExecution) {
		this.taskExecution = taskExecution;
	}
	
	/**
	 * Método de recuperação do campo taskExecution
	 * A execução de tarefa que este detalhe pertence
	 * @return valor do campo taskExecution
	 */
	public TaskExecution getTaskExecution() {
		return taskExecution;
	}
	
	/**
	 * Valor de logFile atribuído a logFile
	 * Referencia para o arquivo de log
	 * @param logFile Atributo da Classe
	 */
	public void setLogFile(MacadameFile logFile) {
		this.logFile = logFile;
	}
	
	/**
	 * Método de recuperação do campo logFile
	 * Referencia para o arquivo de log
	 * @return valor do campo logFile
	 */
	public MacadameFile getLogFile() {
		return logFile;
	}
}
