/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto Protocol - Domain<br>
 *
 * Data de Criação: 21/01/2013<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.domain.MacadameEntity;
import com.compsis.protocol.domain.Protocol;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Tipos de conversões do protocolo
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 21/01/2013 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class ProtocolConversionType extends MacadameEntity {
	
	/**
	 * Protocolo associado
	 */
	private Protocol protocol;
	
	/**
	 * ConversionType associado
	 */
	private ConversionType conversionType;
	
	/**
	 * Método de recuperação do campo protocol
	 *
	 * @return valor do campo protocol
	 */
	public Protocol getProtocol() {
		return protocol;
	}

	/**
	 * Valor de protocol atribuído a protocol
	 *
	 * @param protocol Atributo da Classe
	 */
	public void setProtocol(Protocol protocol) {
		this.protocol = protocol;
	}

	/**
	 * Método de recuperação do campo conversionType
	 *
	 * @return valor do campo conversionType
	 */
	public ConversionType getConversionType() {
		return conversionType;
	}

	/**
	 * Valor de conversionType atribuído a conversionType
	 *
	 * @param conversionType Atributo da Classe
	 */
	public void setConversionType(ConversionType conversionType) {
		this.conversionType = conversionType;
	}
	
}
