/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto Interop - Domain<br>
 *
 * Data de Criação: 04/06/2013<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;
import com.compsis.interop.domain.RunningInteropTask.Paths;
import com.compsis.macadame.collections.MacadameList;
import com.compsis.macadame.domain.MacadameEntity;
import com.compsis.macadame.domain.MacadameFile;
import com.compsis.macadame.security.domain.SecurityOperator;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Representa o retorno financeiro do lote
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 04/06/2013 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class FinancialReturn extends MacadameEntity {
	/**
	 * Todas as transações do lote
	 */
	private MacadameList<Transaction> transactions;
	/**
	 * Transações recusadas pelo gestor
	 */
	private MacadameList<Transaction> refusedTransactions;
	/**
	 * Transações aceitas pelo gestor
	 */
	private MacadameList<Transaction> acceptedTransactions;
	/**
	 * Agendamento de pagamento do gestor para este lote
	 */
	private MacadameList<BatchPayment> batchPayments;
	/**
	 * Operador que está realizando o recebimento financeiro 
	 */
	private SecurityOperator operator;
	
	private Paths paths;
	
	/**
	 * O arquivo TRF
	 */
	private MacadameFile file;
	
	/**
	 * Método de recuperação do campo file
	 * O arquivo TRF
	 * @return valor do campo file
	 */
	public MacadameFile getFile() {
		return file;
	}
	/**
	 * Valor de file atribuído a file
	 * O arquivo TRF
	 * @param file Atributo da Classe
	 */
	public void setFile(MacadameFile file) {
		this.file = file;
	}
	
	/**
	 * Método de recuperação do campo transactions
	 * Todas as transações do lote
	 *
	 * @return valor do campo transactions
	 */
	public MacadameList<Transaction> getTransactions() {
		return transactions;
	}
	/**
	 * Valor de transactions atribuído a transactions
	 * Todas as transações do lote
	 *
	 * @param transactions Atributo da Classe
	 */
	public void setTransactions(MacadameList<Transaction> transactions) {
		this.transactions = transactions;
	}
	/**
	 * Método de recuperação do campo refusedTransactions
	 * Transações recusadas pelo gestor
	 *
	 * @return valor do campo refusedTransactions
	 */
	public MacadameList<Transaction> getRefusedTransactions() {
		return refusedTransactions;
	}
	/**
	 * Valor de refusedTransactions atribuído a refusedTransactions
	 * Transações recusadas pelo gestor
	 *
	 * @param refusedTransactions Atributo da Classe
	 */
	public void setRefusedTransactions(MacadameList<Transaction> refusedTransactions) {
		this.refusedTransactions = refusedTransactions;
	}
	/**
	 * Método de recuperação do campo acceptedTransactions
	 * Transações aceitas pelo gestor
	 *
	 * @return valor do campo acceptedTransactions
	 */
	public MacadameList<Transaction> getAcceptedTransactions() {
		return acceptedTransactions;
	}
	/**
	 * Valor de acceptedTransactions atribuído a acceptedTransactions
	 * Transações aceitas pelo gestor
	 *
	 * @param acceptedTransactions Atributo da Classe
	 */
	public void setAcceptedTransactions(
			MacadameList<Transaction> acceptedTransactions) {
		this.acceptedTransactions = acceptedTransactions;
	}
	/**
	 * Método de recuperação do campo batchPayments
	 * Agendamento de pagamento do gestor para este lote
	 *
	 * @return valor do campo batchPayments
	 */
	public MacadameList<BatchPayment> getBatchPayments() {
		return batchPayments;
	}
	/**
	 * Valor de batchPayments atribuído a batchPayments
	 * Agendamento de pagamento do gestor para este lote
	 *
	 * @param batchPayments Atributo da Classe
	 */
	public void setBatchPayments(MacadameList<BatchPayment> batchPayments) {
		this.batchPayments = batchPayments;
	}
	
	/**
	 * Valor de operator atribuído a operator
	 * Operador que está realizando o recebimento financeiro
	 * @param operator Atributo da Classe
	 */
	public void setOperator(SecurityOperator operator) {
		this.operator = operator;
	}
	
	/**
	 * Método de recuperação do campo operator
	 * Operador que está realizando o recebimento financeiro
	 * @return valor do campo operator
	 */
	public SecurityOperator getOperator() {
		return operator;
	}
	
	/**
	 * Valor de paths atribuído a paths
	 *
	 * @param paths Atributo da Classe
	 */
	public void setPaths(Paths paths) {
		this.paths = paths;
	}
	
	/**
	 * Método de recuperação do campo paths
	 *
	 * @return valor do campo paths
	 */
	public Paths getPaths() {
		return paths;
	}
}
