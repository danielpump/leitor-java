/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 20/05/2014<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.domain.MacadameEntity;
import com.compsis.protocol.domain.ClearingType;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Representa uma transamissão de transação que será realizada. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 20/05/2014 - @author dferraz - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
@SuppressWarnings("serial")
public abstract class Clearing extends MacadameEntity {
	
	/** 
	 * Retorna o código do tipo da compensação
	 * @return
	 */
	public abstract String getClearingCode();
	

}
