package com.compsis.interop.domain;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import com.compsis.macadame.collections.Collections;
import com.compsis.macadame.collections.MacadameList;
import com.compsis.site.domain.Unit;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Representa um agrupamento de transações de um lote, os lotes devem ser agrupados por dia e praça, <br>
 * <b>Exemplo:</b> um objeto dessa clase deve conter uma lista de transações do dia 25/12/2013 da praça 1. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 26/08/2013 - @author bcoan - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
public class TransactionGroup {
	
	private Calendar Date;
	private Unit plaza;
	private MacadameList<? super Transaction> transactions;

	public TransactionGroup(Calendar transactionDate, Unit plaza, MacadameList<? super Transaction> transactions) {
		this.Date = transactionDate;
		this.plaza = plaza;
		this.transactions = transactions;
		transactions = Collections.newArrayList();
	}

	public TransactionGroup() {
		transactions = Collections.newArrayList();
	}

	public Calendar getDate() {
		return Date;
	}

	public void setDate(Calendar date) {
		Date = date;
	}

	public Unit getPlaza() {
		return plaza;
	}

	public void setPlaza(Unit plaza) {
		this.plaza = plaza;
	}

	public <E extends MacadameList> E getTransactions() {
		return (E) transactions;
	}

	public void setTransactions(MacadameList<? super Transaction> transactions) {
		this.transactions = transactions;
	}

	public void add(Transaction transaction) {
			
		transactions.add(transaction);
		
	}


}
