package com.compsis.interop.domain;

import com.compsis.macadame.domain.MacadameEntity;
import com.compsis.notification.domain.SentHistory;

/**
 * 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Representa o histórico da notificação
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 20/02/2014 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
@SuppressWarnings("serial")
public class NotificationHistory extends MacadameEntity{
	public InteropTask interopTask;
	public SentHistory senderHistory;
	/**
	 * Método de recuperação do campo interopTask
	 *
	 * @return valor do campo interopTask
	 */
	public InteropTask getInteropTask() {
		return interopTask;
	}
	/**
	 * Valor de interopTask atribuído a interopTask
	 *
	 * @param interopTask Atributo da Classe
	 */
	public void setInteropTask(InteropTask interopTask) {
		this.interopTask = interopTask;
	}
	/**
	 * Método de recuperação do campo senderHistory
	 *
	 * @return valor do campo senderHistory
	 */
	public SentHistory getSenderHistory() {
		return senderHistory;
	}
	/**
	 * Valor de senderHistory atribuído a senderHistory
	 *
	 * @param senderHistory Atributo da Classe
	 */
	public void setSenderHistory(SentHistory senderHistory) {
		this.senderHistory = senderHistory;
	}
}