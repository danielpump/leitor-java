package com.compsis.interop.domain;

import java.util.Map;

import com.compsis.macadame.collections.MacadameList;
import com.compsis.macadame.navigator.annotation.Navigable;
import com.compsis.site.domain.Unit;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Classe que representa os dados de execução de tarefa quando esta for relacionado
 * a importação do arquivo de isentos avi alesp
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 07/06/2013 - @author Andre Novais - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
@SuppressWarnings("serial")
public class ExemptAlespExecutionDetail extends FileExecutionDetail {
	
	private TaskExecution taskExecution;
	
	/**
	 * Linhas do arquivo csv
	 */
	private MacadameList<MacadameList<String>> lines;

	private MacadameList<Unit> units;
	
	/**
	 * Arquivo csv
	 */
	private InteropFile file;
	/**
	 * Configuração da media que serão isentadas
	 */
	private MacadameList<ProtocolMediaMopProfile> mediaProfilesExempts;
	
	/**
	 * Configuração da media que serão isentadas
	 */
	private MacadameList<ProtocolMediaMopProfile> mediaProfilesNotExempts;
	
	/**
	 * Construtor padrão da classe
	 */
	
	private Map<String,MacadameList<ProtocolMediaMopProfile>> mapGroupingMedia;
	
	
	public ExemptAlespExecutionDetail(){
	}
	
	
	public ExemptAlespExecutionDetail(ExecutionDetail detail) {
		super();
		super.setInsertionDate(detail.getInsertionDate());
	    super.setId(detail.getId());
		super.setCodeTask(detail.getCodeTask());
		super.setSequential(detail.getSequential());
		super.setExecutionDate(detail.getExecutionDate());
		super.setGenerateDate(detail.getGenerateDate());
		super.setLaneVersion(detail.getLaneVersion());
		super.setQuantity(detail.getQuantity());
		super.setStatus(detail.getStatus());
		super.setErrorMessage(detail.getErrorMessage());
	}


	/**
	 * Método de recuperação do campo taskExecution
	 *
	 * @return valor do campo taskExecution
	 */
	public TaskExecution getTaskExecution() {
		return taskExecution;
	}


	/**
	 * Valor de taskExecution atribuído a taskExecution
	 *
	 * @param taskExecution Atributo da Classe
	 */
	public void setTaskExecution(TaskExecution taskExecution) {
		this.taskExecution = taskExecution;
	}


	/**
	 * Método de recuperação do campo lines
	 *
	 * @return valor do campo lines
	 */
	public MacadameList<MacadameList<String>> getLines() {
		return lines;
	}


	/**
	 * Valor de lines atribuído a lines
	 *
	 * @param lines Atributo da Classe
	 */
	public void setLines(MacadameList<MacadameList<String>> lines) {
		this.lines = lines;
	}


	/**
	 * Método de recuperação do campo file
	 *
	 * @return valor do campo file
	 */
	public InteropFile getFile() {
		return file;
	}


	/**
	 * Valor de file atribuído a file
	 *
	 * @param file Atributo da Classe
	 */
	public void setFile(InteropFile file) {
		this.file = file;
	}


	

	/**
	 * Método de recuperação do campo mediaProfilesExempts
	 *
	 * @return valor do campo mediaProfilesExempts
	 */
	
	public MacadameList<ProtocolMediaMopProfile> getMediaProfilesExempts() {
		return mediaProfilesExempts;
	}


	/**
	 * Valor de mediaProfilesExempts atribuído a mediaProfilesExempts
	 *
	 * @param mediaProfilesExempts Atributo da Classe
	 */
	public void setMediaProfilesExempts(
			MacadameList<ProtocolMediaMopProfile> mediaProfilesExempts) {
		this.mediaProfilesExempts = mediaProfilesExempts;
	}


	/**
	 * Método de recuperação do campo mediaProfilesNotExempts
	 *
	 * @return valor do campo mediaProfilesNotExempts
	 */
	
	public MacadameList<ProtocolMediaMopProfile> getMediaProfilesNotExempts() {
		return mediaProfilesNotExempts;
	}


	/**
	 * Valor de mediaProfilesNotExempts atribuído a mediaProfilesNotExempts
	 *
	 * @param mediaProfilesNotExempts Atributo da Classe
	 */
	public void setMediaProfilesNotExempts(
			MacadameList<ProtocolMediaMopProfile> mediaProfilesNotExempts) {
		this.mediaProfilesNotExempts = mediaProfilesNotExempts;
	}


	/**
	 * Método de recuperação do campo units
	 *
	 * @return valor do campo units
	 */
	public MacadameList<Unit> getUnits() {
		return units;
	}


	/**
	 * Valor de units atribuído a units
	 *
	 * @param units Atributo da Classe
	 */
	public void setUnits(MacadameList<Unit> units) {
		this.units = units;
	}


	/**
	 * Método de recuperação do campo mapGroupingMedia
	 *
	 * @return valor do campo mapGroupingMedia
	 */
	public Map<String, MacadameList<ProtocolMediaMopProfile>> getMapGroupingMedia() {
		return mapGroupingMedia;
	}


	/**
	 * Valor de mapGroupingMedia atribuído a mapGroupingMedia
	 *
	 * @param mapGroupingMedia Atributo da Classe
	 */
	public void setMapGroupingMedia(
			Map<String, MacadameList<ProtocolMediaMopProfile>> mapGroupingMedia) {
		this.mapGroupingMedia = mapGroupingMedia;
	}

	
	
		

}
