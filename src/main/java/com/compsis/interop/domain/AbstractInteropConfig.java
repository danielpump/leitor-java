/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto Interop - DOMAIN<br>
 *
 * Data de Criação: 17/06/2013<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

import com.compsis.macadame.collections.MacadameArrayList;
import com.compsis.macadame.collections.MacadameList;
import com.compsis.macadame.domain.DomainSpecificEntity;
import com.compsis.macadame.domain.Site;
import com.compsis.macadame.navigator.annotation.Navigable;
import com.compsis.macadame.security.domain.SecurityOperator;
import com.compsis.mop.domain.MeanOfPayment;
import com.compsis.protocol.domain.Protocol;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Representa entidade de configuração com meio de pagamento protocolo e Site
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 17/06/2013 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public abstract class AbstractInteropConfig extends DomainSpecificEntity{
	private MeanOfPayment meanOfPayment;
	private Site site;
	private Protocol protocol;
	private MacadameList<InteropConversion> converters;
	private MacadameList<InteropSettingSchedule> settingSchedules;
	private MacadameList<InteropTask> tasksConfig;
	private MacadameList<MediaStatusSetting> itMessages;
	
	/**
	 * Indica se é a primeira ativação da InteropConfig
	 */
	private Calendar firstActivationDate;
	/**
	 * InteropConfig pai 
	 */
	private AbstractInteropConfig parent;
	
	/**
	 * esta configuração pode enviar violador
	 */
	private Boolean allowSendViolator;
	/**
	 * Operador que está responsável pela configuração
	 */
	private SecurityOperator operator;
	
	public InteropSettingSchedule getMostRecentSettingSchedule(){
		InteropSettingSchedule settingSchedule = null;
		if(settingSchedules != null && !settingSchedules.isEmpty()){
			for(InteropSettingSchedule inSetSch : settingSchedules){
				if(settingSchedule == null){
					settingSchedule = inSetSch;
				}else if(inSetSch.getEffectiveDate() == null){
					return inSetSch;
				}else{
					if(settingSchedule.getEffectiveDate() != null && settingSchedule.getEffectiveDate().compareTo(inSetSch.getEffectiveDate()) < 0){
						settingSchedule = inSetSch;
					}
				}
			}
		}
		return settingSchedule;
	}
	
	/**
	 * M�todo de recupera��o do campo meanOfPayment
	 *
	 * @return valor do campo meanOfPayment
	 */
	public MeanOfPayment getMeanOfPayment() {
		return meanOfPayment;
	}
	/**
	 * M�todo de recupera��o do campo settings
	 *
	 * @return valor do campo settings
	 */
	@Navigable
	public MacadameList<InteropSettingSchedule> getSettingSchedules() {
		return settingSchedules;
	}
	/**E
	 * Valor de aSettings atribu�do a settings
	 *
	 * @param aSettings Atributo da Classe
	 */
	public void setSettingSchedules(MacadameList<InteropSettingSchedule> aSettings) {
		settingSchedules = aSettings;
	}

	/**
	 * Valor de aMeanOfPayment atribu�do a meanOfPayment
	 *
	 * @param aMeanOfPayment Atributo da Classe
	 */
	public void setMeanOfPayment(MeanOfPayment aMeanOfPayment) {
		meanOfPayment = aMeanOfPayment;
	}

	/**
	 * M�todo de recupera��o do campo protocol
	 *
	 * @return valor do campo protocol
	 */
	public Protocol getProtocol() {
		return protocol;
	}
	/**
	 * Valor de aProtocol atribu�do a protocol
	 *
	 * @param aProtocol Atributo da Classe
	 */
	public void setProtocol(Protocol aProtocol) {
		protocol = aProtocol;
	}
	
	/**
	 * M�todo de recupera��o do campo converters
	 *
	 * @return valor do campo converters
	 */
	@Navigable
	public MacadameList<InteropConversion> getConverters() {
		return converters;
	}
	
	/**
	 * Valor de aConverters atribu�do a converters
	 *
	 * @param aConverters Atributo da Classe
	 */
	public void setConverters(MacadameList<InteropConversion> aConverters) {
		
		converters = aConverters;
	}
	
	
	/**
	 * 
	 * @deprecated
	 * @param aConvertable
	 * @return
	 */
	public InteropConversion getConverter(final String aConvertable) {
		for (InteropConversion converter : getConverters()) {
			if (converter.getConversionType().getEntity().equals(aConvertable)) {
				return converter; 
			}
		}
		if(getParent()!=null && getParent().getConverters()!=null){
			for (InteropConversion converter : getParent().getConverters()) {
				if (converter.getConversionType().getEntity().equals(aConvertable)) {
					return converter; 
				}
			}					
		}
		return null;
	}
	public InteropConversion getConverterByCode(final String aConvertable) {
		for (InteropConversion converter : getConverters()) {
			if (converter.getConversionType().getCode().equals(aConvertable)) {
				return converter; 
			}
		}
		if(getParent()!=null && getParent().getConverters()!=null){
			for (InteropConversion converter : getParent().getConverters()) {
				if (converter.getConversionType().getCode().equals(aConvertable)) {
					return converter; 
				}
			}	
		}
		return null;
	}
	/**
	 * Método de recuperação do campo operator
	 *
	 * @return valor do campo operator
	 */
	@Navigable(priority=0)
	public SecurityOperator getOperator() {
		return operator;
	}
	/**
	 * Valor de operator atribuído a operator
	 *
	 * @param operator Atributo da Classe
	 */
	public void setOperator(SecurityOperator operator) {
		this.operator = operator;
	}
	/**
	 * Método de recuperação do campo tasksConfig
	 *
	 * @return valor do campo tasksConfig
	 */
	@Navigable
	public MacadameList<InteropTask> getTasksConfig() {
		return tasksConfig;
	}
	/**
	 * 
	 * TODO Descrição do Método
	 * @return
	 */
	
	public MacadameList<InteropTask> getAllTasksConfig() {
		MacadameList<InteropTask> allTaskConfig = new MacadameArrayList<InteropTask>();
		if(getParent()!=null){
			allTaskConfig.addAll(getParent().getTasksConfig());
		}
		if(tasksConfig!=null ){
			allTaskConfig.addAll(tasksConfig);
		}
		return allTaskConfig;
	}
	
	
	/**
	 * Valor de tasksConfig atribuído a tasksConfig
	 *
	 * @param tasksConfig Atributo da Classe
	 */
	public void setTasksConfig(MacadameList<InteropTask> tasksConfig) {
		this.tasksConfig = tasksConfig;
	}
	/**
	 * Método de recuperação do campo parent
	 *
	 * @return valor do campo parent
	 */
	public AbstractInteropConfig getParent() {
		return parent;
	}
	/**
	 * Valor de parent atribuído a parent
	 *
	 * @param parent Atributo da Classe
	 */
	public void setParent(AbstractInteropConfig parent) {
		this.parent = parent;
	}
	/**
	 * Método de recuperação do campo site
	 *
	 * @return valor do campo site
	 */
	public Site getSite() {
		return site;
	}
	/**
	 * Valor de site atribuído a site
	 *
	 * @param site Atributo da Classe
	 */
	public void setSite(Site site) {
		this.site = site;
	}
	/**
	 * Método de recuperação do campo firstActivationDate
	 *
	 * @return valor do campo firstActivationDate
	 */
	public Calendar getFirstActivationDate() {
		return firstActivationDate;
	}
	/**
	 * Valor de firstActivationDate atribuído a firstActivationDate
	 *
	 * @param firstActivationDate Atributo da Classe
	 */
	public void setFirstActivationDate(Calendar firstActivationDate) {
		this.firstActivationDate = firstActivationDate;
	}
	/**
	 * Recupera o InteropSetting vigente para o code informado
	 * @param aCode
	 * @param aScheduleDate
	 * @return
	 */
	public InteropSetting getInteropSettingByCodeForEffectiveDate(final String aCode, final Calendar aScheduleDate){
		ArrayList<InteropSettingSchedule> interopSets = new ArrayList<InteropSettingSchedule>();
		for (InteropSettingSchedule schedule : this.settingSchedules) {
			if(schedule.getSettingBySettingType(aCode)!=null){
				interopSets.add(schedule);
			}
		}
		Collections.sort(interopSets, new Comparator<InteropSettingSchedule>() {
			public int compare(InteropSettingSchedule schedule1, InteropSettingSchedule schedule2) {
				return schedule2.getEffectiveDate().compareTo(schedule1.getEffectiveDate());
			}
		});
		
		for (InteropSettingSchedule interopSettingSchedule : interopSets) {
			if(interopSettingSchedule.getEffectiveDate() != null &&
					interopSettingSchedule.getEffectiveDate().compareTo(aScheduleDate)<=0){
				return interopSettingSchedule.getSettingBySettingType(aCode);
			}
		}
		return null;
	}

	/**
	 * Método de recuperação do campo allowSendViolator
	 *
	 * @return valor do campo allowSendViolator
	 */
	public Boolean getAllowSendViolator() {
		return allowSendViolator;
	}

	/**
	 * Valor de allowSendViolator atribuído a allowSendViolator
	 *
	 * @param allowSendViolator Atributo da Classe
	 */
	public void setAllowSendViolator(Boolean allowSendViolator) {
		this.allowSendViolator = allowSendViolator;
	}

	/**
	 * Método de recuperação do campo itMessages
	 *
	 * @return valor do campo itMessages
	 */
	@Navigable
	public MacadameList<MediaStatusSetting> getItMessages() {
		return itMessages;
	}

	/**
	 * Valor de itMessages atribuído a itMessages
	 *
	 * @param itMessages Atributo da Classe
	 */
	public void setItMessages(MacadameList<MediaStatusSetting> itMessages) {
		this.itMessages = itMessages;
	}

	
}
