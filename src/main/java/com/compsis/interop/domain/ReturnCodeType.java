/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 25/03/2013<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.domain.DomainSpecificEntity;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 25/03/2013 - @author wcarvalho - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class ReturnCodeType extends DomainSpecificEntity{

	/**
	 * Retorno do lote BATCH_RETURN : RLOT
	 */
	public static final String BATCH_RETURN = "RLOT";
	
	/**
	 * Retorno da trasanção TRANSACTION_RETURN : RTRN
	 */
	public static final String TRANSACTION_RETURN= "RTRN";
	
	public ReturnCodeType() {
	
	}
	public ReturnCodeType(String code) {
		this.setCode(code);
	}
}
