/*
 * COMPSIS - Computadores e Sistemas Ind. e Com. LTDA<br>
 * SICAT - Interop<br>
 *
 * Data de Criação: 13/11/2012<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.domain.MacadameEntity;
import com.compsis.media.domain.MediaStatus;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Classe que representa a configurações das mensagens que irão ser baixadas para o CV
 * Também define se o status permite ou não isenção <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 13/11/2012 - @author wcarvalho - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class MediaStatusSetting extends MacadameEntity implements Comparable<MediaStatusSetting>{

	/**
	 * Representa a mensagem que será apresentada no IT do cv
	 */
	private String message;
	
	/**
	 * Representa o status 
	 */	
	private MediaStatus mediaStatus;
	
	/**
	 * Representa a configuração do meio de pagamento
	 */
	private AbstractInteropConfig interopConfig;
		
	@Override
	public int compareTo(MediaStatusSetting entity) {
		if(this.mediaStatus != null && entity.getMediaStatus() != null){
			return this.mediaStatus.compareTo(entity.getMediaStatus());		
		}
		return -1;
	}

	/**
	 * Método de recuperação do campo message
	 *
	 * @return valor do campo message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Valor de message atribuído a message
	 *
	 * @param message Atributo da Classe
	 */
	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * Método de recuperação do campo mediaStatus
	 *
	 * @return valor do campo mediaStatus
	 */
	public MediaStatus getMediaStatus() {
		return mediaStatus;
	}

	/**
	 * Valor de mediaStatus atribuído a mediaStatus
	 *
	 * @param mediaStatus Atributo da Classe
	 */
	public void setMediaStatus(MediaStatus mediaStatus) {
		this.mediaStatus = mediaStatus;
	}

	/**
	 * Método de recuperação do campo interopConfig
	 *
	 * @return valor do campo interopConfig
	 */
	public AbstractInteropConfig getInteropConfig() {
		return interopConfig;
	}

	/**
	 * Valor de interopConfig atribuído a interopConfig
	 *
	 * @param interopConfig Atributo da Classe
	 */
	public void setInteropConfig(AbstractInteropConfig interopConfig) {
		this.interopConfig = interopConfig;
	}
	
}
