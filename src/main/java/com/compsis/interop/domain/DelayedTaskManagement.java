/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto Interop - Domain<br>
 *
 * Data de Criação: 20/02/2014<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import java.util.Calendar;

import com.compsis.macadame.domain.MacadameObject;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Wrapper para notificações do tipo atraso. <br>
 * Irá permitir gerenciamento das notificações com base nas últimas execuções
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 20/02/2014 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class DelayedTaskManagement extends MacadameObject{
	private Calendar lastNotification;
	private DelayedTaskSetting delayedTaskSetting;
	/** 
	 * Construtor padrão da classe
	 */
	public DelayedTaskManagement(final DelayedTaskSetting aDelayedTaskSetting) {
		this.delayedTaskSetting = aDelayedTaskSetting;
	}
	
	/**
	 * Método de recuperação do campo delayedTaskSetting
	 *
	 * @return valor do campo delayedTaskSetting
	 */
	public DelayedTaskSetting getDelayedTaskSetting() {
		return delayedTaskSetting;
	}
	
	/**
	 * Valor de lastNotification atribuído a lastNotification
	 *
	 * @param lastNotification Atributo da Classe
	 */
	public void setLastNotification(Calendar lastNotification) {
		this.lastNotification = lastNotification;
	}
	
	
	/**
	 * Método de recuperação do campo lastNotification
	 *
	 * @return valor do campo lastNotification
	 */
	public Calendar getLastNotification() {
		return lastNotification;
	}
}
