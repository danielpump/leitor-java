/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 17/10/2012<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.domain.DomainSpecificEntity;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Classe de domínio para Operações do lote
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 17/10/2012 - @author ntrivedi - Primeiva versão da classe. <br>
 * 14/01/2013 - @author Lucas Israel - Adicionado constantes para facilitar consulta <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class BatchOperation extends DomainSpecificEntity {
	
	public BatchOperation(){}
	
	public BatchOperation(String aCode) {
	 setCode(aCode);
	}

	//Contantes de valores do campo ID
	/**
	 * ID da operação de canelamento
	 */
	public static final Long BATCH_CANCEL_ID = Long.valueOf(7);

	
	//Constantes de valores do campo code
	/**
	 * Gerar Lote - Valor: GER
	 */
	public static final String GENERATION = "GER";

	/**
	 * Enviar lote - Valor: ENV
	 */
	public static final String SEND = "ENV";
	
	/**
	 * Receber retorno de envio (Protocolo Técnico) - Valor: RTL
	 */
	public static final String RECEIVE_FEEDBACK = "RTL";
	
	/**
	 * Receber retorno financeiro (Protocolo Financeiro) - Valor: RTF
	 */
	public static final String PAYBACK = "RTF";
	
	/**
	 * Receber passagens de vale pedágio - Valor: RVP
	 */
	public static final String RECEIVE_TICKETS_WORTH_TOLL = "RVP";
	
	/**
	 * Regerar lote - Valor: RGR
	 */
	public static final String REGENERATE_BATCH = "RGR";
	
	/**
	 * Regravar lote - Valor: REG
	 */
	public static final String REWRITE_BATCH = "REG";
	
	/**
	 * Cancelar lote - Valor: CAN
	 */
	public static final String BATCH_CANCEL = "CAN";
	
	/**
	 * Descontar taxa de serviço - Valor: DTS
	 */
	public static final String CASHING_SERVICE_CHARGE = "DTS";
	
}
