/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto SICAT - Interop<br>
 *
 * Data de Criação: 29/05/2013<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import java.io.File;

import com.compsis.macadame.collections.Collections;
import com.compsis.macadame.collections.MacadameArrayList;
import com.compsis.macadame.collections.MacadameHashSet;
import com.compsis.macadame.collections.MacadameList;
import com.compsis.macadame.collections.MacadameSet;
import com.compsis.macadame.domain.MacadameFile;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Classe que representa arquivos de informações trocados entre concessionárias e OSAs. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 29/05/2013 - @author Naresh Trivedi - Primeiva versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings({ "unchecked", "serial" })
public class InteropFile extends MacadameFile{
	
	/**
	 * Representa uma mensagem de erro referente a validações no arquivo
	 */
	private String errorMessage;

	/**
	 * Converte vetor de java.util.File para MacadameHashSet<InteropFile>
	 * @param aFiles
	 * @return
	 */
	public static MacadameSet<InteropFile> asSet(final File[] aFiles){
		MacadameHashSet<InteropFile> sets = Collections.newHashSet();
		for (File file : aFiles) {
			sets.add(new InteropFile(file));
		}
		return sets;
	}
	
	/**
	 * Converte vetor de java.util.File para MacadameArrayList<InteropFile>
	 * @param aFiles
	 * @return
	 */
	public static MacadameList<InteropFile> asList(final File[] aFiles){
		MacadameArrayList<InteropFile> list = Collections.newArrayList();
		for (File file : aFiles) {
			list.add(new InteropFile(file));
		}
		return list;
	}
	
	private Long quantity;
	
	/**
	 * Sequencial do arquivo.
	 */
	private Long sequential;

	/**
	 * Método de recuperação do campo sequential
	 *
	 * @return valor do campo sequential
	 */
	public Long getSequential() {
		return sequential;
	}

	/**
	 * Valor de sequential atribuído a sequential
	 *
	 * @param sequential Atributo da Classe
	 */
	public void setSequential(Long sequential) {
		this.sequential = sequential;
	}

	public InteropFile(final File aFile){
		super(aFile);
	}
	
	public InteropFile(){}

	public InteropFile(Long sequencial){
		this.sequential = sequencial;
	}
	
	public InteropFile(Long sequencial, File aFile){
		super(aFile);
		this.sequential = sequencial;
	
	}

	/**
     * Creates a new <code>MacadameFile</code> instance from a parent abstract
     * pathname and a child pathname string.
     *
     * <p> If <code>parent</code> is <code>null</code> then the new
     * <code>File</code> instance is created as if by invoking the
     * single-argument <code>File</code> constructor on the given
     * <code>child</code> pathname string.
     *
     * <p> Otherwise the <code>parent</code> abstract pathname is taken to
     * denote a directory, and the <code>child</code> pathname string is taken
     * to denote either a directory or a file.  If the <code>child</code>
     * pathname string is absolute then it is converted into a relative
     * pathname in a system-dependent way.  If <code>parent</code> is the empty
     * abstract pathname then the new <code>File</code> instance is created by
     * converting <code>child</code> into an abstract pathname and resolving
     * the result against a system-dependent default directory.  Otherwise each
     * pathname string is converted into an abstract pathname and the child
     * abstract pathname is resolved against the parent.
     *
     * @param   parent  The parent abstract pathname
     * @param   child   The child pathname string
     * @throws  NullPointerException
     *          If <code>child</code> is <code>null</code>
     */
	public InteropFile(File parent, String child) {
		super(parent, child);
	}
	
	/**
	 * Creates a new <code>MacadameFile</code> instance by converting the given
     * pathname string into an abstract pathname.  If the given string is
     * the empty string, then the result is the empty abstract pathname.
     *
     * @param   pathname  A pathname string
     * @throws  NullPointerException
     *          If the <code>pathname</code> argument is <code>null</code>
	 */
	public InteropFile(String pathname) {
		super(pathname);
	}
	
	/**
     * Creates a new <code>File</code> instance from a parent pathname string
     * and a child pathname string.
     *
     * <p> If <code>parent</code> is <code>null</code> then the new
     * <code>File</code> instance is created as if by invoking the
     * single-argument <code>File</code> constructor on the given
     * <code>child</code> pathname string.
     *
     * <p> Otherwise the <code>parent</code> pathname string is taken to denote
     * a directory, and the <code>child</code> pathname string is taken to
     * denote either a directory or a file.  If the <code>child</code> pathname
     * string is absolute then it is converted into a relative pathname in a
     * system-dependent way.  If <code>parent</code> is the empty string then
     * the new <code>File</code> instance is created by converting
     * <code>child</code> into an abstract pathname and resolving the result
     * against a system-dependent default directory.  Otherwise each pathname
     * string is converted into an abstract pathname and the child abstract
     * pathname is resolved against the parent.
     *
     * @param   parent  The parent pathname string
     * @param   child   The child pathname string
     * @throws  NullPointerException
     *          If <code>child</code> is <code>null</code>
     */
	public InteropFile(String parent, String child) {
		super(parent, child);
	}

	/**
	 * Método de recuperação do campo quantity
	 *
	 * @return valor do campo quantity
	 */
	public Long getQuantity() {
		return quantity;
	}

	/**
	 * Valor de quantity atribuído a quantity
	 *
	 * @param quantity Atributo da Classe
	 */
	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	/**
	 * Método de recuperação do campo errorMessage
	 *
	 * @return valor do campo errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * Valor de errorMessage atribuído a errorMessage
	 *
	 * @param errorMessage Atributo da Classe
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	

}
