package com.compsis.interop.domain;

import com.compsis.macadame.domain.MacadameEntity;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Representa uma versão do sistema com o dia da sua atualização. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 14/04/2014 - @author Bruno Coan - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
public class SystemVersion extends MacadameEntity {

	private String version;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
}
