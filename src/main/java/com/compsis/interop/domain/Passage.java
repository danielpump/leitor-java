/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto SICAT - Interop <br>
 *
 * Data de Cria��o: 19/09/2012<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import java.math.BigDecimal;

import com.compsis.vehicle.domain.Vehicle;
import com.compsis.vehicle.domain.VehicleClass;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Respresenta uma passagem de Ped�gio a ser enviada a um gestor de meio de pagamento
 * eletronico. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 19/09/2012 - @author Naresh Trivedi - Primeiva vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class Passage extends Transaction {
	
	/** Tipo da transação **/
	private PassageType passageType;
	/** Tipo de correção da transação **/
	private CorrectionMode correctionMode;
	private Integer vehicleSpeed;
	private BigDecimal nominalValue;
	/** Categoria considerada para cobrança - Categoria Corrigida */
	private VehicleClass consideredVehicleClass;
	/** Categoria considerada para cobrança - Categoria detectada */
	private VehicleClass detectedVehicleClass;
	/** Categoria classificada pelo operador ou identificada na leitura do tag - Categoria Classificada*/
	private VehicleClass classifiedVehicleClass;
	
	private String licensePlate;
	/** Indica se a transação já foi manipulada **/
	private Boolean changed;	
	
	private Vehicle vehicle;
	/**
	 * Método de recuperação do campo detectedVehicleClass
	 *
	 * @return valor do campo detectedVehicleClass
	 */
	public VehicleClass getDetectedVehicleClass() {
		return detectedVehicleClass;
	}
	/**
	 * Valor de detectedVehicleClass atribuído a detectedVehicleClass
	 *
	 * @param detectedVehicleClass Atributo da Classe
	 */
	public void setDetectedVehicleClass(VehicleClass detectedVehicleClass) {
		this.detectedVehicleClass = detectedVehicleClass;
	}

	/**
	 * Método de recuperação do campo licensePlate
	 *
	 * @return valor do campo licensePlate
	 */
	public String getLicensePlate() {
		return licensePlate;
	}
	/**
	 * Valor de aLicensePlate atribuído a licensePlate
	 *
	 * @param aLicensePlate Atributo da Classe
	 */
	public void setLicensePlate(String aLicensePlate) {
		licensePlate = aLicensePlate;
	}
	/**
	 * Método de recuperação do campo passageType
	 *
	 * @return valor do campo passageType
	 */
	public PassageType getPassageType() {
		return passageType;
	}
	/**
	 * Valor de aPassageType atribuído a passageType
	 *
	 * @param aPassageType Atributo da Classe
	 */
	public void setPassageType(PassageType aPassageType) {
		passageType = aPassageType;
	}
	/**
	 * Método de recuperação do campo correctionMode
	 *
	 * @return valor do campo correctionMode
	 */
	public CorrectionMode getCorrectionMode() {
		return correctionMode;
	}
	/**
	 * Valor de aCorrectionMode atribuído a correctionMode
	 *
	 * @param aCorrectionMode Atributo da Classe
	 */
	public void setCorrectionMode(CorrectionMode aCorrectionMode) {
		correctionMode = aCorrectionMode;
	}
	/**
	 * Método de recuperação do campo vehicleSpeed
	 *
	 * @return valor do campo vehicleSpeed
	 */
	public Integer getVehicleSpeed() {
		return vehicleSpeed;
	}
	/**
	 * Valor de aVehicleSpeed atribuído a vehicleSpeed
	 *
	 * @param aVehicleSpeed Atributo da Classe
	 */
	public void setVehicleSpeed(Integer aVehicleSpeed) {
		vehicleSpeed = aVehicleSpeed;
	}
	/**
	 * Método de recuperação do campo nominalValue
	 *
	 * @return valor do campo nominalValue
	 */
	public BigDecimal getNominalValue() {
		return nominalValue;
	}
	/**
	 * Valor de aNominalValue atribuído a nominalValue
	 *
	 * @param aNominalValue Atributo da Classe
	 */
	public void setNominalValue(BigDecimal aNominalValue) {
		nominalValue = aNominalValue;
	}
	/**
	 * Método de recuperação do campo consideredVehicleClass
	 *
	 * @return valor do campo consideredVehicleClass
	 */
	public VehicleClass getConsideredVehicleClass() {
		return consideredVehicleClass;
	}
	/**
	 * Valor de aConsideredVehicleClass atribuído a consideredVehicleClass
	 *
	 * @param aConsideredVehicleClass Atributo da Classe
	 */
	public void setConsideredVehicleClass(VehicleClass aConsideredVehicleClass) {
		consideredVehicleClass = aConsideredVehicleClass;
	}
	/**
	 * Método de recuperação do campo classifiedVehicleClass
	 *
	 * @return valor do campo classifiedVehicleClass
	 */
	public VehicleClass getClassifiedVehicleClass() {
		return classifiedVehicleClass;
	}
	/**
	 * Valor de aClassifiedVehicleClass atribuído a classifiedVehicleClass
	 *
	 * @param aClassifiedVehicleClass Atributo da Classe
	 */
	public void setClassifiedVehicleClass(VehicleClass aClassifiedVehicleClass) {
		classifiedVehicleClass = aClassifiedVehicleClass;
	}
	
	
	/** 
	 * Altera a passagem indicando que ela já foi manipulada.
	 */
	public void manipulated(){
		changed = Boolean.TRUE;
	}
	
	/** 
	 * Altera a passagem indicando que ela não foi manipulada.
	 */
	public void unManipulated(){
		changed = Boolean.FALSE;
	}
	
	/** 
	 * Retorna um valor {@link Boolean} indicando se a passagem já foi.
	 * @return
	 */
	public Boolean isManipulated(){
		return changed;
	}
	
	
	/**
	 * Método de recuperação do campo changed
	 * Indica se a transação já foi manipulada
	 * @return valor do campo changed
	 */
	public Boolean getChanged() {
		return changed;
	}
	
	/**
	 * Valor de changed atribuído a changed
	 *
	 * @param changed Atributo da Classe
	 */
	public void setChanged(Boolean changed) {
		this.changed = changed;
	}
	public Vehicle getVehicle() {
		return vehicle;
	}
	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}
	
}
