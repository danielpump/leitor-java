/*
 * COMPSIS - Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 17/10/2012<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.domain.DomainSpecificEntity;
import com.compsis.macadame.domain.MacadameEntity;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentaão da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 17/10/2012 - @author ntrivedi - Primeiva versão  da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class InteropSetting extends DomainSpecificEntity implements Cloneable{

	private String value;
	
	private InteropSettingType interopSettingType;
	
	private InteropSettingSchedule interopSettingSchedule;
	/**
	 * Método de recuperação do campo interopSettingType
	 *
	 * @return valor do campo interopSettingType
	 */
	public InteropSettingType getInteropSettingType() {
		return interopSettingType;
	}
	/**
	 * Valor de interopSettingType atribuído a interopSettingType
	 *
	 * @param interopSettingType Atributo da Classe
	 */
	public void setInteropSettingType(InteropSettingType interopSettingType) {
		this.interopSettingType = interopSettingType;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String aValue) {
		value = aValue;
	}
	/**
	 * Método de recuperação do campo interopSettingSchedule
	 *
	 * @return valor do campo interopSettingSchedule
	 */
	public InteropSettingSchedule getInteropSettingSchedule() {
		return interopSettingSchedule;
	}
	/**
	 * Valor de interopSettingSchedule atribuído a interopSettingSchedule
	 *
	 * @param interopSettingSchedule Atributo da Classe
	 */
	public void setInteropSettingSchedule(
			InteropSettingSchedule interopSettingSchedule) {
		this.interopSettingSchedule = interopSettingSchedule;
	}
	
	public InteropSetting clone(){
		InteropSetting cloned = new InteropSetting();
		cloned.setId(getId());
		cloned.setInsertionDate(getInsertionDate());
		cloned.setInteropSettingSchedule(getInteropSettingSchedule());
		cloned.setValue(getValue());
		if(getInteropSettingType() != null){
			cloned.setInteropSettingType(getInteropSettingType().clone());
		}
		return cloned;
		
		
	}
	
	public Boolean asBoolean(){
		return this.value.equals("1"); 
	}
	
	public void setAsBoolean(Boolean valueAsBoolean){
		this.value = valueAsBoolean ? "1" : "0"; 
	}
	
	
	
}
