/*
 * COMPSIS - Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto InteropConfig - DOMAIN<br>
 *
 * Data de Criação: 21/09/2012<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.site.domain.Unit;


/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Entidade de configuração com meio de pagamento protocolo e Unit
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 21/09/2012 - @author ntrivedi - Primeira versão da classe. <br>
 * 13/12/2012 - @author mscaldaferro - AP-3158. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class UnitInteropConfig extends AbstractInteropConfig {
	public Unit getSite(){
		return (Unit) super.getSite();
	}
}
