/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>	
 * Produto SICAT - Interop <br>
 *
 * Data de Cria��o: 19/09/2012<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import java.io.File;
import java.math.BigDecimal;
import java.util.Calendar;

import com.compsis.interop.domain.interfaces.BatchTransactionCalculator;
import com.compsis.macadame.collections.MacadameHashSet;
import com.compsis.macadame.collections.MacadameList;
import com.compsis.macadame.collections.MacadameSet;
import com.compsis.macadame.domain.MacadameEntity;
import com.compsis.macadame.navigator.annotation.Navigable;
import com.compsis.macadame.security.domain.SecurityOperator;
import com.compsis.site.domain.Unit;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Lote de opera��es financeiras a serem enviadas ao gestor do meio de pagamento. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 19/09/2012 - @author Naresh Trivedi - Primeira vers�o da classe. <br>
 * 13/12/2012 - @author mscaldaferro - AP-3158. <br>
 * 06/06/2013 - @author Daniel Ferraz - Refactor do modelo atual nesta data. <br>
 * 16/09/2013 - Lucas Israel - Adicionado execução do detalhe corrente.
 *<br>
 *<br> 
 * LISTA DE CLASSES INTERNAS: <br>
 */
@SuppressWarnings("serial")
public class Batch extends Clearing {
	private File file;
	/**
	 * Atributo que representa o versionamento do registro.
	 * Utilizado para fins de concorrência
	 */
	private int batchVersion;
	
	/**
	 * Modo de execução do caso de uso neste lote
	 */
	private ExecutionMode executionMode;
	
	/**
	 * Sequencial do lote externo.
	 */
	private String externalCode;
	/**
	 * Sequencial do lote interno.
	 */
	private String internalCode;
	/**
	 * Valor total de transações no lote.
	 */
	private BigDecimal amount;
	/**
	 * Valor total de vale pedágio no lote.
	 */
	private BigDecimal ticketAmount;
	
	/**
	 * Quantidade de transações no lote.
	 */
	private Integer quantity;
	
	/**
	 * Dia das transações que compõem um lote
	 */
	private Calendar transactionDay;

	/**
	 * Dia das transações que compõem um lote na forma numérica.
	 */
	private Long transactionDayNumeric;
	/**
	 * Transa��es pertencentes ao lote.
	 */
	private MacadameSet<Transaction> transactions;
	/**
	 * Pra�a das transa��es contidas no lote,
	 * pode ser uma pra�a ou uma pista.
	 */
	private Unit unit;
	/**
	 * Status do lote.
	 */
	private BatchStatus status;
	/**
	 * C�digo de retorno do lote.
	 */
	private ReturnCode returnCode;
	/**
	 * Meio de pagamento do lote.
	 */
	private AbstractInteropConfig interopConfig;
	/**
	 * Hist�rico das opera��es realizadas com o lote.
	 */
	private MacadameSet<BatchHistory> operationsHistory;
	
	private MacadameSet<BatchPayment> paymentHistory;
	
	/**
	 * Retorno financeiro do lote
	 */
	private FinancialReturn financialReturn;
	
	/**
	 * Operador que está realizando o caso de uso no lote
	 */
	private SecurityOperator operator;
	

	private BatchValues batchValues;

	/**
	 * Detalhe da execução da tarefa para o lote <br>
	 * Não persistido.
	 */
	private BatchExecutionDetail currentExecutionDetail;
	
	/** 
	 * Retorna o registro mais recente referente ao código informado
	 * @param statusCode - {@link String}:Codigo de status do lote
	 * @return {@link BatchHistory}
	 */
	public BatchHistory getMostRecentHistoryByOperationCode(String operationCode){
		BatchHistory history = null;
		if(getOperationsHistory() != null && !getOperationsHistory().isEmpty()){
			for(BatchHistory op: getOperationsHistory()){
				if(op.getOperation().getCode().equals(operationCode)){
					if(history == null){
						history = op;
					}else if(history.getOperationDate().compareTo(op.getOperationDate()) > 0){
						history = op;
					}
				}
			}
		}
		return history;
	}	
	
		
	/**
	 * Hist�rico dos pagamentos do lote. 
	 */
	protected Batch () {
		this.transactions = new MacadameHashSet<Transaction>();
		this.operationsHistory = new MacadameHashSet<BatchHistory>();
		quantity = Integer.valueOf(0);
		batchValues = new BatchValues();
	}
	
	public static class Builder {
		
		private Batch batch;
		
		private BatchTransactionCalculator calculator;
		
		public Builder () {
			this.batch = new Batch();
		}
		
		public Batch build () {
			return this.batch;
		}
		
		public Builder withExternalCode (final String aExternalCode) {
			batch.externalCode = aExternalCode;
			return this;
		}
		
		public Builder from (final Unit aUnit) {
			batch.unit = aUnit;
			return this;
		}
		
		public Builder withTransactions (final MacadameList<Transaction> aTransactions) {
			calculator.calculateTotalTransaction(batch, aTransactions);
			batch.quantity = aTransactions.size();
			batch.setTransactionDay(aTransactions.get(0).getTransactionDate());
			batch.transactions = new MacadameHashSet<Transaction>(aTransactions); 
			return this;
		}
		
		public Builder withStatus (final BatchStatus aStatus) {
			batch.status = aStatus;
			return this;
		}
		
		public Builder forInteropConfig (final AbstractInteropConfig aInteropConfig) {
			batch.interopConfig = aInteropConfig;
			return this;
		}
		public Builder generatedBy (final SecurityOperator operator) {
			batch.operator = operator;
			return this;
		}
		
		public Builder withTransactionsCalculator (final BatchTransactionCalculator calculator) {
			this.calculator = calculator;
			return this;
		}
		
	}
	
	/**
	 * M�todo de recupera��o do campo externalCode
	 *
	 * @return valor do campo externalCode
	 */
	public String getExternalCode() {
		return externalCode;
	}
	/**
	 * Valor de aExternalCode atribu�do a externalCode
	 *
	 * @param aExternalCode Atributo da Classe
	 */
	public void setExternalCode(String aExternalCode) {
		externalCode = aExternalCode;
	}
	/**
	 * M�todo de recupera��o do campo internalCode
	 *
	 * @return valor do campo internalCode
	 */
	public String getInternalCode() {
		return internalCode;
	}
	/**
	 * Valor de aInternalCode atribu�do a internalCode
	 *
	 * @param aInternalCode Atributo da Classe
	 */
	public void setInternalCode(String aInternalCode) {
		internalCode = aInternalCode;
	}
	/**
	 * M�todo de recupera��o do campo amount
	 *
	 * @return valor do campo amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	/**
	 * Valor de aAmount atribu�do a amount
	 *
	 * @param aAmount Atributo da Classe
	 */
	public void setAmount(BigDecimal aAmount) {
		amount = aAmount;
	}
	
	/**
	 * Adiciona o valor passado ao total do lote
	 *
	 * @return valor do campo amount
	 */
	public void addAmountValue(BigDecimal amount) {
		if(this.amount == null){
			this.amount = BigDecimal.ZERO;
		}
		this.amount = this.amount.add(amount);
	}
	
	/**
	 * M�todo de recupera��o do campo quantity
	 *
	 * @return valor do campo quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}
	/**
	 * Valor de aQuantity atribu�do a quantity
	 *
	 * @param aQuantity Atributo da Classe
	 */
	public void setQuantity(Integer aQuantity) {
		quantity = aQuantity;
	}
	/**
	 * M�todo de recupera��o do campo transactions
	 *
	 * @return valor do campo transactions
	 */
	@Navigable
	public MacadameSet<Transaction> getTransactions() {
		return transactions;
	}
	/**
	 * Valor de aTransactions atribu�do a transactions
	 *
	 * @param aTransactions Atributo da Classe
	 */
	public void setTransactions(MacadameSet<Transaction> aTransactions) {
		transactions = aTransactions;
	}
	/**
	 * M�todo de recupera��o do campo unit
	 *
	 * @return valor do campo unit
	 */
	public Unit getUnit() {
		return unit;
	}
	/**
	 * Valor de aPlaza atribu�do a plaza
	 *
	 * @param aPlaza Atributo da Classe
	 */
	public void setUnit(Unit aUnit) {
		unit = aUnit;
	}
	/**
	 * M�todo de recupera��o do campo status
	 *
	 * @return valor do campo status
	 */
	public BatchStatus getStatus() {
		return status;
	}
	/**
	 * Valor de aStatus atribu�do a status
	 *
	 * @param aStatus Atributo da Classe
	 */
	public void setStatus(BatchStatus aStatus) {
		status = aStatus;
	}
	/**
	 * M�todo de recupera��o do campo returnCode
	 *
	 * @return valor do campo returnCode
	 */
	public ReturnCode getReturnCode() {
		return returnCode;
	}
	/**
	 * Valor de aReturnCode atribu�do a returnCode
	 *
	 * @param aReturnCode Atributo da Classe
	 */
	public void setReturnCode(ReturnCode aReturnCode) {
		returnCode = aReturnCode;
	}
	/**
	 * M�todo de recupera��o do campo interopConfig
	 *
	 * @return valor do campo interopConfig
	 */
	public AbstractInteropConfig getInteropConfig() {
		return interopConfig;
	}
	/**
	 * Valor de aInteropConfig atribu�do a interopConfig
	 *
	 * @param aInteropConfig Atributo da Classe
	 */
	public void setInteropConfig(AbstractInteropConfig aInteropConfig) {
		interopConfig = aInteropConfig;
	}
	
	/**
	 * Método de recuperação do campo operationsHistory
	 *
	 * @return valor do campo operationsHistory
	 */
	@Navigable
	public MacadameSet<BatchHistory> getOperationsHistory() {
		return operationsHistory;
	}
	/**
	 * Valor de aOperationsHistory atribu�do a operationsHistory
	 *
	 * @param aOperationsHistory Atributo da Classe
	 */
	public void setOperationsHistory(MacadameSet<BatchHistory> aOperationsHistory) {
		operationsHistory = aOperationsHistory;
	}
	/** 
	 * Adiciona uma transa��o a lista de transa��es do lote.
	 * @param aTransaction
	 */
	public void addTransaction(final Transaction aTransaction) {
		amount = amount.add(aTransaction.getValue());
		quantity++;		
		transactions.add(aTransaction);
	}
	/**
	 * M�todo de recupera��o do campo paymentHistory
	 *
	 * @return valor do campo paymentHistory
	 */
	public MacadameSet<BatchPayment> getPaymentHistory() {
		return paymentHistory;
	}
	/**
	 * Valor de paymentHistory atribu�do a paymentHistory
	 *
	 * @param paymentHistory Atributo da Classe
	 */
	public void setPaymentHistory(MacadameSet<BatchPayment> paymentHistory) {
		this.paymentHistory = paymentHistory;
	}

	/**
	 * Valor de financialReturn atribuído a financialReturn
	 * Retorno financeiro do lote
	 * @param financialReturn Atributo da Classe
	 */
	public void setFinancialReturn(FinancialReturn financialReturn) {
		this.financialReturn = financialReturn;
	}
	
	/**
	 * Método de recuperação do campo financialReturn
	 * Retorno financeiro do lote
	 * @return valor do campo financialReturn
	 */
	public FinancialReturn getFinancialReturn() {
		return financialReturn;
	}
	
	/**
	 * Valor de operator atribuído a operator
	 * Operador que está realizando o caso de uso no lote
	 * @param operator Atributo da Classe
	 */
	public void setOperator(SecurityOperator operator) {
		this.operator = operator;
	}
	
	/**
	 * Método de recuperação do campo operator
	 * Operador que está realizando o caso de uso no lote
	 * @return valor do campo operator
	 */
	public SecurityOperator getOperator() {
		return operator;
	}
	
	/**
	 * Valor de executionMode atribuído a executionMode
	 * Modo de execução do caso de uso neste lote
	 * @param executionMode Atributo da Classe
	 */
	public void setExecutionMode(ExecutionMode executionMode) {
		this.executionMode = executionMode;
	}
	
	/**
	 * Método de recuperação do campo executionMode
	 * Modo de execução do caso de uso neste lote
	 * @return valor do campo executionMode
	 */
	public ExecutionMode getExecutionMode() {
		return executionMode;
	}
	/**
	 * Método de recuperação do campo ticketAmount
	 * Valor total de vale pedágio no lote.
	 * @return valor do campo ticketAmount
	 */
	public BigDecimal getTicketAmount() {
		return ticketAmount;
	}
	/**
	 * Valor de ticketAmount atribuído a ticketAmount
	 * Valor total de vale pedágio no lote.
	 * @param ticketAmount Atributo da Classe
	 */
	public void setTicketAmount(BigDecimal ticketAmount) {
		this.ticketAmount = ticketAmount;
	}
	/**
	 * Método de recuperação do campo transactionDay
	 *
	 * @return valor do campo transactionDay
	 */
	public Calendar getTransactionDay() {
		return transactionDay;
	}
	/**
	 * Valor de transactionDay atribuído a transactionDay
	 *
	 * @param transactionDay Atributo da Classe
	 */
	public void setTransactionDay(Calendar transactionDay) {
		this.transactionDay = transactionDay;
	}
	/**
	 * Método de recuperação do campo batchValues
	 *
	 * @return valor do campo batchValues
	 */
	public BatchValues getBatchValues() {
		return batchValues;
	}
	/** 
	 * Informa o valor da executionDetail da manipulação do lote
	 * @param currentExecutionDetail
	 */
	public void setCurrentExecutionDetail(
			BatchExecutionDetail aBatchExecutionDetail) {
		this.currentExecutionDetail = aBatchExecutionDetail;
	}
	/**
	 * Método de recuperação do campo currentExecutionDetail
	 * Detalhe da execução da tarefa atual sobre o lote
	 * @return valor do campo currentExecutionDetail
	 */
	public BatchExecutionDetail getCurrentExecutionDetail() {
		return currentExecutionDetail;
	}
	
	public void setBatchValues(BatchValues batchValues) {
		this.batchValues = batchValues;
	}


	/**
	 * Método de recuperação do campo batchVersion
	 *
	 * @return valor do campo batchVersion
	 */
	public int getBatchVersion() {
		return batchVersion;
	}

	/**
	 * Valor de batchVersion atribuído a batchVersion
	 *
	 * @param batchVersion Atributo da Classe
	 */
	public void setBatchVersion(int batchVersion) {
		this.batchVersion = batchVersion;
	}

	/**
	 * Valor de file atribuído a file
	 *
	 * @param file Atributo da Classe
	 */
	public void setFile(File file) {
		this.file = file;
	}
	
	
	/**
	 * Método de recuperação do campo file
	 *
	 * @return valor do campo file
	 */
	public File getFile() {
		return file;
	}


	/** 
	 * Getter de transactionDayNumeric
	 * @return
	 */
	public Long getTransactionDayNumeric() {
		return transactionDayNumeric;
	}


	/** 
	 * Setter de transactionDayNumeric
	 * @param transactionDayNumeric
	 */
	public void setTransactionDayNumeric(Long transactionDayNumeric) {
		this.transactionDayNumeric = transactionDayNumeric;
	}


	@Override
	public String getClearingCode() {
		return getInternalCode();
	}
	
	
}
