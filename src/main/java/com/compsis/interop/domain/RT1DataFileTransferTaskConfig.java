package com.compsis.interop.domain;

import com.compsis.macadame.collections.MacadameList;
import com.compsis.protocol.domain.FileInformation;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Classe com os dados especificos do arquivo RT1
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 05/06/2013 - @author Daniel Ferraz - Primeira vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
public class RT1DataFileTransferTaskConfig extends DataFileTransferTaskConfig {
	
	private FileInformation rt1File;
	private FileInformation cgmpTaxDiscountRecord;
	
	private MacadameList<String> lines;

	/**
	 * Método de recuperação do campo rt1File
	 *
	 * @return valor do campo rt1File
	 */
	public FileInformation getRt1File() {
		return rt1File;
	}

	/**
	 * Valor de rt1File atribuído a rt1File
	 *
	 * @param rt1File Atributo da Classe
	 */
	public void setRt1File(FileInformation rt1File) {
		this.rt1File = rt1File;
	}


	/**
	 * Método de recuperação do campo cgmpTaxDiscountRecord
	 *
	 * @return valor do campo cgmpTaxDiscountRecord
	 */
	public FileInformation getCgmpTaxDiscountRecord() {
		return cgmpTaxDiscountRecord;
	}

	/**
	 * Valor de cgmpTaxDiscountRecord atribuído a cgmpTaxDiscountRecord
	 *
	 * @param cgmpTaxDiscountRecord Atributo da Classe
	 */
	public void setCgmpTaxDiscountRecord(FileInformation cgmpTaxDiscountRecord) {
		this.cgmpTaxDiscountRecord = cgmpTaxDiscountRecord;
	}

	/**
	 * M�todo de recupera��o do campo lines
	 *
	 * @return valor do campo lines
	 */
	public MacadameList<String> getLines() {
		return lines;
	}

	/**
	 * Valor de lines atribu�do a lines
	 *
	 * @param lines Atributo da Classe
	 */
	public void setLines(MacadameList<String> lines) {
		this.lines = lines;
	}
	
	

}
