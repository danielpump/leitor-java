/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 25/03/2013<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Enum para representar os valores dos motivos de rejeição <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 25/03/2013 - @author wcarvalho - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public enum ReasonRejection {

	
	/**
	 * Arquivo aceito - code : 00
	 */
	FILE_ACCEPT("00"),
	
	/**
	 * Formato de registro não reconhecido - code : 01
	 */
	RECORD_FORMAT_INVALID("01"),
	
	/**
	 * Número de registros incorreto - code : 02
	 */
	NUMBER_INCORRECT_RECORDS("02"),
	
	/**
	 * 	Valor total incorreto - code : 03
	 */
	TOTAL_VALUE_INCORRECT ("03"),
	
	/**
	 * Outros - code : 04
	 */
	OTHERS("04"),
	
	/**
	 * Arquivo repetido  - code : 05
	 */
	ARCHIVE_REPEATED("05"),
	
	/**
	 * Passagens de praças especiais misturadas com praças normais - code : 06
	 */
	
	PLAZA_PASSAGES_SPECIAL_MIXED_NORMAL_PLAZA("06"),
	
	/**
	 * Passagens com ano ou mês diferente para praças especiais - code : 07
	 */
	
	PASSAGES_YEAR_MONTH_DIFFERENT_SPECIAL_PLAZAS("07"),
	
	/**
	 * Data de geração maior que a data de geração do arquivo code : 08
	 */
	GENERATION_DATE_MORE_FILE_GENERATION_DATE("08"),
	
	/**
	 * Data de passagem maior que a data de geração do arquivo  - code : 09
	 */
	PASSAGE_DATE_MORE_FILE_GENERATION_DATE("09"),
	
	/**
	 * Sequencia anterior não processada - code : 10
	 */
	SEQUENCE_PREVIOUS_UNPROCESSED("10"),
	
	/**
	 * Campo sequencia de registro duplicada - code : 11
	 */
	FIELD_DUPLICATE_RECORD_SEQUENCE("11"),
	
	/**
	 * Nome do arquivo fora do padrão estabelecido pelo gestor - code : 12
	 */
	FILE_NAME_NOT_STANDARD_MANAGER("12");


	private String code;
	
	
	private ReasonRejection(final String aCode) {
		this.code = aCode;
	}
	
	/**
	 * Método de recuperação do campo code
	 *
	 * @return valor do campo code
	 */
	public String getCode() {
		return code;
	}

		
}
