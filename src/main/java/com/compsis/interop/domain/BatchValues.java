package com.compsis.interop.domain;

import java.math.BigDecimal;

import com.compsis.macadame.entity.IEntity;
/**
 * 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 27/08/2013 - @author amachado - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
public class BatchValues implements IEntity {

	
	private static final long serialVersionUID = 5351413311326678318L;

	private Integer approvedQuantity;
	
	private BigDecimal approvedAmount;
	
	private Integer rejectedQuantity;
	
	private BigDecimal rejectedAmount;

	public BatchValues(){
		approvedQuantity = Integer.valueOf(0);
		rejectedQuantity = Integer.valueOf(0);
		approvedAmount = BigDecimal.ZERO;
		rejectedAmount = BigDecimal.ZERO;
	}
	
	/**
	 * Método de recuperação do campo approvedQuantity
	 *
	 * @return valor do campo approvedQuantity
	 */
	public Integer getApprovedQuantity() {
		return approvedQuantity;
	}
	/**
	 * Valor de approvedQuantity atribuído a approvedQuantity
	 *
	 * @param approvedQuantity Atributo da Classe
	 */
	public void setApprovedQuantity(Integer approvedQuantity) {
		this.approvedQuantity = approvedQuantity;
	}
	/**
	 * Método de recuperação do campo approvedAmount
	 *
	 * @return valor do campo approvedAmount
	 */
	public BigDecimal getApprovedAmount() {
		return approvedAmount;
	}
	/**
	 * Valor de approvedAmount atribuído a approvedAmount
	 *
	 * @param approvedAmount Atributo da Classe
	 */
	public void setApprovedAmount(BigDecimal approvedAmount) {
		this.approvedAmount = approvedAmount;
	}
	/**
	 * Método de recuperação do campo rejectedQuantity
	 *
	 * @return valor do campo rejectedQuantity
	 */
	public Integer getRejectedQuantity() {
		return rejectedQuantity;
	}
	/**
	 * Valor de rejectedQuantity atribuído a rejectedQuantity
	 *
	 * @param rejectedQuantity Atributo da Classe
	 */
	public void setRejectedQuantity(Integer rejectedQuantity) {
		this.rejectedQuantity = rejectedQuantity;
	}
	/**
	 * Método de recuperação do campo rejectedAmount
	 *
	 * @return valor do campo rejectedAmount
	 */
	public BigDecimal getRejectedAmount() {
		return rejectedAmount;
	}
	/**
	 * Valor de rejectedAmount atribuído a rejectedAmount
	 *
	 * @param rejectedAmount Atributo da Classe
	 */
	public void setRejectedAmount(BigDecimal rejectedAmount) {
		this.rejectedAmount = rejectedAmount;
	}
	
	
	
	
}
