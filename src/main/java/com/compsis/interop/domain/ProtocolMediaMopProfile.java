/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 17/10/2012<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.navigator.annotation.Navigable;
import com.compsis.media.domain.MediaConfiguration;
import com.compsis.nosqlmapping.annotations.IgnoreDynamicSearch;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 17/10/2012 - @author wcarvalho - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class ProtocolMediaMopProfile extends MediaConfiguration {

	/**
	 * Atributo que representa a entidade envio ou recebimento de cadastros.
	 */
	private ExecutionDetail executionDetail;
	
	private Boolean firstTransaction;
	
	/**
	 * id da media no modelo nosql
	 */
	@IgnoreDynamicSearch
	private String noSqlID;
	
	/**
	 * Sequencial do cadastro de media no modelo nosql
	 */
	@IgnoreDynamicSearch
	private String mediaRecordSequence;
	/**
	 * Linha recebida no arquivo. Sem tratamento de parser ou conversões
	 */
	private transient String lineReceivedRecord;
	/**
	 * Flag para indicar se a mídia foi alterada no recebimento. <br>
	 * Conveniente para teste uniátio e irá proporcionar um ganho
	 * muito alto de performance.
	 */
	private transient boolean changedMedia;
	/**
	 * Flag para indicar se a configuração da mídia foi alterada no recebimento. <br>
	 * Conveniente para teste uniátio e irá proporcionar um ganho
	 * muito alto de performance.
	 */
	private transient boolean changedMediaConfiguration;
	
	/**
	 * Dia do pagamento informado pelo gestor no contrato do cliente
	 */
	@IgnoreDynamicSearch
	private String paymentDay;
	
	/**
	 * Meio de pagamento informado pelo gestor no contrato do cliente <br> 
	 * CC=Cartão Crédito <br>
	 * DC=Cartão Débito <br>
	 * BL=Boleto Bancário <br>
	 */
	@IgnoreDynamicSearch
	private String methdoOfPayment;
	
	/**
	 * Forma de pagamento informado pelo gestor no contrado do cliente <br>
	 * PRE - Pré pagamento <br>
	 * POS - Pós pagamento <br>
	 */
	@IgnoreDynamicSearch
	private String paymentType;
	
	/**
	 * Método de recuperação do campo paymentDay
	 *
	 * @return valor do campo paymentDay
	 */
	public String getPaymentDay() {
		return paymentDay;
	}

	/**
	 * Valor de paymentDay atribuído a paymentDay
	 *
	 * @param paymentDay Atributo da Classe
	 */
	public void setPaymentDay(String paymentDay) {
		this.paymentDay = paymentDay;
	}

	/**
	 * Método de recuperação do campo methdoOfPayment
	 *
	 * @return valor do campo methdoOfPayment
	 */
	public String getMethdoOfPayment() {
		return methdoOfPayment;
	}

	/**
	 * Valor de methdoOfPayment atribuído a methdoOfPayment
	 *
	 * @param methdoOfPayment Atributo da Classe
	 */
	public void setMethdoOfPayment(String methdoOfPayment) {
		this.methdoOfPayment = methdoOfPayment;
	}

	/**
	 * Método de recuperação do campo paymentType
	 *
	 * @return valor do campo paymentType
	 */
	public String getPaymentType() {
		return paymentType;
	}

	/**
	 * Valor de paymentType atribuído a paymentType
	 *
	 * @param paymentType Atributo da Classe
	 */
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	/**
	 * Método de recuperação do campo executionDetail<br>
	 * Atributo que representa a o detalhe da execução de uma task
	 * @return valor do campo executionDetail
	 */
	@Navigable
	public ExecutionDetail getExecutionDetail() {
		return executionDetail;
	}

	/**
	 * Valor de executionDetail atribuído a executionDetail <br>
	 * Atributo que representa a o detalhe da execução de uma task
	 * @param executionDetail Atributo da Classe
	 */
	public void setExecutionDetail(ExecutionDetail executionDetail) {
		this.executionDetail = executionDetail;
	}

	/** 
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((executionDetail == null) ? 0 : executionDetail.hashCode());
		return result;
	}

	/** 
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof ProtocolMediaMopProfile)) {
			return false;
		}
		ProtocolMediaMopProfile other = (ProtocolMediaMopProfile) obj;
		if (executionDetail == null) {
			if (other.executionDetail != null) {
				return false;
			}
		} else if (!executionDetail.equals(other.executionDetail)) {
			return false;
		}
		return true;
	}

	/**
	 * Método de recuperação do campo lineReceivedRecord <br>
	 * Linha recebida no arquivo. Sem tratamento de parser ou conversões
	 * @return valor do campo lineReceivedRecord
	 */
	public String getLineReceivedRecord() {
		return lineReceivedRecord;
	}

	/**
	 * Valor de lineReceivedRecord atribuído a lineReceivedRecord <br>
	 * Linha recebida no arquivo. Sem tratamento de parser ou conversões
	 * @param lineReceivedRecord Atributo da Classe
	 */
	public void setLineReceivedRecord(String lineReceivedRecord) {
		this.lineReceivedRecord = lineReceivedRecord;
	}

	/**
	 * Valor de changedMedia atribuído a changedMedia <br>
	 * Flag para indicar se a mídia foi alterada no recebimento.
	 * Conveniente para teste uniátio e irá proporcionar um ganho
	 * muito alto de performance.
	 * @param changedMedia Atributo da Classe
	 */
	public void setChangedMedia(boolean changedMedia) {
		this.changedMedia = changedMedia;
	}
	
	/**
	 * Método de recuperação do campo changedMedia <br>
	 * Flag para indicar se a mídia foi alterada no recebimento.
	 * Conveniente para teste uniátio e irá proporcionar um ganho
	 * muito alto de performance.
	 * @return valor do campo changedMedia
	 */
	public boolean isChangedMedia() {
		return changedMedia;
	}
	
	/**
	 * Valor de changedMediaConfiguration atribuído a changedMediaConfiguration <br>
	 * Flag para indicar se a configuração da mídia foi alterada no recebimento.
	 * Conveniente para teste uniátio e irá proporcionar um ganho
	 * muito alto de performance.
	 * @param changedMediaConfiguration Atributo da Classe
	 */
	public void setChangedMediaConfiguration(boolean changedMediaConfiguration) {
		this.changedMediaConfiguration = changedMediaConfiguration;
	}
	
	/**
	 * Método de recuperação do campo changedMediaConfiguration <br>
	 * Flag para indicar se a configuração da mídia foi alterada no recebimento.
	 * Conveniente para teste uniátio e irá proporcionar um ganho
	 * muito alto de performance.
	 * @return valor do campo changedMediaConfiguration
	 */
	public boolean isChangedMediaConfiguration() {
		return changedMediaConfiguration;
	}

	/**
	 * Valor de mediaRecordSequence atribuído a mediaRecordSequence
	 * Sequencial do cadastro de media no modelo nosql
	 * @param mediaRecordSequence Atributo da Classe
	 */
	public void setMediaRecordSequence(String mediaRecordSequence) {
		this.mediaRecordSequence = mediaRecordSequence;
	}
	
	/**
	 * Método de recuperação do campo mediaRecordSequence
	 * Sequencial do cadastro de media no modelo nosql
	 * @return valor do campo mediaRecordSequence
	 */
	public String getMediaRecordSequence() {
		return mediaRecordSequence;
	}
	
	/**
	 * Valor de noSqlID atribuído a noSqlID
	 * id da media no modelo nosql
	 * @param noSqlID Atributo da Classe
	 */
	public void setNoSqlID(String noSqlID) {
		this.noSqlID = noSqlID;
	}
	
	/**
	 * Método de recuperação do campo noSqlID
	 * id da media no modelo nosql
	 * @return valor do campo noSqlID
	 */
	public String getNoSqlID() {
		return noSqlID;
	}
	
	/**
	 * Valor de firstTransaction atribuído a firstTransaction
	 *
	 * @param firstTransaction Atributo da Classe
	 */
	public void setFirstTransaction(Boolean firstTransaction) {
		this.firstTransaction = firstTransaction;
	}
	
	/**
	 * Método de recuperação do campo firstTransaction
	 *
	 * @return valor do campo firstTransaction
	 */
	public Boolean getFirstTransaction() {
		return firstTransaction;
	}
}
