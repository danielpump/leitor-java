package com.compsis.interop.domain;
import com.compsis.macadame.domain.MacadameEntity;
import com.compsis.notification.domain.Notification;

/**
 * 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Representa as configurações de tempo de uma notificação
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 20/02/2014 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
@SuppressWarnings("serial")
public class DelayedTaskSetting extends MacadameEntity {
	/**
	 * A notificação
	 */
	private Notification notification;
	/**
	 * intervalo em minutos que a notificação será acionada (para as notificões do
	 * tipo "atraso").
	 */
	private int delayNotification;
	/**
	 * Intervalo em minutos entre uma notificação e outra
	 */
	private int notificationInterval;
	/**
	 * Método de recuperação do campo delayNotification
	 * intervalo em minutos que a notificação será acionada (para as notificões do
	 * tipo "atraso").
	 * @return valor do campo delayNotification
	 */
	public int getDelayNotification() {
		return delayNotification;
	}
	/**
	 * Valor de delayNotification atribuído a delayNotification
	 * intervalo em minutos que a notificação será acionada (para as notificões do
	 * tipo "atraso").
	 * @param delayNotification Atributo da Classe
	 */
	public void setDelayNotification(int delayNotification) {
		this.delayNotification = delayNotification;
	}
	/**
	 * Método de recuperação do campo notificationInterval
	 * Intervalo em minutos entre uma notificação e outra
	 * @return valor do campo notificationInterval
	 */
	public int getNotificationInterval() {
		return notificationInterval;
	}
	/**
	 * Valor de notificationInterval atribuído a notificationInterval
	 * Intervalo em minutos entre uma notificação e outra
	 * @param notificationInterval Atributo da Classe
	 */
	public void setNotificationInterval(int notificationInterval) {
		this.notificationInterval = notificationInterval;
	}
	
	/**
	 * Valor de notification atribuído a notification
	 *
	 * @param notification Atributo da Classe
	 */
	public void setNotification(Notification notification) {
		this.notification = notification;
	}
	
	/**
	 * Método de recuperação do campo notification
	 *
	 * @return valor do campo notification
	 */
	public Notification getNotification() {
		return notification;
	}
}