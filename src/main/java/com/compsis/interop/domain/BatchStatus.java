/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto SICAT - Interop <br>
 *
 * Data de Cria��o: 19/09/2012<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.domain.DomainSpecificEntity;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 19/09/2012 - @author ntrivedi - Primeiva vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class BatchStatus extends DomainSpecificEntity {
	
	/**
	 * Lote Gerado -code : GER
	 */
	public static final String GENERATED = "GER";
	
	/**
	 * Lote Enviado - code : ENV
	 */
	public static final String SENT = "ENV";
	
	/**
	 * Lote Aceito - code : ACT
	 */
	public static final String ACCEPT = "ACT";
	
	/**
	 * Lote Recusado - code : REC
	 */
	public static final String  DECLINED = "REC";
	
	/**
	 * Lote Cancelado - code : CAN
	 */
	public static final String  CANCELED = "CAN";
	
	/**
	 * Lote Agendado
	 */
	public static final String  SCHEDULED_PAYMENT = "PGA";
	
	/**
	 * Lote Pago
	 */
	public static final String  PAID = "PG ";
	
	public BatchStatus () {}
	
	public BatchStatus (final String aCode) {
		super.setCode(aCode);
	}
	
}
