/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 07/11/2013<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.domain.MacadameFile;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * ExecutionDetail especializado para tratamento de detalhes das tarefas. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 07/11/2013 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class FileExecutionDetail extends ExecutionDetail {
	/** 
	 * Construtor padrão da classe
	 */
	public FileExecutionDetail() {
	}
	
	/**
	 * Construtor alternativo da classe
	 */
	public FileExecutionDetail(final MacadameFile file, final RunningInteropTask interopTask){
		this.file = file;
		setRunningInteropTask(interopTask);
	}
	private RunningInteropTask runningInteropTask;
	private MacadameFile file;	
	private boolean validFile = true;
	
	/**
	 * Valor de file atribuído a file
	 *
	 * @param file Atributo da Classe
	 */
	public void setFile(MacadameFile file) {
		this.file = file;
	}
	
	
	/**
	 * Método de recuperação do campo file
	 *
	 * @return valor do campo file
	 */
	public MacadameFile getFile() {
		return file;
	}
	
	/**
	 * Método de recuperação do campo validFile
	 *
	 * @return valor do campo validFile
	 */
	public boolean isValidFile() {
		return validFile;
	}

	/**
	 * Valor de validFile atribuído a validFile
	 *
	 * @param validFile Atributo da Classe
	 */
	public void setValidFile(boolean validFile) {
		this.validFile = validFile;
	}

	/**
	 * Valor de runningInteropTask atribuído a runningInteropTask
	 *
	 * @param runningInteropTask Atributo da Classe
	 */
	public void setRunningInteropTask(RunningInteropTask runningInteropTask) {
		this.runningInteropTask = runningInteropTask;
		if(runningInteropTask!=null && runningInteropTask.getRunningTask()!=null){
			setTaskExecution(runningInteropTask.getRunningTask());
			setCodeTask(runningInteropTask.getRunningTask().getTaskConfig().getTask().getCode());
		}
	}
	
	/**
	 * Método de recuperação do campo runningInteropTask
	 *
	 * @return valor do campo runningInteropTask
	 */
	public RunningInteropTask getRunningInteropTask() {
		return runningInteropTask;
	}
}
