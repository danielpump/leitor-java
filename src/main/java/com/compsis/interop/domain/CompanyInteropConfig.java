/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto Interop - Domain<br>
 *
 * Data de Criação: 17/06/2013<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.site.domain.Company;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Entidade de configuração com meio de pagamento protocolo e Company
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 17/06/2013 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class CompanyInteropConfig extends AbstractInteropConfig{
	public Company getSite(){
		return (Company) super.getSite();
	}

}
