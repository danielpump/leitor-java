/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto SICAT - Interop<br>
 *
 * Data de Criação: 21/09/2012<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import com.compsis.eventmanager.domain.Notifiable;
import com.compsis.macadame.collections.MacadameArrayList;
import com.compsis.macadame.collections.MacadameList;
import com.compsis.macadame.domain.DomainEntity;
import com.compsis.macadame.navigator.annotation.Navigable;
import com.compsis.macadame.security.domain.SecurityOperator;
import com.compsis.macadame.util.Strings;
import com.compsis.protocol.domain.ProtocolSetting;
import com.compsis.protocol.domain.Task;
import com.compsis.protocol.domain.map.TaskAttributeValueMap;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Define configurações de uma tarefa para uma configuração de meio de pagamento. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 21/09/2012 - @author Naresh Trivedi - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class InteropTask extends DomainEntity implements Notifiable{
	
	public final static class Factory {
		/**
		 * Monta a instancia adequada de InteropTask
		 * para cada tipo de task. <br>
		 * A Task informada no parametro já será setada na
		 * instancia.
		 * @param aTask
		 * @return
		 */
		public static InteropTask getInstance(final Task aTask){
			InteropTask interopTask = null;
			if(in(aTask.getCode(),Task.Codes.BATCH_GENERATION)){
				interopTask = new BatchTaskConfig();
			}else if(in(aTask.getCode(),Task.Codes.BATCH_RETURN, Task.Codes.BATCH_SENDING,Task.Codes.FINANCIAL_BATCH_RETURN)){
				interopTask = new BatchFileTaskConfig();
			}else if(in(aTask.getCode(),Task.Codes.LIST_FULL, Task.Codes.WHITE_LIST_PARTIAL, Task.Codes.BLACK_LIST_PARTIAL,Task.Codes.EXEMPT_ALESP)){
				interopTask = new DataFileTransferTaskConfig();
			}else if(in(aTask.getCode(),Task.Codes.MAINTENANCE_DISCOUNT)){
				interopTask = new RT1DataFileTransferTaskConfig();				
			}else if(in(aTask.getCode(), Task.Codes.TARIFF_GENERATION,Task.Codes.TARIFF_SENDING, Task.Codes.ACQUIRED_VALE_PEDAGIO, Task.Codes.PERFORMED_VALE_PEDAGIO )){
				interopTask = new EntityFileTaskConfig();
			}else if (in(aTask.getCode(), Task.Codes.TAGS_MESSAGE, Task.Codes.TAG_REQUEST, Task.Codes.SEND_TARIFF_MESSAGE, Task.Codes.IMAGE_REQUEST, Task.Codes.CONCESSIONAIRE_DISCOUNT)){
				interopTask = new DataTransferTaskConfig();				
			}else if (in(aTask.getCode(), Task.Codes.SEND_IMAGE_MESSAGE,Task.Codes.PASSAGE_MESSAGE)){
				interopTask = new EntityTaskConfig();
			}else {
				interopTask = new DataTransferTaskConfig();
			}
			return interopTask;
		}
		
		private static boolean in(String compare, String...with){
			return Arrays.asList(with).contains(compare);
		}
	}
	
	public static class MetaTags {
		public static final String TASK_NAME = "TASKNAME";
		public static final String PROTOCOL_NAME = "PROTOCOLNAME";
		public static final String MOP_NAME = "MOPNAME";
		public static final String EXECUTION_MODE = "EXECUTIONMODE";
		public static final String LAST_EXECUTION = "LASTEXECUTION";
		public static final String NEX_EXECUTION = "NEXTEXECUTION";
		public static final String DELAY_EXECUTION = "DELAYEXECUTION";
		public static final String ERROR_MESSAGE = "ERRORMESSAGE";
		public static final String STACK_TRACE = "STACKTRACE";
		public static final String TIME_OF_VERIFICATION = "TIMEOFVERIFICATION";
		public static final String SEQUENTIAL = "SEQUENTIAL";
	}

	/**
	 * Tarefa configurada.
	 */
	private Task task;	
	
	/**
	 * Modo de execução da tarefa AUTOMATICO / MANUAL.
	 */
	private ExecutionMode executionMode = ExecutionMode.MANUAL;
	/**
	 * Intervalo de execução da tarefa.
	 */
	private String interval;
	/**
	 * Última data de execução da tarefa.
	 */
	private Calendar lastDate;
	/**
	 * Próxima data de execução da tarefa.
	 */
	private Calendar nextDate;
	/**
	 * Configuração do meio de pagamento vinculada a tarefa.
	 */
	private AbstractInteropConfig interopConfig;
	
	/**
	 * Ultimo sequencial processado com sucesso em determinada tarefa
	 */
	private Long lastValidSequential;
	
	/**
	 * Configuração do protocolo vinculada a tarefa.
	 */
	private MacadameList<ProtocolSetting> protocolSettings;
	/**
	 * Operador do sistema que iniciou a task manualmente
	 */
	private SecurityOperator operator;
	
	/**
	 * Lista das tarefas a serem executadas para esta configuração
	 */
	private MacadameList<TaskExecution> tasksExecutions;
	
	/**
	 * Valores para os campos definidos em parameters.
	 * Esses valores são inseridos via interface e podem ser recuperados
	 * na navegação através desse atributo.
	 */
	private TaskAttributeValueMap parameterValues;
		
	/**
	 * Gera chave de logs com o padrão:<br>
	 * Task.Protocol.MOP<br>
	 * MOP<br>
	 * + Chaves passadas pelo parametro <b>actualLogNames</b><br>
	 * @param actualLogNames VarArgs com outras chaves de Log que podem ser passadas por parametro e serão adicionadas no LOG.
	 * @return logNames.toArray(new String[]{}) - Array de String contendo as chaves de log
	 */
	public String[] getLogNames(String... actualLogNames) {
		List<String> logNames;
		if(actualLogNames == null) {
			logNames = new ArrayList<String>();
		} else {
			logNames = new ArrayList<String>(Arrays.asList(actualLogNames));
		}
		if(getTask()!=null) {
			if(getInteropConfig()!=null) {
				logNames.add(Strings.concat( getTask().getCode() ,  "." , 
											 getInteropConfig().getProtocol().getCode() ,  "." ,
											 getInteropConfig().getMeanOfPayment().getCode()
										   ));
				logNames.add(getInteropConfig().getMeanOfPayment().getCode());
				logNames.add(getTask().getBusinessCasePrefix());
			} else {
				logNames.add(getTask().getCode());
			}
		}
		return logNames.toArray(new String[]{});
	}
	
	/**
	 * Método de recuperação do campo task
	 *
	 * @return valor do campo task
	 */
	public Task getTask() {
		return task;
	}
	/**
	 * Valor de aTask atribuído a task
	 *
	 * @param aTask Atributo da Classe
	 */
	public void setTask(Task aTask) {
		task = aTask;
	}
	/**
	 * Método de recuperação do campo executionMode
	 *
	 * @return valor do campo executionMode
	 */
	public ExecutionMode getExecutionMode() {
		return executionMode;
	}
	/**
	 * Valor de aExecutionMode atribuído a executionMode
	 *
	 * @param aExecutionMode Atributo da Classe
	 */
	public void setExecutionMode(ExecutionMode aExecutionMode) {
		executionMode = aExecutionMode;
	}
	/**
	 * Método de recuperação do campo interval
	 *
	 * @return valor do campo interval
	 */
	public String getInterval() {
		return interval;
	}
	/**
	 * Valor de aInterval atribuído a interval
	 *
	 * @param aInterval Atributo da Classe
	 */
	public void setInterval(String aInterval) {
		interval = aInterval;
	}
	/**
	 * Método de recuperação do campo lastDate
	 *
	 * @return valor do campo lastDate
	 */
	public Calendar getLastDate() {
		return lastDate;
	}
	/**
	 * Valor de aLastDate atribuído a lastDate
	 *
	 * @param aLastDate Atributo da Classe
	 */
	public void setLastDate(Calendar aLastDate) {
		lastDate = aLastDate;
	}
	/**
	 * Método de recuperação do campo nextDate
	 *
	 * @return valor do campo nextDate
	 */
	public Calendar getNextDate() {
		return nextDate;
	}
	/**
	 * Valor de aNextDate atribuído a nextDate
	 *
	 * @param aNextDate Atributo da Classe
	 */
	public void setNextDate(Calendar aNextDate) {
		nextDate = aNextDate;
	}	
	
	/**
	 * Método de recuperação do campo interopConfig
	 *
	 * @return valor do campo interopConfig
	 */
	public AbstractInteropConfig getInteropConfig() {
		return interopConfig;
	}
	/**
	 * Método de recuperação do campo operator
	 *
	 * @return valor do campo operator
	 */
	@Navigable
	public SecurityOperator getOperator() {
		return operator;
	}
	/**
	 * Valor de operator atribuído a operator
	 *
	 * @param operator Atributo da Classe
	 */
	public void setOperator(SecurityOperator operator) {
		this.operator = operator;
	}
	/**
	 * Método de recuperação do campo protocolSettings
	 *
	 * @return valor do campo protocolSettings
	 */
	public MacadameList<ProtocolSetting> getProtocolSettings() {
		return protocolSettings;
	}
	/**
	 * Valor de protocolSettings atribuído a protocolSettings
	 *
	 * @param protocolSettings Atributo da Classe
	 */
	public void setProtocolSettings(MacadameList<ProtocolSetting> protocolSettings) {
		this.protocolSettings = protocolSettings;
	}		
	
	/**
	 * Adiciona {@link ProtocolSetting} no {@link InteropTask}
	 * @param protocolSetting
	 */
	public void addProtocolSetting(final ProtocolSetting protocolSetting) {
		if(getProtocolSettings()==null) {
			setProtocolSettings(new MacadameArrayList<ProtocolSetting>());
		}
		getProtocolSettings().add(protocolSetting);
	}
		
	public ProtocolSetting getProtocolSetting(final String aSettingCode) {
		for (ProtocolSetting setting : getProtocolSettings()) {
			if (setting.getType().getCode().equals(aSettingCode)) {
				return setting;
			}
		}
		return null;
	}
	/**
	 * Método de recuperação do campo tasksExecutions
	 *
	 * @return valor do campo tasksExecutions
	 */
	public MacadameList<TaskExecution> getTasksExecutions() {
		return tasksExecutions;
	}
	/**
	 * Valor de tasksExecutions atribuído a tasksExecutions
	 *
	 * @param tasksExecutions Atributo da Classe
	 */
	public void setTasksExecutions(MacadameList<TaskExecution> tasksExecutions) {
		this.tasksExecutions = tasksExecutions;
	}
	/**
	 * Valor de interopConfig atribuído a interopConfig
	 *
	 * @param interopConfig Atributo da Classe
	 */
	public void setInteropConfig(AbstractInteropConfig interopConfig) {
		this.interopConfig = interopConfig;
	}
	
	
	
	/**
	 * Método de recuperação do campo lastValidSequencial
	 *
	 * @return valor do campo lastValidSequencial
	 */
	public Long getLastValidSequential() {
		return lastValidSequential;
	}
	/**
	 * Valor de lastValidSequencial atribuído a lastValidSequencial
	 *
	 * @param lastValidSequential Atributo da Classe
	 */
	public void setLastValidSequential(Long lastValidSequential) {
		this.lastValidSequential = lastValidSequential;
	}
	/** 
	 * TODO Descrição do Método
	 * @param aNotifiable
	 * @return
	 * @see com.compsis.eventmanager.domain.Notifiable#isSameNotifiable(com.compsis.eventmanager.domain.Notifiable)
	 */
	@Override
	public boolean isSameNotifiable(Notifiable aNotifiable) {
		if( aNotifiable instanceof InteropTask ){
			return ((InteropTask) aNotifiable).getId().equals(getId());
		}
		return false;
	}	
	
	/**
	 * Valor de parameterValues atribuído a parameterValues
	 *
	 * @param parameterValues Atributo da Classe
	 */
	public void setParameterValues(TaskAttributeValueMap parameterValues) {
		this.parameterValues = parameterValues;
	}
	
	/**
	 * Método de recuperação do campo parameterValues
	 *
	 * @return valor do campo parameterValues
	 */
	public TaskAttributeValueMap getParameterValues() {
		return parameterValues;
	}
}
