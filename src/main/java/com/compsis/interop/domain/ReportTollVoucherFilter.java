/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 23/09/2013<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.issuer.domain.Issuer;
import com.compsis.macadame.collections.MacadameArrayList;
import com.compsis.macadame.collections.MacadameList;
import com.compsis.macadame.domain.Filter;
import com.compsis.macadame.domain.Period;
import com.compsis.site.domain.Unit;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Filtro para pesquisa de relatórios de Vale Pedágio. <br>
 * Utilizado em:<br>
 * - 1115: Passagens Realizadas Vale Pedágio
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 23/09/2013 - @author Vinícius O. Ueda - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class ReportTollVoucherFilter extends Filter {
	private Period period;
	private MacadameList<Unit> units;
	private MacadameList<Issuer> issuers;
	private String mediaIdentification;
	private Boolean withoutIssuer = false;
	private Boolean searchAllIssuer;
	
	/**
	 * Indica se serão retornados somente as passagens vale pedágio efetivamente realizadas na pista
	 */
	private boolean onlyRealized = false;
	
	/**
	 * Método de recuperação do campo onlyRealized
	 *
	 * @return valor do campo onlyRealized
	 */
	public boolean isOnlyRealized() {
		return onlyRealized;
	}
	/**
	 * Valor de onlyRealized atribuído a onlyRealized
	 *
	 * @param onlyRealized Atributo da Classe
	 */
	public void setOnlyRealized(boolean onlyRealized) {
		this.onlyRealized = onlyRealized;
	}
	/**
	 * Método de recuperação do campo period
	 *
	 * @return valor do campo period
	 */
	public Period getPeriod() {
		return period;
	}
	/**
	 * Valor de period atribuído a period
	 *
	 * @param period Atributo da Classe
	 */
	public void setPeriod(Period period) {
		this.period = period;
	}
	/**
	 * Método de recuperação do campo units
	 *
	 * @return valor do campo units
	 */
	public MacadameList<Unit> getUnits() {
		if(units == null){
			units = new MacadameArrayList<Unit>();
		}
		return units;
	}
	/**
	 * Valor de units atribuído a units
	 *
	 * @param units Atributo da Classe
	 */
	public void setUnits(MacadameList<Unit> units) {
		this.units = units;
	}
	/**
	 * Método de recuperação do campo issuers
	 *
	 * @return valor do campo issuers
	 */
	public MacadameList<Issuer> getIssuers() {
		if(issuers == null){
			issuers = new MacadameArrayList<Issuer>();
		}
		return issuers;
	}
	/**
	 * Valor de issuers atribuído a issuers
	 *
	 * @param issuers Atributo da Classe
	 */
	public void setIssuers(MacadameList<Issuer> issuers) {
		this.issuers = issuers;
	}
	/**
	 * Método de recuperação do campo mediaIdentification
	 *
	 * @return valor do campo mediaIdentification
	 */
	public String getMediaIdentification() {
		return mediaIdentification;
	}
	/**
	 * Valor de mediaIdentification atribuído a mediaIdentification
	 *
	 * @param mediaIdentification Atributo da Classe
	 */
	public void setMediaIdentification(String mediaIdentification) {
		this.mediaIdentification = mediaIdentification;
	}
	public Boolean isWithoutIssuer() {
		return withoutIssuer;
	}
	public void setWithoutIssuer(Boolean withoutIssuer) {
		this.withoutIssuer = withoutIssuer;
	}
	public Boolean isSearchAllIssuer() {
		return searchAllIssuer;
	}
	public void setSearchAllIssuer(Boolean searchAllIssuer) {
		this.searchAllIssuer = searchAllIssuer;
	}
	
	
	
}
