/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 17/10/2012<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.collections.Collections;
import com.compsis.macadame.collections.MacadameList;
import com.compsis.macadame.domain.DomainSpecificEntity;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Motivo da alteração manual de tag. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 01/07/2014 - @author dferraz - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
@SuppressWarnings("serial")
public class ManualTagReason extends DomainSpecificEntity {
	
	/** 
	 * Construtor padrão da classe
	 */
	public ManualTagReason() {
	}
	/** 
	 * Construtor alternativo da classe para instancia objetos com o <code>code</code> j� especificado
	 */
	public ManualTagReason(final String aCode) {
		super.setCode(aCode);
	}
	
    public static final String NOT_APPLY = "0";//Não se aplica
    public static final String PASSAGE_BLOCKED = "1";//Passagem bloqueada
    public static final String TAG_READING_FAIL = "2";//Falha na leitura do tag		    				
    public static final String AUTO_POS_MAINTENANCE = "3";//Pista automática em manutenção	    				
    public static final String VEHICLE_EXTRA_LARGE = "4";//Veículo extra-largo	
	
    public static MacadameList<ManualTagReason> loadAllManualTagReasons(){
    	
    	MacadameList<ManualTagReason> manualTagReasons = Collections.newArrayList();
    	
    	manualTagReasons.add(new ManualTagReason(NOT_APPLY));
    	manualTagReasons.add(new ManualTagReason(PASSAGE_BLOCKED));
    	manualTagReasons.add(new ManualTagReason(TAG_READING_FAIL));
    	manualTagReasons.add(new ManualTagReason(AUTO_POS_MAINTENANCE));
    	manualTagReasons.add(new ManualTagReason(VEHICLE_EXTRA_LARGE));
    	
    	return null;
    }

}
