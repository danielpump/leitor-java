/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 19/06/2013<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;


/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Classe utilizada na listagem da tabela para configuração do envio de transações <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 19/06/2013 - @author wcarvalho - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class ConfigSendTransaction{

	
	/** 
	 * Construtor padrão
	 */
	public ConfigSendTransaction() {}
	
	/** 
	 * Construtor Alternativo
	 */
	public ConfigSendTransaction(final String item) {this.item = item;}
	public ConfigSendTransaction(final String item, final String statusIcon) {this.item = item; this.statusIcon = statusIcon;}
	/**
	 * Item a ser configurado
	 */
	private String item;
	
	/**
	 * Icone 
	 */
	private String statusIcon;
	/**
	 * Método de recuperação do campo item
	 * Item a ser configurado
	 * @return valor do campo item
	 */
	public String getItem() {
		return item;
	}
	/**
	 * Valor de item atribuído a item
	 * Item a ser configurado
	 * @param item Atributo da Classe
	 */
	public void setItem(String item) {
		this.item = item;
	}

	/**
	 * Método de recuperação do campo statusIcon
	 * Icone
	 * @return valor do campo statusIcon
	 */
	public String getStatusIcon() {
		return statusIcon;
	}

	/**
	 * Valor de statusIcon atribuído a statusIcon
	 * Icone
	 * @param statusIcon Atributo da Classe
	 */
	public void setStatusIcon(String statusIcon) {
		this.statusIcon = statusIcon;
	}

	
	
}
