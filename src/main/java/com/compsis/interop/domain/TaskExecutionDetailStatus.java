/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * SICAT - Componentente Interop<br>
 *
 * Data de Criação: 04/09/2013<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;


/**
 * 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Representa os estados do detalhe da tarefa. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 27/08/2014 - @author aotoni - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public enum TaskExecutionDetailStatus {

	SUCESS(0, "Sucesso"),
	FAIL(1, "Falha"),
	RES(2, "Ressalva");
	
	private Integer code;

	private String description;

	/** 
	 * TODO Construtor padrão/alternativo da classe
	 */
	private TaskExecutionDetailStatus(Integer cd, String ds) {
		this.code = cd;
		this.description = ds;
	}
	
	public static TaskExecutionDetailStatus byCode(Integer cd) {
		TaskExecutionDetailStatus retorno = null;
		for (TaskExecutionDetailStatus t : values()) {
			if (cd.equals(t.getCode())) {
				retorno = t;
			}
		}
		return retorno;
	}
	
	/**
	 * Método de recuperação do campo code
	 *
	 * @return valor do campo code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Valor de code atribuído a code
	 *
	 * @param code Atributo da Classe
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Método de recuperação do campo description
	 *
	 * @return valor do campo description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Valor de descripton atribuído a description
	 *
	 * @param descripton Atributo da Classe
	 */
	public void setDescription(String descripton) {
		this.description = descripton;
	}
	
}
