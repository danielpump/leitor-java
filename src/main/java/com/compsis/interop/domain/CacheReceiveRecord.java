/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 06/06/2013<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.country.domain.Country;
import com.compsis.issuer.domain.Issuer;
import com.compsis.macadame.collections.Map;
import com.compsis.macadame.navigator.annotation.Navigable;
import com.compsis.media.domain.LaneAction;
import com.compsis.media.domain.MediaStatus;
import com.compsis.vehicle.domain.VehicleClass;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Esta entidade tem como finalidade manter os mapas
 * de informações que sempre serão acessadas no recebimento de cadastro do st19 <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 06/06/2013 - @author wcarvalho - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class CacheReceiveRecord {

	/**
	 * mapa de status de media
	 */
	private Map<String, MediaStatus> mapMediaStatus;
	
	/**
	 * mapa de categorias
	 */
	private Map<String, VehicleClass> mapVehicleClass;
	
	/**
	 * mapa de emissores
	 */
	private Map<String, Issuer> mapIssuer;
	
	/**
	 * mapa de paises
	 */
	private Map<String, Country> mapCountry;
	
	/**
	 * mapa de acões pista
	 */
	private Map<String, LaneAction> mapLaneAction;

	/**
	 * Método de recuperação do campo mapMediaStatus
	 *
	 * @return valor do campo mapMediaStatus
	 */
	@Navigable
	public Map<String, MediaStatus> getMapMediaStatus() {
		return mapMediaStatus;
	}

	/**
	 * Método de recuperação do campo mapVehicleClass
	 *
	 * @return valor do campo mapVehicleClass
	 */
	@Navigable
	public Map<String, VehicleClass> getMapVehicleClass() {
		return mapVehicleClass;
	}

	/**
	 * Valor de mapVehicleClass atribuído a mapVehicleClass
	 *
	 * @param mapVehicleClass Atributo da Classe
	 */
	public void setMapVehicleClass(Map<String, VehicleClass> mapVehicleClass) {
		this.mapVehicleClass = mapVehicleClass;
	}

	/**
	 * Método de recuperação do campo mapIssuer
	 *
	 * @return valor do campo mapIssuer
	 */
	@Navigable
	public Map<String, Issuer> getMapIssuer() {
		return mapIssuer;
	}

	/**
	 * Valor de mapIssuer atribuído a mapIssuer
	 *
	 * @param mapIssuer Atributo da Classe
	 */
	public void setMapIssuer(Map<String, Issuer> mapIssuer) {
		this.mapIssuer = mapIssuer;
	}

	/**
	 * Método de recuperação do campo mapCountry
	 *
	 * @return valor do campo mapCountry
	 */
	@Navigable
	public Map<String, Country> getMapCountry() {
		return mapCountry;
	}

	/**
	 * Valor de mapCountry atribuído a mapCountry
	 *
	 * @param mapCountry Atributo da Classe
	 */
	public void setMapCountry(Map<String, Country> mapCountry) {
		this.mapCountry = mapCountry;
	}

	/**
	 * Método de recuperação do campo mapLaneAction
	 *
	 * @return valor do campo mapLaneAction
	 */
	@Navigable
	public Map<String, LaneAction> getMapLaneAction() {
		return mapLaneAction;
	}

	/**
	 * Valor de mapLaneAction atribuído a mapLaneAction
	 *
	 * @param mapLaneAction Atributo da Classe
	 */
	public void setMapLaneAction(Map<String, LaneAction> mapLaneAction) {
		this.mapLaneAction = mapLaneAction;
	}

	/**
	 * Valor de mapMediaStatus atribuído a mapMediaStatus
	 *
	 * @param mapMediaStatus Atributo da Classe
	 */
	public void setMapMediaStatus(Map<String, MediaStatus> mapMediaStatus) {
		this.mapMediaStatus = mapMediaStatus;
	}
	
}
