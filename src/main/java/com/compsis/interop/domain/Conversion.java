/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 21/09/2012<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.domain.MacadameEntity;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 21/09/2012 - @author ntrivedi - Primeiva vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class Conversion extends MacadameEntity {

	private String internalValue;	
	private String externalValue;
	
	/**
	 * M�todo de recupera��o do campo internalValue
	 *
	 * @return valor do campo internalValue
	 */
	public String getInternalValue() {
		return internalValue;
	}
	/**
	 * Valor de aInternalValue atribu�do a internalValue
	 *
	 * @param aInternalValue Atributo da Classe
	 */
	public void setInternalValue(String aInternalValue) {
		internalValue = aInternalValue;
	}
	/**
	 * M�todo de recupera��o do campo externalValue
	 *
	 * @return valor do campo externalValue
	 */
	public String getExternalValue() {
		return externalValue;
	}
	/**
	 * Valor de aExternalValue atribu�do a externalValue
	 *
	 * @param aExternalValue Atributo da Classe
	 */
	public void setExternalValue(String aExternalValue) {
		externalValue = aExternalValue;
	}
	/** 
	 * TODO Descrição do Método
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Conversion [");
		builder.append(super.toString());
		builder.append(",");
		if (internalValue != null) {
			builder.append("internalValue=");
			builder.append(internalValue);
			builder.append(", ");
		}
		if (externalValue != null) {
			builder.append("externalValue=");
			builder.append(externalValue);
		}
		builder.append("]");
		return builder.toString();
	}
}
