/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 12/10/2012<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.collections.MacadameList;
import com.compsis.macadame.domain.DomainSpecificEntity;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 12/10/2012 - @author wcarvalho - Primeira vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class ConversionType extends DomainSpecificEntity{

	private String entity;
	private String attribute;
	
	/**
	 * Construtor padrão da classe
	 */
	public ConversionType(){}
	
	/**
	 * Construtor alternativo da classe
	 * @param aCode - Code da entidade
	 */
	public ConversionType(final String aCode){
		super.setCode(aCode);
	}
	
	/**
	 * Indica se o atributo a ser convertido permite valor default.
	 */
	private Boolean allowDefaultValue;
	
	/**
	 * Lista de protocolos associados
	 */
	private MacadameList<ProtocolConversionType> protocolConversionTypes;

	/**
	 * Método de recuperação do campo protocolConversionTypes
	 *
	 * @return valor do campo protocolConversionTypes
	 */
	public MacadameList<ProtocolConversionType> getProtocolConversionTypes() {
		return protocolConversionTypes;
	}
	/**
	 * Valor de protocolConversionTypes atribuído a protocolConversionTypes
	 *
	 * @param protocolConversionTypes Atributo da Classe
	 */
	public void setProtocolConversionTypes(
		 MacadameList<ProtocolConversionType> protocolConversionTypes) {
		this.protocolConversionTypes = protocolConversionTypes;
	}
	/**
	 * M�todo de recupera��o do campo entity
	 *
	 * @return valor do campo entity
	 */
	public String getEntity() {
		return entity;
	}
	/**
	 * Valor de aEntity atribu�do a entity
	 *
	 * @param aEntity Atributo da Classe
	 */
	public void setEntity(String aEntity) {
		entity = aEntity;
	}
	/**
	 * M�todo de recupera��o do campo attribute
	 *
	 * @return valor do campo attribute
	 */
	public String getAttribute() {
		return attribute;
	}
	/**
	 * Valor de aAttribute atribu�do a attribute
	 *
	 * @param aAttribute Atributo da Classe
	 */
	public void setAttribute(String aAttribute) {
		attribute = aAttribute;
	}
	/**
	 * Método de recuperação do campo allowDefaultValue
	 *
	 * @return valor do campo allowDefaultValue
	 */
	public Boolean getAllowDefaultValue() {
		return allowDefaultValue;
	}
	/**
	 * Valor de allowDefaultValue atribuído a allowDefaultValue
	 *
	 * @param allowDefaultValue Atributo da Classe
	 */
	public void setAllowDefaultValue(Boolean allowDefaultValue) {
		this.allowDefaultValue = allowDefaultValue;
	}
	/** 
	 * TODO Descrição do Método
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConversionType [");
		builder.append(super.toString());
		builder.append(",");
		if (entity != null) {
			builder.append("entity=");
			builder.append(entity);
			builder.append(", ");
		}
		if (attribute != null) {
			builder.append("attribute=");
			builder.append(attribute);
			builder.append(", ");
		}
		if (allowDefaultValue != null) {
			builder.append("allowDefaultValue=");
			builder.append(allowDefaultValue);
			builder.append(", ");
		}
		if (protocolConversionTypes != null) {
			builder.append("protocolConversionTypes=");
			builder.append(protocolConversionTypes);
		}
		builder.append("]");
		return builder.toString();
	}
	
	
}
