/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 06/06/2014<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.domain.MacadameObject;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 06/06/2014 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class NoSQLToRDBMS<N, R> extends MacadameObject {
	private N nosqlEntity;
	private R rdbmsEntity;
	/**
	 * Método de recuperação do campo nosqlEntity
	 *
	 * @return valor do campo nosqlEntity
	 */
	public N getNosqlEntity() {
		return nosqlEntity;
	}
	/**
	 * Valor de nosqlEntity atribuído a nosqlEntity
	 *
	 * @param nosqlEntity Atributo da Classe
	 */
	public void setNosqlEntity(N nosqlEntity) {
		this.nosqlEntity = nosqlEntity;
	}
	/**
	 * Método de recuperação do campo rdbmsEntity
	 *
	 * @return valor do campo rdbmsEntity
	 */
	public R getRdbmsEntity() {
		return rdbmsEntity;
	}
	/**
	 * Valor de rdbmsEntity atribuído a rdbmsEntity
	 *
	 * @param rdbmsEntity Atributo da Classe
	 */
	public void setRdbmsEntity(R rdbmsEntity) {
		this.rdbmsEntity = rdbmsEntity;
	}
}
