/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 23/10/2014<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.domain.MacadameObject;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 23/10/2014 - @author fpereira - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class EntityRunningTaskErrorLog extends MacadameObject {
	
	/**
	 * Atributo que representa a Task que ocorreu o erro
	 */
	private RunningInteropTask runningInteropTask;
	/**
	 * Atributo que representa a mensagem gerada pela excessão que ocorreu na execução da tarefa
	 */
	private String exceptionMessage;
	
	/** 
	 * retorna a task que ocorreu o erro
	 * @return
	 */
	public RunningInteropTask getRunningInteropTask() {
		return runningInteropTask;
	}
	/** 
	 * Metodo para fazer o set da task onde o erro ocorreu
	 * @param runningInteropTask
	 */
	public void setRunningInteropTask(RunningInteropTask runningInteropTask) {
		this.runningInteropTask = runningInteropTask;
	}
	/** 
	 * retorna a mensagem da excessão que ocorreu
	 * @return
	 */
	public String getExceptionMessage() {
		return exceptionMessage;
	}
	/** 
	 *  Metodo para fazer o set da mensagem da excessão que ocorreu
	 * @param exceptionMessage
	 */
	public void setExceptionMessage(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}
	
	
}
