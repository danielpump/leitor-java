/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 13/10/2012<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.collections.MacadameList;
import com.compsis.macadame.domain.MacadameEntity;
import com.compsis.macadame.util.Strings;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 13/10/2012 - @author wcarvalho - Primeira vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class InteropConversion  extends MacadameEntity {

	public static final String VEHICLE_CLASS = "com.compsis.vehicle.domain.VehicleClass";
	public static final String PLAZA = "com.compsis.site.domain.Unit";
	public static final String IMAGE_REASON = "com.compsis.image.domain.ImageReason";
	public static final String CONCESSION = "com.compsis.site.domain.Company";
	public static final String COUNTRY = "com.compsis.country.domain.Country";
	public static final String MEDIA_STATUS = "com.compsis.media.domain.MediaStatus";
	public static final String LANE_ACTION = "com.compsis.media.domain.LaneAction";
	public static final String RETURN_CODE = "com.compsis.interop.domain.ReturnCode";
	
	public static final String CODE_VEHICLE_CLASS = "CATG";
	public static final String CODE_PLAZA = "PRCA";
	public static final String CODE_IMAGE_REASON = "MIMG";
	public static final String CODE_CONCESSION = "CONC";
	public static final String CODE_COUNTRY = "PAIS";
	public static final String CODE_MEDIA_STATUS = "SMED";
	public static final String CODE_LANE_ACTION = "APST";
	public static final String CODE_RETURN_CODE_BATCH = "RLOT";
	public static final String CODE_RETURN_CODE_TRANSACTION = "RTRN";
	public static final String CODE_CLASSIFICATION = "STDO";
	public static final String CODE_POS = "PSTA";
	/**
	 * Nome da concessionária
	 */
	public static final String CODE_CONCESSION_NAME = "NCON";
	/**
	 * Código para conversões do privilégio. Valor padrão = <b>PRIV</b>
	 */
	public static final String CODE_PRIVILEGE = "PRIV";

	
	private String defaultValue;
	private ConversionType conversionType;
	private MacadameList<Conversion> conversions;
	
	private AbstractInteropConfig interopConfig;
	
	public String convertToInternalValue(final String externalValue) {
		for(Conversion conversion : conversions) {
			// Caso esteja configurado com valor default, o valor externo pode não existir
			if (!Strings.isNullOrEmpty(conversion.getExternalValue()) &&
					Strings.removeZeros(conversion.getExternalValue().trim() ).equals(Strings.removeZeros(externalValue.trim()))) {
				return conversion.getInternalValue();
			}
		}
		return defaultValue;
	}
	
	
	
	public String convertToExternalValue(final String internalValue) {
		
		for(Conversion conversion : conversions) {
			if (  Strings.removeZeros(conversion.getInternalValue().trim()).equals(Strings.removeZeros(internalValue.trim())) &&
					!Strings.isNullOrEmpty(conversion.getExternalValue())) {
				return  Strings.removeZeros(conversion.getExternalValue());
			}
		}
		return defaultValue;
	}
	
	/**
	 * M�todo de recupera��o do campo defaultValue
	 *
	 * @return valor do campo defaultValue
	 */
	public String getDefaultValue() {
		return defaultValue;
	}
	/**
	 * Valor de aDefaultValue atribu�do a defaultValue
	 *
	 * @param aDefaultValue Atributo da Classe
	 */
	public void setDefaultValue(String aDefaultValue) {
		defaultValue = aDefaultValue;
	}
	
	/**
	 * M�todo de recupera��o do campo conversionType
	 *
	 * @return valor do campo conversionType
	 */
	public ConversionType getConversionType() {
		return conversionType;
	}
	
	/**
	 * Valor de aConversionType atribu�do a conversionType
	 *
	 * @param aConversionType Atributo da Classe
	 */
	public void setConversionType(ConversionType aConversionType) {
		conversionType = aConversionType;
	}
	/**
	 * M�todo de recupera��o do campo conversions
	 *
	 * @return valor do campo conversions
	 */
	public MacadameList<Conversion> getConversions() {
		return conversions;
	}
	
	/**
	 * Valor de aConversions atribu�do a conversions
	 *
	 * @param aConversions Atributo da Classe
	 */
	public void setConversions(MacadameList<Conversion> aConversions) {
		conversions = aConversions;
	}

	/**
	 * Método de recuperação do campo interopConfig
	 *
	 * @return valor do campo interopConfig
	 */
	public AbstractInteropConfig getInteropConfig() {
		return interopConfig;
	}

	/**
	 * Valor de interopConfig atribuído a interopConfig
	 *
	 * @param interopConfig Atributo da Classe
	 */
	public void setInteropConfig(AbstractInteropConfig interopConfig) {
		this.interopConfig = interopConfig;
	}

	/** 
	 * TODO Descrição do Método
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final int maxLen = 10;
		StringBuilder builder = new StringBuilder();
		builder.append("InteropConversion [");
		builder.append(super.toString());
		builder.append(",");
		if (defaultValue != null) {
			builder.append("defaultValue=");
			builder.append(defaultValue);
			builder.append(", ");
		}
		if (conversionType != null) {
			builder.append("conversionType=");
			builder.append(conversionType);
			builder.append(", ");
		}
		if (conversions != null) {
			builder.append("conversions=");
			builder.append(conversions.subList(0,
					Math.min(conversions.size(), maxLen)));
			builder.append(", ");
		}
		if (interopConfig != null) {
			builder.append("interopConfig=");
			builder.append(interopConfig);
		}
		builder.append("]");
		return builder.toString();
	}
	public static void main(String[] args) {
		System.out.println(Integer.MAX_VALUE);
	}
	
}
