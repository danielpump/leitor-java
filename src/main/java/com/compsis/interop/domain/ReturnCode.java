/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 17/10/2012<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.collections.MacadameList;
import com.compsis.macadame.domain.DomainSpecificEntity;
import com.compsis.protocol.domain.Protocol;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 17/10/2012 - @author ntrivedi - Primeiva vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class ReturnCode extends DomainSpecificEntity {

	
	public static final String ACCEPT_EVASION="98";
	public static final String OPERATE_EVASION="99";
	
	public static final String WITHOUT_RETURN = "-1";
	
	public static final String TRANSACTION_TRANSACAO_REPETIDA = "01";
	public static final String TRANSACTION_TAG_BLOQUEADO = "06";
	public static final String TRANSACTION_CAT_DIVERGENTE = "09";
	public static final String TRANSACTION_FORA_DO_PRAZO = "10";
	public static final String TRANSACTION_DESCONTO = "16";
	public static final String TRANSACTION_DIFERENCIADO = "17";
	public static final String TRANSACTION_PRACA_BLOQUEADA = "18";
	public static final String TRANSACTION_DADOS_INVALIDOS = "19";
	public static final String TRANSACTION_ISENTO = "84";
	public static final String TRANSACTION_EXEMPT_WAMOUNT = "53";
	
	/**
	 * Tipos de retornos:
	 * 1 - Retorno de lote
	 * 2 - Retorno de transação
	 */
	private ReturnCodeType returnCodetype;
	
	private MacadameList<Protocol> protocols;
	
	/**
	 * 
	 * TODO Construtor padrão/alternativo da classe
	 */
	public ReturnCode() {
		super();
	}
	
	/**
	 * 
	 * TODO Construtor padrão/alternativo da classe
	 * @param code
	 */
	public ReturnCode(String code) {
		setCode(code);
	}


	/**
	 * Método de recuperação do campo returnCodetype
	 *
	 * @return valor do campo returnCodetype
	 */
	public ReturnCodeType getReturnCodetype() {
		return returnCodetype;
	}
	
	/**
	 * Valor de returnCodetype atribuído a returnCodetype
	 *
	 * @param returnCodetype Atributo da Classe
	 */
	public void setReturnCodetype(ReturnCodeType returnCodetype) {
		this.returnCodetype = returnCodetype;
	}

	public MacadameList<Protocol> getProtocols() {
		return protocols;
	}

	public void setProtocols(MacadameList<Protocol> protocols) {
		this.protocols = protocols;
	}
	
	
	
}
