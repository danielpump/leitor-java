/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 30/08/2013<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import java.math.BigDecimal;
import java.util.Calendar;

import com.compsis.macadame.domain.MacadameEntity;
import com.compsis.media.domain.Media;
import com.compsis.mop.domain.MeanOfPayment;
import com.compsis.site.domain.Classification;
import com.compsis.vehicle.domain.VehicleClass;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Classe de dominio Vale Pegadio.
 * Representa passagens compradas ou realizadas <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 30/08/2013 - @author wcarvalho - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class TollVoucher extends MacadameEntity {

	public static final Integer PASSAGE_PURCHASED = Integer.valueOf(0);
	public static final Integer PASSAGE_CANCELLED = Integer.valueOf(1);
	public static final Integer PASSAGE_REALIZED = Integer.valueOf(2);
	
	
	
	/**
	 * Categoria do veiculo, para qual o vale foi comprado
	 */
	private VehicleClass vehicleClass;
	
	/**
	 * Sentido para qual o vale foi comprado
	 */
	private Classification classification;
	
	
	/**
	 * mídia para o vale comprado
	 */
	private Media media;


	private MeanOfPayment mop;
	
	/**
	 * Passagem realizada com o vale pedagio
	 */
	private Passage passage;
	
	/**
	 * Número da viagem
	 */
	private Long travelNumber;
	
	/**
	 * Valor do vale pedagio
	 */
	private BigDecimal value;
	
	/**
	 * status do vale pedagio
	 * 0 - comprada
	 * 1 - cancelada
	 */
	private Integer status;
	
	/**
	 * data da viagem
	 */
	private Calendar travelDate;
	
	/**
	 * data de quando foi comprada ou cancelado
	 */
	private Calendar statusDate;

	/**
	 * Método de recuperação do campo vehicleClass
	 * Categoria do veiculo, para qual o vale foi comprado
	 * @return valor do campo vehicleClass
	 */
	public VehicleClass getVehicleClass() {
		return vehicleClass;
	}

	/**
	 * Valor de vehicleClass atribuído a vehicleClass
	 * Categoria do veiculo, para qual o vale foi comprado
	 * @param vehicleClass Atributo da Classe
	 */
	public void setVehicleClass(VehicleClass vehicleClass) {
		this.vehicleClass = vehicleClass;
	}



	public Classification getClassification() {
		return classification;
	}

	public void setClassification(Classification classification) {
		this.classification = classification;
	}

	/**
	 * Método de recuperação do campo media
	 *  mídia para o vale comprado
	 * @return valor do campo mediaConfiguration
	 */
	public Media getMedia() {
		return media;
	}

	/**
	 * Valor de media atribuído a mediaConfiguration
	 * mídia para o vale comprado
	 * @param mediaConfiguration Atributo da Classe
	 */
	public void setMedia(Media media) {
		this.media = media;
	}


	/**
	 * Método de recuperação do campo travelNumber
	 * Número da viagem
	 * @return valor do campo travelNumber
	 */
	public Long getTravelNumber() {
		return travelNumber;
	}

	/**
	 * Valor de travelNumber atribuído a travelNumber
	 * Número da viagem
	 * @param travelNumber Atributo da Classe
	 */
	public void setTravelNumber(Long travelNumber) {
		this.travelNumber = travelNumber;
	}

	/**
	 * Método de recuperação do campo value
	 * Valor do vale pedagio
	 * @return valor do campo value
	 */
	public BigDecimal getValue() {
		return value;
	}

	/**
	 * Valor de value atribuído a value
	 * Valor do vale pedagio
	 * @param value Atributo da Classe
	 */
	public void setValue(BigDecimal value) {
		this.value = value;
	}

	/**
	 * Método de recuperação do campo status
	 * status do vale pedagio
	 * 0 - comprada
	 * 1 - cancelada
	 * @return valor do campo status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * Valor de status atribuído a status
	 * status do vale pedagio
	 * 0 - comprada
	 * 1 - cancelada
	 * @param status Atributo da Classe
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * Método de recuperação do campo travelDate
	 * data da viagem
	 * @return valor do campo travelDate
	 */
	public Calendar getTravelDate() {
		return travelDate;
	}

	/**
	 * Valor de travelDate atribuído a travelDate
	 * data da viagem
	 * @param travelDate Atributo da Classe
	 */
	public void setTravelDate(Calendar travelDate) {
		this.travelDate = travelDate;
	}

	/**
	 * Método de recuperação do campo statusDate
	 * data de quando foi comprada ou cancelado
	 * @return valor do campo statusDate
	 */
	public Calendar getStatusDate() {
		return statusDate;
	}

	/**
	 * Valor de statusDate atribuído a statusDate
	 * data de quando foi comprada ou cancelado
	 * @param statusDate Atributo da Classe
	 */
	public void setStatusDate(Calendar statusDate) {
		this.statusDate = statusDate;
	}

	/**
	 * Método de recuperação do campo passage
	 * Passagem realizada com o vale pedagio
	 * @return valor do campo passage
	 */
	public Passage getPassage() {
		return passage;
	}

	/**
	 * Valor de passage atribuído a passage
	 * Passagem realizada com o vale pedagio
	 * @param passage Atributo da Classe
	 */
	public void setPassage(Passage passage) {
		this.passage = passage;
	}

	/**
	 * Meio de pagamento da passagem
	 * @return mop
	 */
	public MeanOfPayment getMop() {
		return mop;
	}

	/**
	 * Meio de pagamento da passagem
	 * @param mop
	 */
	public void setMop(MeanOfPayment mop) {
		this.mop = mop;
	}

	
	
}
