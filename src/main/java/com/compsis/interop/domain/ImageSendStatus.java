/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 26/05/2014<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.domain.DomainSpecificEntity;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 26/05/2014 - @author dferraz - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class ImageSendStatus extends DomainSpecificEntity {
	
	public ImageSendStatus(){}
	
	public ImageSendStatus(String code){
		setCode(code);
	}
	
	/**
	 * Não enviar - Valor: NENV
	 */
	public static final String DO_NOT_SEND = "NENV";
	
	/**
	 * Aguardando envio - Valor: AENV
	 */
	public static final String WATING = "AENV";
	
	/**
	 * Enviada - Valor: ENV
	 */
	public static final String SENT = "ENV";


}
