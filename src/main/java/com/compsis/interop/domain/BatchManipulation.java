/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 03/06/2013<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.collections.MacadameList;
import com.compsis.macadame.domain.MacadameEntity;
import com.compsis.macadame.security.domain.SecurityOperator;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Classe que representa uma Manipulação de Lotes. <br>
 * Utilizada como entidade navegável para Manipulação de Lotes.<br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 03/06/2013 - @author Vinícius O. Ueda - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class BatchManipulation extends MacadameEntity{
	/**
	 * Lista de transações do lote
	 */
	private MacadameList<Transaction> transactions;
	
	/**
	 * Lote a ser manipulado
	 */
	private Batch batch;
	
	/**
	 * Operação realizada no lote (ex: liberação, envio, etc).
	 * As chaves estão em BatchOperation
	 */
	private String currentOperation;
	
	/**
	 * Operador que realizou a manipulação
	 */
	private SecurityOperator operator;
	
	/**
	 * Modo de execução
	 */
	private ExecutionMode executionMode;
	
	/**
	 * Método de recuperação do campo transactions
	 *
	 * @return valor do campo transactions
	 */
	public MacadameList<Transaction> getTransactions() {
		return transactions;
	}
	/**
	 * Valor de transactions atribuído a transactions
	 *
	 * @param transactions Atributo da Classe
	 */
	public void setTransactions(MacadameList<Transaction> transactions) {
		this.transactions = transactions;
	}
	/**
	 * Método de recuperação do campo batch
	 *
	 * @return valor do campo batch
	 */
	public Batch getBatch() {
		return batch;
	}
	/**
	 * Valor de batch atribuído a batch
	 *
	 * @param batch Atributo da Classe
	 */
	public void setBatch(Batch batch) {
		this.batch = batch;
	}

	/**
	 * Método de recuperação do campo currentOperation
	 *
	 * @return valor do campo currentOperation
	 */
	public String getCurrentOperation() {
		return currentOperation;
	}
	/**
	 * Valor de currentOperation atribuído a currentOperation
	 *
	 * @param currentOperation Atributo da Classe
	 */
	public void setCurrentOperation(String currentOperation) {
		this.currentOperation = currentOperation;
	}
	/**
	 * Método de recuperação do campo operator
	 *
	 * @return valor do campo operator
	 */
	public SecurityOperator getOperator() {
		return operator;
	}
	/**
	 * Valor de operator atribuído a operator
	 *
	 * @param operator Atributo da Classe
	 */
	public void setOperator(SecurityOperator operator) {
		this.operator = operator;
	}
	/**
	 * Método de recuperação do campo executionMode
	 *
	 * @return valor do campo executionMode
	 */
	public ExecutionMode getExecutionMode() {
		return executionMode;
	}
	/**
	 * Valor de executionMode atribuído a executionMode
	 *
	 * @param executionMode Atributo da Classe
	 */
	public void setExecutionMode(ExecutionMode executionMode) {
		this.executionMode = executionMode;
	}
	
}
