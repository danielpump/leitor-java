/*
 * COMPSIS - Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto Interop - Domain<br>
 *
 * Data de Criação: 29/11/2012<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.domain.DomainSpecificEntity;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Tipo de Passagem
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 29/11/2012 - @author vueda - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class TransactionType extends DomainSpecificEntity {
	/**
	 * Passagem - Valor: P
	 */
	public final static String PASSAGE = "P";
	/**
	 * Recarga - Valor: R
	 */
	public final static String RELOAD = "R";

	 /**
	  * Construtor padrão da classe
	  */
	 public TransactionType(){}
	 
	 /**
	  * Construtor alternativo da classe
	  * @param aCode
	  */
	 public TransactionType(final String aCode){
		 setCode(aCode);
	 }
}
