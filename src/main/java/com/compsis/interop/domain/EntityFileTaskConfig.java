/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto SICAT - Interop<br>
 *
 * Data de Criação: 29/05/2013<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;


import java.util.HashMap;
import java.util.Map;

import com.compsis.interop.domain.interfaces.FileTaskConfig;
import com.compsis.macadame.domain.MacadameFile;
import com.compsis.protocol.domain.FileInformation;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Classe que representa uma tarefa de transferencia de 
 * lotes de passagens com uma OSA através de arquivos. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 29/05/2013 - @author Naresh Trivedi - Primeiva versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
@SuppressWarnings("serial")
public class EntityFileTaskConfig extends EntityTaskConfig implements FileTaskConfig {

	/**
	 * Arquivos utilizados na execução da tarefa.
	 */
	private Map<String, MacadameFile> files;
	
	/**
	 * Arquivos inválidos identificados durante a tarefa.
	 */
	private Map<String, MacadameFile> invalidFiles;
	
	/**
	 * Representação dos objetos de informações dos arquivos.
	 */
	private Map<String, FileInformation> filesInformations;
	
	
	public EntityFileTaskConfig(){
		invalidFiles = new HashMap<String, MacadameFile>();
	}

	/**
	 * Método de recuperação do campo files
	 *
	 * @return valor do campo files
	 */
	public Map<String, MacadameFile> getFiles() {
		return files;
	}

	/**
	 * Valor de files atribuído a files
	 *
	 * @param files Atributo da Classe
	 */
	public void setFiles(Map<String, MacadameFile> files) {
		this.files = files;
	}

	/**
	 * Método de recuperação do campo invalidFiles
	 *
	 * @return valor do campo invalidFiles
	 */
	public Map<String, MacadameFile> getInvalidFiles() {
		return invalidFiles;
	}

	/**
	 * Valor de invalidFiles atribuído a invalidFiles
	 *
	 * @param invalidFiles Atributo da Classe
	 */
	public void setInvalidFiles(Map<String, MacadameFile> invalidFiles) {
		this.invalidFiles = invalidFiles;
	}

	/**
	 * Método de recuperação do campo filesInformations
	 *
	 * @return valor do campo filesInformations
	 */
	public Map<String, FileInformation> getFilesInformations() {
		return filesInformations;
	}

	/**
	 * Valor de filesInformations atribuído a filesInformations
	 *
	 * @param filesInformations Atributo da Classe
	 */
	public void setFilesInformations(Map<String, FileInformation> filesInformations) {
		this.filesInformations = filesInformations;
	}
	
}
