/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto Interop - Domain<br>
 *
 * Data de Criação: 21/01/2013<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.domain.DomainSpecificEntity;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Tipo do InteropSetting <br>
 * ex: Enviar Isentos, Enviar Violadores, Enviar violadores em pista fechada, etc
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 21/01/2013 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class InteropSettingType extends DomainSpecificEntity implements Cloneable {
	
	private String classification;
	
	private boolean edit;
	
	
	/**
	 * Método de recuperação do campo edit
	 *
	 * @return valor do campo edit
	 */
	public boolean isEdit() {
		return edit;
	}

	/**
	 * Valor de edit atribuído a edit
	 *
	 * @param edit Atributo da Classe
	 */
	public void setEdit(boolean edit) {
		this.edit = edit;
	}

	/**
	 * Método de recuperação do campo classification
	 *
	 * @return valor do campo classification
	 */
	public String getClassification() {
		return classification;
	}

	/**
	 * Valor de classification atribuído a classification
	 *
	 * @param classification Atributo da Classe
	 */
	public void setClassification(String classification) {
		this.classification = classification;
	}

	/**
	 * Enviar Isentos - Valor: 1
	 */
	public static final String SEND_EXCEMPT = "1";
	
	/**
	 * Enviar violadores ocorridos em pista manual aberta - Valor: 3
	 */
	public static final String SEND_VIOLATORS_OCCURRING_IN_MANUAL_LANE_OPEN = "3"; 
	
	/**
	 * Enviar violadores ocorridos em pista manual aberta - Valor: 4
	 */
	public static final String SEND_VIOLATORS_OCCURRING_IN_AUTOMATIC_LANE_OPEN = "4"; 
	
	/**
	 * Enviar violadores ocorridos em pista mista não exclusiva aberta - Valor: 5
	 */
	public static final String SEND_VIOLATORS_OCCURRING_IN_LANE_MIXED_NONEXCLUSIVE_OPEN = "5"; 
	
	/**
	 * Enviar violadores ocorridos em pista manual fechada - Valor: 6
	 */
	public static final String SEND_VIOLATORS_OCCURRING_IN_MANUAL_LANE_CLOSED = "6"; 
	
	/**
	 * Enviar violadores ocorridos em pista automática fechada - Valor: 7
	 */
	public static final String SEND_VIOLATORS_OCCURRING_IN_AUTOMATIC_LANE_CLOSED = "7"; 
	
	/**
	 * Enviar violadores ocorridos em pista mista não exclusiva fechada - Valor: 8
	 */
	public static final String SEND_VIOLATORS_OCCURRING_IN_MIXED_NONEXCLUSIVE_LANE_CLOSED = "8";
	
	/**
	 * Enviar violadores automaticamente - Valor: 9
	 */
	public static final String SEND_VIOLATORS_AUTOMATIC = "9";
	
	/**
	 * Enviar violadores - Valor: 10
	 */
	public static final String SEND_VIOLATORS = "10";

	/**
	 * Quantidade de passagens no lote - Valor: 12
	 */
	public static final String MAXIMUM_NUMBER_OF_TRANSACTIONS = "12";
	
	/**
	 * Agrupar isentos em lote único - Valor: 13
	 */
	public static final String GROUPING_EXEMPT_SINGLE_BATCH = "13";
	
	/**
	 * Agrupar violadores em lote único - Valor: 14
	 */
	public static final String GROUPING_VIOLATORS_SINGLE_BATCH = "14";
	
	/**
	 * Agrupar por data da ocorrência - Valor: 15
	 */
	public static final String SORT_BY_OCCURENCE_DATE = "15";
	
	/**
	 * Agrupar por local de ocorrência - Valor: 16
	 */
	public static final String GROUP_BY_PLACE_OF_OCCURRENCE = "16";
	
	/**
	 * Agrupar por tipo de passagem - Valor: 17
	 */
	public static final String GROUP_BY_TYPE_OF_PASSAGE = "17";
	
	/**
	 * Agrupar por modo de correção - Valor: 18
	 */
	public static final String GROUP_BY_METHOD_OF_CORRECTION = "18";
	
	/**
	 * Agrupar por modo de leitura da mídia - Valor: 19
	 */
	public static final String GROUP_BY_METHOD_OF_READING_MEDIA = "19";
	
	/**
	 * Valor do numero de serie do envio da mensagem - Valor: 20
	 */
	public static final String MESSAGE_SERIAL_NUMBER = "20";
	
	/**
	 * Identificação da OSA - Valor: 21
	 */
	public static final String OSA_ID = "21";
	
	/**
	 * Permite manipulação de transação com status 
	 */
	public static final String ALLOW_MANIPULATION_WAITING = "22";
	/**
	 * Permite manipulação de transação com status 
	 */
	public static final String ALLOW_MANIPULATION_AWAITING_SUBMISSION = "23";
	/**
	 * Permite manipulação de transação com status 
	 */
	public static final String ALLOW_MANIPULATION_AWAITING_RETURN = "24";
	/**
	 * Permite manipulação de transação com status 
	 */
	public static final String ALLOW_MANIPULATION_APPROVED = "25";
	/**
	 * Permite manipulação de transação com status 
	 */
	public static final String ALLOW_MANIPULATION_REJECTED = "26";
	/**
	 * Permite manipulação de transação com status 
	 */
	public static final String ALLOW_MANIPULATION_NO_CHARGE = "27";
	/**
	 * Permite manipulação de transação com status 
	 */
	public static final String ALLOW_MANIPULATION_REPLACED = "28";
	/**
	 * Permite manipulação de transação com status 
	 */
	public static final String ALLOW_MANIPULATION_PAID = "29";
	/**
	 * Permite manipulação de transação com status 
	 */
	public static final String ALLOW_MANIPULATION_PAID_OTHER_VALUE = "30";
	/**
	 * Permite manipulação de transação com status 
	 */
	public static final String ALLOW_MANIPULATION_PROVISIONED = "31";
	
	public InteropSettingType clone(){
		InteropSettingType cloned = new InteropSettingType();
		cloned.setId(getId());
		cloned.setActive(getActive());
		cloned.setClassification(getClassification());
		cloned.setCode(getCode());
		cloned.setEdit(isEdit());
		cloned.setInsertionDate(getInsertionDate());		
		
		return cloned;
	}

}
