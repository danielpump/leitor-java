/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto SICAT - Interop <br>
 *
 * Data de Cria��o: 18/09/2012<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import java.math.BigDecimal;
import java.util.Calendar;

import com.compsis.image.domain.Image;
import com.compsis.macadame.collections.MacadameList;
import com.compsis.macadame.domain.MacadameEntity;
import com.compsis.macadame.domain.Site;
import com.compsis.media.domain.Media;
import com.compsis.mop.domain.MeanOfPayment;
import com.compsis.operator.domain.OperatorDetailToll;
import com.compsis.vehicle.domain.Vehicle;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Opera��o financeira realizada com um gestor de meio de pagamento. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 18/09/2012 - @author Naresh Trivedi - Primeiva vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class Operation extends MacadameEntity {
	
	private Calendar transactionDate;
	private Long transactionSeq;
	private Integer operationSeq;
	private BigDecimal value;
	private Batch batch;
	private Site site;
	
	/** Meio de pagamento utilizado da opera��o **/
	private MeanOfPayment meanOfPayment;
	
	/** Media da opera��o **/
	private Media media;
	
	/** Imagens da transa��o **/
	private MacadameList<Image> images;

	/** Id da transa��o do SGAP **/
	private String transactionId;
	
	/** Operador da Opera��o **/
	private OperatorDetailToll operator;
	
	/** ve�culo detectado (especifica��o do perfil de eixos ser� dada neste atributo)**/
	private Vehicle vehicle;
	
	/** Tipo de corre��o da Transa��o **/
	private CorrectionMode correctionMode;
	
	/** Tipo da transa��o **/
	private TransactionType transactionType;
	
	/** Informa se a leitura do tag foi pela antena ou manual **/
	private TransactionInput transactionInput;
	
	/**
	 * Valor de meanOfPayment atribu�do a meanOfPayment
	 *
	 * @param meanOfPayment Atributo da Classe
	 */
	public void setMeanOfPayment(MeanOfPayment meanOfPayment) {
		this.meanOfPayment = meanOfPayment;
	}
	
	/**
	 * M�todo de recupera��o do campo meanOfPayment
	 *
	 * @return valor do campo meanOfPayment
	 */
	public MeanOfPayment getMeanOfPayment() {
		return this.meanOfPayment;
	}
	
	/**
	 * Valor de media atribu�do a media
	 *
	 * @param aMedia Atributo da Classe
	 */
	public void setMedia(final Media aMedia) {
		this.media = aMedia;
	}
	
	/**
	 * M�todo de recupera��o do campo media
	 *
	 * @return valor do campo media
	 */
	public Media getMedia() {
		return this.media;
	}
	
	/**
	 * Valor de images atribu�do a images
	 *
	 * @param images Atributo da Classe
	 */
	public void setImages(MacadameList<Image> images) {
		this.images = images;
	}
	
	/**
	 * M�todo de recupera��o do campo images
	 *
	 * @return valor do campo images
	 */
	public MacadameList<Image> getImages() {
		return this.images;
	}
	
	/**
	 * Valor de transactionId atribu�do a transactionId
	 *
	 * @param transactionId Atributo da Classe
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	/**
	 * M�todo de recupera��o do campo transactionId
	 *
	 * @return valor do campo transactionId
	 */
	public String getTransactionId() {
		return this.transactionId;
	}
	
	/**
	 * M�todo de recupera��o do campo transactionInput
	 *
	 * @return valor do campo transactionInput
	 */
	public TransactionInput getTransactionInput() {
		return this.transactionInput;
	}
	
	/**
	 * Valor de transactionInput atribu�do a transactionInput
	 *
	 * @param transactionInput Atributo da Classe
	 */
	public void setTransactionInput(TransactionInput transactionInput) {
		this.transactionInput = transactionInput;
	}
	
	/**
	 * Valor de transactionType atribu�do a transactionType
	 *
	 * @param transactionType Atributo da Classe
	 */
	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}
	
	/**
	 * M�todo de recupera��o do campo transactionType
	 *
	 * @return valor do campo transactionType
	 */
	public TransactionType getTransactionType() {
		return this.transactionType;
	}
	
	/**
	 * Valor de operator atribu�do a operator
	 *
	 * @param operator Atributo da Classe
	 */
	public void setOperator(OperatorDetailToll operator) {
		this.operator = operator;
	}
	
	/**
	 * M�todo de recupera��o do campo operator
	 *
	 * @return valor do campo operator
	 */
	public OperatorDetailToll getOperator() {
		return this.operator;
	}
	
	/**
	 * Valor de vehicle atribu�do a vehicle
	 *
	 * @param vehicle Atributo da Classe
	 */
	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}
	
	/**
	 * M�todo de recupera��o do campo vehicle
	 *
	 * @return valor do campo vehicle
	 */
	public Vehicle getVehicle() {
		return this.vehicle;
	}
	
	/**
	 * Valor de correctionMode atribu�do a correctionMode
	 *
	 * @param correctionMode Atributo da Classe
	 */
	public void setCorrectionMode(CorrectionMode correctionMode) {
		this.correctionMode = correctionMode;
	}
	
	/**
	 * M�todo de recupera��o do campo correctionMode
	 *
	 * @return valor do campo correctionMode
	 */
	public CorrectionMode getCorrectionMode() {
		return this.correctionMode;
	}
	
	/**
	 * M�todo de recupera��o do campo origin
	 *
	 * @return valor do campo origin
	 */
	public Site getSite() {
		return this.site;
	}
	
	/**
	 * Valor de origin atribu�do a origin
	 *
	 * @param origin Atributo da Classe
	 */
	public void setOrigin(Site site) {
		this.site = site;
	}
	
	/**
	 * M�todo de recupera��o do campo transactionDate
	 *
	 * @return valor do campo transactionDate
	 */
	public Calendar getTransactionDate() {
		return this.transactionDate;
	}
	/**
	 * Valor de transactionDate atribu�do a transactionDate
	 *
	 * @param transactionDate Atributo da Classe
	 */
	public void setTransactionDate(final Calendar transactionDate) {
		this.transactionDate = transactionDate;
	}
	/**
	 * M�todo de recupera��o do campo transactionSeq
	 *
	 * @return valor do campo transactionSeq
	 */
	public Long getTransactionSeq() {
		return this.transactionSeq;
	}
	/**
	 * Valor de transactionSeq atribu�do a transactionSeq
	 *
	 * @param transactionSeq Atributo da Classe
	 */
	public void setTransactionSeq(final Long transactionSeq) {
		this.transactionSeq = transactionSeq;
	}
	/**
	 * M�todo de recupera��o do campo operationSeq
	 *
	 * @return valor do campo operationSeq
	 */
	public Integer getOperationSeq() {
		return this.operationSeq;
	}
	/**
	 * Valor de operationSeq atribu�do a operationSeq
	 *
	 * @param operationSeq Atributo da Classe
	 */
	public void setOperationSeq(final Integer operationSeq) {
		this.operationSeq = operationSeq;
	}
	/**
	 * M�todo de recupera��o do campo value
	 *
	 * @return valor do campo value
	 */
	public BigDecimal getValue() {
		return this.value;
	}
	/**
	 * Valor de value atribu�do a value
	 *
	 * @param value Atributo da Classe
	 */
	public void setValue(final BigDecimal value) {
		this.value = value;
	}
	/**
	 * M�todo de recupera��o do campo batch
	 *
	 * @return valor do campo batch
	 */
	public Batch getBatch() {
		return this.batch;
	}
	/**
	 * Valor de batch atribu�do a batch
	 *
	 * @param batch Atributo da Classe
	 */
	public void setBatch(final Batch batch) {
		this.batch = batch;
	}

	/** 
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Operation [transactionDate=");
		builder.append(this.transactionDate);
		builder.append(", transactionSeq=");
		builder.append(this.transactionSeq);
		builder.append(", operationSeq=");
		builder.append(this.operationSeq);
		builder.append(", value=");
		builder.append(this.value);
		builder.append(", batch=");
		builder.append(this.batch);
		builder.append(", origin=");
		builder.append(this.site);
		builder.append(", meanOfPayment=");
		builder.append(this.meanOfPayment);
		builder.append(", media=");
		builder.append(this.media);
		builder.append(", images=");
		builder.append(this.images);
		builder.append(", transactionId=");
		builder.append(this.transactionId);
		builder.append(", operator=");
		builder.append(this.operator);
		builder.append(", vehicle=");
		builder.append(this.vehicle);
		builder.append(", correctionMode=");
		builder.append(this.correctionMode);
		builder.append(", transactionType=");
		builder.append(this.transactionType);
		builder.append(", transactionInput=");
		builder.append(this.transactionInput);
		builder.append("]");
		return builder.toString();
	}
	
	
}
