/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 23/10/2014<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.domain.MacadameObject;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 23/10/2014 - @author fpereira - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class EntityTaskLog extends MacadameObject {
	
	/**
	 * Atributo que representa Code da Task executada
	 */
	private String taskCode;
	
	/**
	 * Atributo que representa Code da OSA
	 */
	private String osaCode;

	/** 
	 * retorna o code da task onde ocorreu o erro
	 * @return
	 */
	public String getTaskCode() {
		return taskCode;
	}
	/** 
	 * seta  o code da task
	 * @return
	 */
	public void setTaskCode(String taskCode) {
		this.taskCode = taskCode;
	}
	
	/** 
	 * retorna o code da OSA 
	 * @return
	 */
	public String getOsaCode() {
		return osaCode;
	}
	
	/** 
	 * seta  o code da OSA 
	 * @return
	 */
	public void setOsaCode(String osaCode) {
		this.osaCode = osaCode;
	}
	
	
}
