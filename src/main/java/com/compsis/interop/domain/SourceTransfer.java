/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * SICAT - Interop<br>
 *
 * Data de Cria��o: 25/10/2012<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.domain.DomainSpecificEntity;
import com.compsis.mop.domain.MeanOfPayment;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Classe que representa uma Fonte de Repasse
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 25/10/2012 - @author vueda - Primeiva vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class SourceTransfer extends DomainSpecificEntity {
	/**
	 * Meio de pagamento associado
	 */
	private MeanOfPayment mop;

	/**
	 * Método de recuperação do campo mop
	 *
	 * @return valor do campo mop
	 */
	public MeanOfPayment getMop() {
		return mop;
	}

	/**
	 * Valor de mop atribuído a mop
	 *
	 * @param mop Atributo da Classe
	 */
	public void setMop(MeanOfPayment mop) {
		this.mop = mop;
	}
		
}
