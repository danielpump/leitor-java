/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Interop-CORE<br>
 *
 * Data de Criação: 07/11/2013<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.domain.MacadameObject;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Responsável por definir como uma determinada tarefa será executada. <br>
 * Definição das fases da execução da tarefa. <br>
 * As tarefas podem ter 3 fases: <br>
 * - Prepare <br>
 * - Perform <br>
 * - Finally <br>
 * 
 * As navegações invocadas serão chamadas seguindo o padrão: <br>
 * -> PREPARE#CONTEXT_NAME <br>
 * -> PERFORM#CONTEXT_NAME <br>
 * -> FINALLY#CONTEXT_NAME <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 07/11/2013 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class TaskDefinition extends MacadameObject {
	/**
	 * Nome do contexto da tarefa <br>
	 * A implementação do contexto deverá ser implementada da seguinda maneira: <br>
	 * -> PREPARE#CONTEXT_NAME <br>
	 * -> PERFORM#CONTEXT_NAME <br>
	 * -> FINALLY#CONTEXT_NAME <br>
	 */
	private String contextName;
	/**
	 * Flag que indica se irá logar exceptions no log do arquivo da execução da tarefa
	 */
	private boolean logExceptions;
	
	public void setContextName(String contextName) {
		this.contextName = contextName;
	}
	
	/**
	 * Obtem o nome da navegação para o prepare
	 * @return
	 */
	public String getPrepareContext(){
		return new StringBuffer("PREPARE#").append(contextName).toString();
	}
	
	/**
	 * Obtem o nome da navegação para o perform
	 * @return
	 */
	public String getPerformContext(){
		return new StringBuffer("PERFORM#").append(contextName).toString();		
	}
	
	/**
	 * Obtem o nome da navegação para o finally
	 * @return
	 */
	public String getFinallyContext(){
		return new StringBuffer("FINALLY#").append(contextName).toString();				
	}
	
	/**
	 * Nome do contexto
	 *
	 * @return valor do campo contextName
	 */
	public String getContextName() {
		return contextName;
	}
	
	/**
	 * Valor de logExceptions atribuído a logExceptions
	 * Flag que indica se irá logar exceptions no log do arquivo da execução da tarefa
	 * @param logExceptions Atributo da Classe
	 */
	public void setLogExceptions(boolean logExceptions) {
		this.logExceptions = logExceptions;
	}
	
	/**
	 * Método de recuperação do campo logExceptions
	 * Flag que indica se irá logar exceptions no log do arquivo da execução da tarefa
	 * @return valor do campo logExceptions
	 */
	public boolean isLogExceptions() {
		return logExceptions;
	}
}
