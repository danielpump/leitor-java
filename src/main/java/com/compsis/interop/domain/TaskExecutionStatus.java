/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * SICAT - Componentente Interop<br>
 *
 * Data de Criação: 04/09/2013<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.domain.DomainSpecificEntity;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Representa os estados da execução de tarefas. 
 * Os possíveis estados(até o atual momento) são:<br>
 *  - Sucesso: CODE = 'SUCC'<br>
 *  - Falha: CODE = 'FAIL'<br>
 *  - Em execução: CODE='RUNN'<br>
 *  - Aplicação reiniciada: CODE='RSTD'<br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 04/09/2013 - @author Vinícius O. Ueda - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class TaskExecutionStatus extends DomainSpecificEntity{
	/**
	 * Construtor padrão da classe
	 */
	public TaskExecutionStatus(){}
	/**
	 * Construtor alternativo da classe
	 * @param aCode
	 */
	public TaskExecutionStatus(final String aCode){
		super.setCode(aCode);
	}
	//Constantes referentes ao possíveis estado de execução.
	public static final String FINISHED = "FINS";
	public static final String RUNNING = "RUNN";
	public static final String APPLICATION_RESTARTED = "RSTD";

}
