/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto Interop - DOMAIN<br>
 *
 * Data de Criação: 10/12/2013<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.domain.MacadameEntity;
import com.compsis.protocol.domain.ExecutionDetailType;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Entidade que representa o controle de sequencial das tarefas para cada InteropConfiguration. <br>
 * Poderá ser implementada uma estratégia para cada protocolo.
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 10/12/2013 - @author Lucas Israel - Primeira versão da classe. <br>
 * 03/04/2014 - @author Vinicius O. Ueda - Refactor de controle de seq para todas as tarefas. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class InteropSequential extends MacadameEntity{
	private String sequential;
	private AbstractInteropConfig interopConfig;
	
	/**
	 * Tipo de tarefa do sequencial
	 */
	private ExecutionDetailType taskType;
	
	/**
	 * Identifica se o registro foi encontrado na base de dados quando consultado ou se
	 * foi criado um com o sequencial inicial
	 */
	private boolean foundRegister = false;
	
	/**
	 * Representa o sequencial 
	 * @return
	 */
	public String getSequential() {
		return sequential;
	}
	/**
	 * Representa o sequencial
	 * @param sequential
	 */
	public void setSequential(String sequential) {
		this.sequential = sequential;
	}
	/**
	 * InteropConfig que este controle de sequencial pertence
	 * @return
	 */
	public AbstractInteropConfig getInteropConfig() {
		return interopConfig;
	}
	/**
	 * InteropConfig que este controle de sequencial pertence
	 * @param interopConfig
	 */
	public void setInteropConfig(AbstractInteropConfig interopConfig) {
		this.interopConfig = interopConfig;
	}
	
	/** 
	 * Getter de taskType
	 * @return
	 */
	public ExecutionDetailType getTaskType() {
		return taskType;
	}
	
	/** 
	 * Setter de taskType
	 * @param taskType
	 */
	public void setTaskType(ExecutionDetailType taskType) {
		this.taskType = taskType;
	}
	
	
	/**
	 * Método de recuperação do campo foundRegister
	 *
	 * @return valor do campo foundRegister
	 */
	public boolean isFoundRegister() {
		return foundRegister;
	}
	
	/**
	 * Valor de foundRegister atribuído a foundRegister
	 *
	 * @param foundRegister Atributo da Classe
	 */
	public void setFoundRegister(boolean foundRegister) {
		this.foundRegister = foundRegister;
	}
	
	/** 
	 * Incrementa o sequencial de acordo com o valor informado.
	 * Deve ser utilizado para sequenciais numéricos apenas
	 * @param value - Valor que deve ser incrementado
	 */
	public void incrementSequential(Long value){
		Long currentValue = new Long(getSequential());
		Long newValue = currentValue + value;
		setSequential(newValue.toString());
	}
	
}
