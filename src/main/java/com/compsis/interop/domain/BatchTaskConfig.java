/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 05/06/2013<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.collections.Collections;
import com.compsis.macadame.collections.MacadameList;

/**
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TaskConfig de tarefas relacionadas à lote. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 05/06/2013 - @author Vinícius O. Ueda - Primeira versão da classe. <br>
 * 26/05/2014 - @author dcarvalho - Alteração de herança de EntityFileTaskConfig para TransactionGroupTaskConfig após a mudança do modelo no EA. <br>
 * <br>
 * <br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
@SuppressWarnings("serial")
public class BatchTaskConfig extends TransactionGroupTaskConfig {

	/**
	 * Construtor padrão
	 */
	public BatchTaskConfig() {
	}

	/**
	 * Lista de Passagens apenas para a navegação de geração de lote
	 */
	private transient MacadameList<? extends Transaction> transactions;
	
	private MacadameList<Batch> batches;
	

	/**
	 * Valor de batches atribuído a transactions
	 * 
	 * @param transactions
	 *            Atributo da Classe
	 */
	public void setTransactions(MacadameList<Passage> transactions) {
		this.transactions = transactions;
	}

	/**
	 * Método de recuperação do campo transactions
	 * @return 
	 * 
	 * @return valor do campo transactions
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public <E extends MacadameList> E getTransactions() {
		return (E) transactions;
	}

	
	public MacadameList<Batch> getBatches() {
		
		if(batches == null) {
			batches = Collections.newArrayList();
		}
		
		return batches;
	}

	public void setBatches(MacadameList<Batch> batches) {
		this.batches = batches;
	}
}
