/*
 * COMPSIS - Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto SGAP - Interop<br>
 *
 * Data de Criação: 24/09/2012<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.domain.DomainSpecificEntity;


/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Classe para especificar o tipo de correção da transação. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 24/09/2012 - @author Lucas Israel - Primeiva versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class CorrectionMode extends DomainSpecificEntity{
	/** Transações Sem Correções **/
	public static final String NO_CORRECTION = "SC"; 
	/** Transações com Correções Manual **/
	public static final String MANUAL_CORRECTION = "MN"; 
	/** Transações com Correção Automática **/
	public static final String AUTOMATIC_CORRECTION = "AT";
	
	public CorrectionMode(){};
	
	public CorrectionMode(String correctionMode){
		setCode(correctionMode);
	}
}
