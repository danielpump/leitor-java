/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 29/11/2012<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.domain.DomainSpecificEntity;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Define o tipo de entrada da transa��o. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 29/11/2012 - @author vueda - Primeira vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class MediaInputMode extends DomainSpecificEntity {
	/** Leitura do TAG foi autom�tica, ou seja, foi informado pela antena **/
	public static final String AUTOMATIC = "AT";
	/** Leitura do TAG foi manual, ou seja, foi informado manualmente pelo operador **/
	public static final String MANUAL = "MN";
	/** SEM LEITURA **/
	public static final String NO_READ = "SL";
	/**
	 * Inserção Manual
	 */
	public static final String MANUAL_INSERTION = "IM";
	
	public MediaInputMode(){
	}
	public MediaInputMode(final String aCode){
		setCode(aCode);
	}
}
