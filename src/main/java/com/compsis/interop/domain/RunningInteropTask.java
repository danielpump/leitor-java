/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto Interop - Domain<br>
 *
 * Data de Criação: 13/06/2013<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.collections.MacadameArrayList;
import com.compsis.macadame.collections.MacadameList;
import com.compsis.macadame.domain.MacadameFile;
import com.compsis.macadame.entity.IEntity;
import com.compsis.macadame.navigator.annotation.Navigable;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Entidade que representa o processo de execução de tarefa 
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 13/06/2013 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class RunningInteropTask extends InteropTask {
	private TaskExecution runningTask;
	private Paths paths = new Paths(this);
	private TaskDefinition taskDefinition;
	/**
	 * Flag indicando se o {@link #runningTask} irá ser setado para null após a execução da tarefa
	 */
	private boolean gcRunningTask = true;
	
	/**
	 * Suspende a execução da tarefa em caso de falha.<br>
	 * Funciona da seguinte maneira:<br>
	 * - Ao iniciar a navegação a tarefa deve alterar esse flag para dizer o comportamento desejado.<br>
	 * - O flag como true indica que se existir algum erro na execução de um detalhe os outros não
	 * serão processados (Ex: Processamento de arquivos .TAG de sequencial 1,2,3 e 4. Se houver falha no nro 2 deve
	 * ser interrompido a navegação para que não sejam processados o 3 e 4).<br>
	 * - O flag como false indica que a execução dos outros detalhes acontecerá mesmo quando houver erro em um deles. 
	 */
	private boolean suspendTaskOnFail = false;
	
	/**
	 * Sequencial de tarefas. Pode possuir mais de uma entidade para tarefa de cadastro full
	 */
	private MacadameList<InteropSequential> interopSequential = new MacadameArrayList<InteropSequential>();
	private Integer taskLimit;
	
	private Integer taskSizeList;

	/** 
	 * Adiciona interopSequential
	 * @param sequential
	 */
	public void addInteropSequential(InteropSequential sequential){
		this.interopSequential.add(sequential);
	}
	/** 
	 * Getter de interopSequential
	 * @return
	 */
	public MacadameList<InteropSequential> getInteropSequential() {
		return interopSequential;
	}

	/** 
	 * Setter de interopSequential
	 * @param interopSequential
	 */
	public void setInteropSequential(MacadameList<InteropSequential> interopSequential) {
		this.interopSequential = interopSequential;
	}

	/**
	 * Define se a tarefa será interrompida quando houver erro.
	 * @param suspendTaskOnFail
	 */
	public void suspendTaskOnFail(boolean suspendTaskOnFail) {
		this.suspendTaskOnFail = suspendTaskOnFail;
	}
	
	/**
	 * Retorna o flag de identificação de suspensão da tarefa em caso de falha
	 * @return suspendTaskOnFail
	 */
	public boolean isTaskSuspendedOnFail(){
		return suspendTaskOnFail;
	}
	
	/**
	 * Método de recuperação do campo paths
	 *
	 * @return valor do campo paths
	 */
	@Navigable
	public Paths getPaths() {
		return paths;
	}

	/**
	 * Valor de paths atribuído a paths
	 *
	 * @param paths Atributo da Classe
	 */
	public void setPaths(Paths paths) {
		this.paths = paths;
	}
	
	/**
	 * Método de recuperação do campo runningTask
	 *
	 * @return valor do campo runningTask
	 */
	public TaskExecution getRunningTask() {
		return runningTask;
	}

	/**
	 * Valor de runningTask atribuído a runningTask
	 *
	 * @param runningTask Atributo da Classe
	 */
	public void setRunningTask(TaskExecution runningTask) {
		this.runningTask = runningTask;
	}
	

	public static class Paths implements IEntity{
		RunningInteropTask parent;
		
		/** 
		 * Construtor padrão da classe
		 */
		public Paths(final RunningInteropTask aDataFileTransferTaskConfig) {
			this.parent = aDataFileTransferTaskConfig;
		}
		/**
		 * Diretório de gravacao
		 */
		private MacadameFile sendPatch;
			
		/**
		 * Diretório de gravacao
		 */
		private MacadameFile writePatch;
		/**
		 * Diretório de leitura
		 */
		private MacadameFile readPatch;
		/**
		 * Diretório de trabalho
		 */
		private MacadameFile workPatch;
		/**
		 * Diretório de histórico de arquivos processados
		 */
		private MacadameFile historyPatch;
		/**
		 * Diretório de arquivos com erros no processamento
		 */
		private MacadameFile errorPatch;
		/**
		 * Diretório de armazenamento de LOG's dos processamentos
		 */
		private MacadameFile logPatch;
		/**
		 * Método de recuperação do campo readPatch
		 * Diretório de leitura
		 * @return valor do campo readPatch
		 */
		public MacadameFile getReadPatch() {
			return readPatch;
		}
		/**
		 * Valor de readPatch atribuído a readPatch
		 * Diretório de leitura
		 * @param readPatch Atributo da Classe
		 */
		public void setReadPatch(MacadameFile readPatch) {
			this.readPatch = readPatch;
		}
		/**
		 * Método de recuperação do campo workPatch
		 * Diretório de trabalho
		 * @return valor do campo workPatch
		 */
		public MacadameFile getWorkPatch() {
			return workPatch;
		}
		/**
		 * Valor de workPatch atribuído a workPatch
		 * Diretório de trabalho
		 * @param workPatch Atributo da Classe
		 */
		public void setWorkPatch(MacadameFile workPatch) {
			this.workPatch = workPatch;
		}
		/**
		 * Método de recuperação do campo historyPatch
		 * Diretório de histórico de arquivos processados
		 * @return valor do campo historyPatch
		 */
		public MacadameFile getHistoryPatch() {
			return historyPatch;
		}
		/**
		 * Valor de historyPatch atribuído a historyPatch
		 * Diretório de histórico de arquivos processados
		 * @param historyPatch Atributo da Classe
		 */
		public void setHistoryPatch(MacadameFile historyPatch) {
			this.historyPatch = historyPatch;
		}
		/**
		 * Método de recuperação do campo errorPatch
		 * Diretório de arquivos com erros no processamento
		 * @return valor do campo errorPatch
		 */
		public MacadameFile getErrorPatch() {
			return errorPatch;
		}
		/**
		 * Valor de errorPatch atribuído a errorPatch
		 * Diretório de arquivos com erros no processamento
		 * @param errorPatch Atributo da Classe
		 */
		public void setErrorPatch(MacadameFile errorPatch) {
			this.errorPatch = errorPatch;
		}
		/**
		 * Método de recuperação do campo logPatch
		 * Diretório de armazenamento de LOG's dos processamentos
		 * @return valor do campo logPatch
		 */
		public MacadameFile getLogPatch() {
			return logPatch;
		}
		/**
		 * Valor de logPatch atribuído a logPatch
		 * Diretório de armazenamento de LOG's dos processamentos
		 * @param logPatch Atributo da Classe
		 */
		public void setLogPatch(MacadameFile logPatch) {
			this.logPatch = logPatch;
		}	
		
		/**
		 * Método de recuperação do campo parent
		 *
		 * @return valor do campo parent
		 */
		public RunningInteropTask getParent() {
			return parent;
		}
		public MacadameFile getSendPatch() {
			return sendPatch;
		}
		public void setSendPatch(MacadameFile sendPatch) {
			this.sendPatch = sendPatch;
		}
		public MacadameFile getWritePatch() {
			return writePatch;
		}
		public void setWritePatch(MacadameFile writePatch) {
			this.writePatch = writePatch;
		}	
	}
	
	
	/**
	 * Valor de taskDefinition atribuído a taskDefinition
	 *
	 * @param taskDefinition Atributo da Classe
	 */
	public void setTaskDefinition(TaskDefinition taskDefinition) {
		this.taskDefinition = taskDefinition;
	}
	
	/**
	 * Método de recuperação do campo taskDefinition
	 *
	 * @return valor do campo taskDefinition
	 */
	public TaskDefinition getTaskDefinition() {
		return taskDefinition;
	}
	
	/**
	 * Método de recuperação do campo gcRunningTask <br>
	 * Flag indicando se o {@link #runningTask} irá ser setado para null após a execução da tarefa
	 * @return valor do campo gcRunningTask
	 */
	public boolean isGcRunningTask() {
		return gcRunningTask;
	}

	/**
	 * Flag indicando se o {@link #runningTask} irá ser setado para null após a execução da tarefa <br>
	 * Ao marcar este método, após a execução da tarefa, o {@link #runningTask} não irá ser removido.
	 */
	public RunningInteropTask preserveRunningTaskAfterRunner() {
		this.gcRunningTask = false;
		return this;
	}
	public void setTaskLimit(Integer taskLimit) {
		this.taskLimit = taskLimit;
	}
	
	public Integer getTaskLimit() {
		return taskLimit;
	}
	
	/**
	 * 
	 * Armazena o valor atual da lista de Entidades. Este valor é usado como parametro para agendar proxima tarefa.
	 * @return
	 */
	public Integer getTaskSizeList() {
		return taskSizeList;
	}
	public void setTaskSizeList(Integer taskSizeList) {
		this.taskSizeList = taskSizeList;
	}
}
