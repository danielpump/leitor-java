/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 26/05/2014<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import java.util.Calendar;

import com.compsis.macadame.collections.Collections;
import com.compsis.macadame.collections.MacadameList;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 26/05/2014 - @author dcarvalho - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
@SuppressWarnings("serial")
public class TransactionGroupTaskConfig extends EntityTaskConfig {
	
	/** Lista de transações já agrupadas de acordo com todas as configurações do meio de pagamento */
	private MacadameList<TransactionGroup> processedTransactions;
	
	/** Lista de transações que estão em processo de agrupamento. */
	private MacadameList<TransactionGroup> processingTransactions;
	
	/** Data de inicio para consulta de transações do lote */
	private Calendar startDate;
	
	/** Data final para consulta de transações do lote */
	private Calendar endDate;
	

	/**
	 * Método de recuperação do campo processedTransactions
	 * @return valor do campo processedTransactions
	 */
	public MacadameList<TransactionGroup> getProcessedTransactions() {
		return processedTransactions;
	}
	/**
	 * Valor de processedTransactions atribuído a processedTransactions
	 * @param processedTransactions Atributo da Classe
	 */
	public void setProcessedTransactions(
			MacadameList<TransactionGroup> processedTransactions) {
		this.processedTransactions = processedTransactions;
	}
	/**
	 * Método de recuperação do campo processingTransactions
	 * @return valor do campo processingTransactions
	 */
	public MacadameList<TransactionGroup> getProcessingTransactions() {
		return processingTransactions;
	}
	/**
	 * Valor de processingTransactions atribuído a processingTransactions
	 * @param processingTransactions Atributo da Classe
	 */
	public void setProcessingTransactions(
			MacadameList<TransactionGroup> processingTransactions) {
		this.processingTransactions = processingTransactions;
	}
	/**
	 * Método de recuperação do campo startDate
	 * @return valor do campo startDate
	 */
	public Calendar getStartDate() {
		return startDate;
	}
	/**
	 * Valor de startDate atribuído a startDate
	 * @param startDate Atributo da Classe
	 */
	public void setStartDate(Calendar startDate) {
		this.startDate = startDate;
	}
	/**
	 * Método de recuperação do campo endDate
	 * @return valor do campo endDate
	 */
	public Calendar getEndDate() {
		return endDate;
	}
	/**
	 * Valor de endDate atribuído a endDate
	 * @param endDate Atributo da Classe
	 */
	public void setEndDate(Calendar endDate) {
		this.endDate = endDate;
	}
	
	public void addProcessedTransaction(TransactionGroup batch) {
		if(processedTransactions == null) {
			processedTransactions = Collections.newArrayList();
		}
		
		processedTransactions.add(batch);
	}
	
}
