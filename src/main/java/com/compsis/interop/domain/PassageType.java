package com.compsis.interop.domain;

import com.compsis.macadame.domain.DomainSpecificEntity;

public class PassageType extends DomainSpecificEntity {

	/**
	 * Normal - Valor: NOR
	 */
	public final static String NORMAL = "NOR";
	/**
	 * Evasão - Valor: EVA
	 */
	public final static String EVASION = "EVA";
	/**
	 * Isento - Valor: ISN
	 */
	public final static String EXEMPT = "ISN";
	/**
	 * Marcha ré - Valor: MRE
	 */
	public final static String REVERSE = "MRE";
	/**
	 * Lista nela - Valor: LNL
	 */
	public final static String BLACK_LIST = "LNL";
	/**
	 * Isento de passagem múltipla - Valor: IPM
	 */
	public final static String ISPM = "IPM";
	/**
	 * Tarifa diferenciada - Valor: TDF
	 */
	public final static String DISCOUNT = "TDF";
		
	 /**
	  * Construtor padrão da classe
	  */
	 public PassageType(){}
	 
	 /**
	  * Construtor alternativo da classe
	  * @param aCode
	  */
	 public PassageType(final String aCode){
		 setCode(aCode);
	 }
	
}
