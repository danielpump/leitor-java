/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto SICAT - Interop<br>
 *
 * Data de Criação: 29/05/2013<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.collections.MacadameArrayList;
import com.compsis.macadame.collections.MacadameList;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Classe que representa uma tarefa de transferencia de dados com uma OSA. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 29/05/2013 - @author Naresh Trivedi - Primeiva versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class DataTransferTaskConfig extends RunningInteropTask {
	
	private Integer lengthOfLists;
	
	public Integer getLengthOfLists() {
		return lengthOfLists;
	}

	public void setLengthOfLists(Integer lengthOfLists) {
		this.lengthOfLists = lengthOfLists;
	}

	@SuppressWarnings("unchecked")
	public <T extends ExecutionDetail> MacadameList<T> getDetails() {
		return (MacadameList<T>) super.getRunningTask().getExecutionDetails();
	}
	
	@SuppressWarnings("unchecked")
	public <T extends ExecutionDetail> void setDetails(MacadameList<T> executionDetails) {
		super.getRunningTask().setExecutionDetails((MacadameList<ExecutionDetail>)executionDetails);
	}
	
	public <T extends ExecutionDetail> boolean addDetail(T detail){
		if(getRunningTask().getExecutionDetails() == null){
			setDetails(new MacadameArrayList<T>());
		}
		return getDetails().add(detail);
	}
}
