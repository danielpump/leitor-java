/*
 * COMPSIS - Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto SICAT - Interop <br>
 *
 * Data de Criação: 18/09/2012<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import static com.compsis.interop.domain.TransactionStatus.AWAITING_RETURN;
import static com.compsis.interop.domain.TransactionStatus.REPLACED;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

import com.compsis.image.domain.Image;
import com.compsis.interop.domain.exception.ProtocolMetadataNotFoundException;
import com.compsis.macadame.collections.MacadameArrayList;
import com.compsis.macadame.collections.MacadameList;
import com.compsis.macadame.domain.MacadameEntity;
import com.compsis.macadame.navigator.annotation.Navigable;
import com.compsis.macadame.security.domain.SecurityOperator;
import com.compsis.media.domain.Media;
import com.compsis.media.domain.MediaStatus;
import com.compsis.mop.domain.MeanOfPayment;
import com.compsis.privilege.domain.Privilege;
import com.compsis.protocol.domain.ClearingType;
import com.compsis.protocol.domain.Protocol;
import com.compsis.protocol.domain.ProtocolAttribute;
import com.compsis.protocol.domain.ProtocolMetaData;
import com.compsis.site.domain.POSClassification;
import com.compsis.site.domain.Unit;
import com.comspsis.system.domain.MacadameSystem;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Operação financeira realizada com um gestor de meio de pagamento. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 18/09/2012 - @author Naresh Trivedi - Primeiva versão da classe. <br>
 * 07/12/2012 - @author mscaldaferro - AP-3158. <br>
 * 13/05/2014 - @author vueda - Adicionado sentNumber. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class Transaction extends MacadameEntity {
	private static final Integer INTEGER_TWO = Integer.valueOf(2);
	/**
	 * Atributo que representa o versionamento do registro.
	 * Utilizado para fins de concorrência
	 */
	private int transactionVersion;
	private Long numericTransactionDate;
	private Long numericTransactionDay;
	private Integer quantityImages;
	/** Id da transação do SGAP **/
	private String originalTransactionId;
	private TransactionStatus status;
	private Calendar transactionDate;
	private Calendar transactionDay;
	private BigDecimal value;
	private ReturnCode returnCode;
	private Unit unit;
	private POSClassification posClassification;
	/** Informa se a leitura do tag foi pela antena ou manual **/
	private MediaInputMode mediaInputMode;
	/** Media da operação **/
	private Media media;
	private Media unregisteredMedia;
	private Integer typeMediaRegistry;
	private MediaStatus mediaStatus;
	private Long seqPosTransaction;
	private AbstractInteropConfig interopConfig;
	private MacadameList<ProtocolMetaData> attributes;
	private String note;
	private Batch currentBatch;
	private Clearing clearing;
	private Long idClearing;
	private Long externalCode;
	private MacadameList<TransactionHistory> transactionHistories;
	private TransactionType transactionType;
	private Privilege privilege;
	private MacadameSystem system;
	private Boolean manualInsertion = Boolean.FALSE;
	
	/** Imagens da transação **/
	private MacadameList<Image> images;
	
	private SecurityOperator operator;
	
	/**
	 * Supervisor da Transação 
	 */
	private Long supervisorCode;
	
	/**
	 * Identificador do número de vezes que a Transação foi associada a um lote.
	 * É incrementado a cada vez que for associado um lote para a Transação.
	 * Inicia com o valor zero
	 */
	private Integer sentNumber;
	
	/**
	 * Mostra o tipo de compensação ao qual a transação pertence
	 */
	private ClearingType clearingType;	
	
	/**
	 * Mostra o estado do envia das imagens da transação
	 */
	private ImageSendStatus imageSendStatus;
	
	/** Identificação da transação. */
	private String transactionIdentification;
	
	
	/**
	 * Metodo criado para facilitar integracao com camada view.
	 * Devera retornar sempre a media valida.
	 * Se uma das medias nao for nula, sera retornada como valida neste metodo.
	 * @return
	 */
	public Media getValidMedia(){
		if(getMedia()==null){
			return this.getUnregisteredMedia();
		}
		return getMedia();
	}
	
	/**
	 * Método de recuperação do campo originalTransactionId
	 *
	 * @return valor do campo originalTransactionId
	 */
	public String getOriginalTransactionId() {
		return originalTransactionId;
	}
	/**
	 * Valor de originalTransactionId atribuído a originalTransactionId
	 *
	 * @param originalTransactionId Atributo da Classe
	 */
	public void setOriginalTransactionId(String originalTransactionId) {
		this.originalTransactionId = originalTransactionId;
	}
	/**
	 * Método de recuperação do campo status
	 *
	 * @return valor do campo status
	 */
	public TransactionStatus getStatus() {
		return status;
	}
	/**
	 * Valor de status atribuído a status
	 *
	 * @param status Atributo da Classe
	 */
	public void setStatus(TransactionStatus status) {
		this.status = status;
	}
	/**
	 * Método de recuperação do campo transactionDate
	 *
	 * @return valor do campo transactionDate
	 */
	public Calendar getTransactionDate() {
		return transactionDate;
	}
	/**
	 * Valor de transactionDate atribuído a transactionDate
	 *
	 * @param transactionDate Atributo da Classe
	 */
	public void setTransactionDate(Calendar aTransactionDate) {
		transactionDate = aTransactionDate;
		if(transactionDay==null){
			transactionDay = transactionDate;
		}	
	}
	/**
	 * Método de recuperação do campo value
	 *
	 * @return valor do campo value
	 */
	public BigDecimal getValue() {
		return value;
	}
	/**
	 * Valor de value atribuído a value
	 *
	 * @param value Atributo da Classe
	 */
	public void setValue(BigDecimal value) {
		this.value = value;
	}
	
	/**
	 * Método de recuperação do campo returnCode
	 *
	 * @return valor do campo returnCode
	 */
	public ReturnCode getReturnCode() {
		return returnCode;
	}
	
	/**
	 * Valor de returnCode atribuído a returnCode
	 *
	 * @param returnCode Atributo da Classe
	 */
	public void setReturnCode(ReturnCode returnCode) {
		this.returnCode = returnCode;
	}
	
	/**
	 * Método de recuperação do campo posClassification
	 *
	 * @return valor do campo posClassification
	 */
	public POSClassification getPosClassification() {
		return posClassification;
	}
	
	/**
	 * Valor de posClassification atribuído a posClassification
	 *
	 * @param posClassification Atributo da Classe
	 */
	public void setPosClassification(POSClassification posClassification) {
		this.posClassification = posClassification;
	}

	/**
	 * Método de recuperação do campo unit
	 *
	 * @return valor do campo unit
	 */
	public Unit getUnit() {
		return unit;
	}
	
	/**
	 * Valor de unit atribuído a unit
	 *
	 * @param unit Atributo da Classe
	 */
	public void setUnit(Unit unit) {
		this.unit = unit;
	}
	/**
	 * Método de recuperação do campo mediaInputMode
	 *
	 * @return valor do campo mediaInputMode
	 */
	public MediaInputMode getMediaInputMode() {
		return mediaInputMode;
	}
	
	/**
	 * Valor de mediaInputMode atribuído a mediaInputMode
	 *
	 * @param mediaInputMode Atributo da Classe
	 */
	public void setMediaInputMode(MediaInputMode mediaInputMode) {
		this.mediaInputMode = mediaInputMode;
	}
	/**
	 * Método de recuperação do campo media
	 *
	 * @return valor do campo media
	 */
	public Media getMedia() {
		return media;
	}
	
	/**
	 * Valor de media atribuído a media
	 *
	 * @param media Atributo da Classe
	 */
	public void setMedia(Media media) {
		this.media = media;
	}
	/**
	 * Método de recuperação do campo mediaStatus
	 *
	 * @return valor do campo mediaStatus
	 */
	public MediaStatus getMediaStatus() {
		return mediaStatus;
	}
	/**
	 * Valor de mediaStatus atribuído a mediaStatus
	 *
	 * @param mediaStatus Atributo da Classe
	 */
	public void setMediaStatus(MediaStatus mediaStatus) {
		this.mediaStatus = mediaStatus;
	}
	/**
	 * Método de recuperação do campo seqPosTransaction
	 *
	 * @return valor do campo seqPosTransaction
	 */
	public Long getSeqPosTransaction() {
		return seqPosTransaction;
	}
	
	/**
	 * Valor de seqPosTransaction atribuído a seqPosTransaction
	 *
	 * @param seqPosTransaction Atributo da Classe
	 */
	public void setSeqPosTransaction(Long seqPosTransaction) {
		this.seqPosTransaction = seqPosTransaction;
	}
	/**
	 * Método de recuperação do campo interopConfig
	 *
	 * @return valor do campo interopConfig
	 */
	public AbstractInteropConfig getInteropConfig() {
		return interopConfig;
	}
	
	/**
	 * Método de recuperação do campo meio de pagamento
	 *
	 * @return valor do campo interopConfig
	 */
	public MeanOfPayment getMeanOfPayment() {
		if(interopConfig != null){
			return interopConfig.getMeanOfPayment();
		}
		return null;
	}
	
	/**
	 * Método de recuperação do campo protocolo
	 *
	 * @return valor do campo interopConfig
	 */
	public Protocol getProtocol() {
		if(interopConfig != null){
			return interopConfig.getProtocol();
		}
		return null;
	}
	
	/**
	 * Valor de interopConfig atribuído a interopConfig
	 *
	 * @param interopConfig Atributo da Classe
	 */
	public void setInteropConfig(AbstractInteropConfig interopConfig) {
		this.interopConfig = interopConfig;
	}
	/**
	 * Método de recuperação do campo attributes
	 *
	 * @return valor do campo attributes
	 */
	public MacadameList<ProtocolMetaData> getAttributes() {
		return attributes;
	}
	/**
	 * Valor de attributes atribuído a attributes
	 *
	 * @param attributes Atributo da Classe
	 */
	public void setAttributes(MacadameList<ProtocolMetaData> attributes) {
		this.attributes = attributes;
	}
	/**
	 * Método de recuperação do campo images
	 *
	 * @return valor do campo images
	 */
	public MacadameList<Image> getImages() {
		return images;
	}
	/**
	 * Valor de images atribuído a images
	 *
	 * @param images Atributo da Classe
	 */
	public void setImages(MacadameList<Image> images) {
		this.images = images;
	}
	
	public String getMetaData(final String aKey) {
		if(attributes != null){
			for (ProtocolMetaData metaData : attributes) {
				if (metaData.getAttribute().getKey().equals(aKey)) {
					return metaData.getValue();
				}
			}
		}
		return null;
	}
	
	/** 
	 * Caso não existe o meta dado é lançada uma exceção de {@link NullPointerException}.
	 * Se existir o metadado ao inves dele devolver a string devolve o valor boolean. 
	 * Quando o metadado é 1 o valor é true e quando é 0 o false é false.
	 * @param aKey
	 * @return
	 */
	public Boolean getMetaDataAsBoolean(final String aKey) {
		String metaDataValue = getMetaData(aKey);
		if(metaDataValue == null){			
			throw new ProtocolMetadataNotFoundException(1,
					new StringBuilder("Metadata ").append(aKey).append(" don't exist in passage with id:").append(
					(this.getId() != null ? this.getId().toString() : "Transaction not persisted.")).toString(), null);
		}
		return metaDataValue.equals("1") ? Boolean.TRUE : Boolean.FALSE;  
	}
	
	/**
	 * Adiciona um novo valor de metadados na transaction <br>
	 * Caso já exista uma valor, apenas atualiza
	 * @param key
	 * @param value
	 */
	public void putMetaData(final ProtocolAttribute attribute, String value){
		if(attributes==null){
			attributes = new MacadameArrayList<ProtocolMetaData>();
		}
		boolean exist = false;
		for (ProtocolMetaData protocolMetaData : attributes) {
			if(protocolMetaData.getAttribute().getKey().equals(attribute.getKey())){
				protocolMetaData.setValue(value);
				exist = true;
			}
		}		
		if(!exist){
			final ProtocolMetaData metaData = new ProtocolMetaData();
			metaData.setAttribute(attribute);
			metaData.setValue(value);
			attributes.add(metaData);
		}
	}
	
	/** 
	 * Devolve o registro mais atual do historico das transações,
	 * antes da ultima alteração.
	 * Como o ultimo registro é uma foto do estado atual da transação
	 * foi necesario recuperar o ultimo historico antes do atual.
	 * Caso não exista este historico é retornado o valor null.
	 * @return
	 */
	public TransactionHistory getLastHistory(){
		orderHistoryByDateAsc();
		if(transactionHistories.size() > 1){
			return transactionHistories.get(transactionHistories.size() - INTEGER_TWO);
		}
		return null;
	}
	
	public TransactionHistory getLastManipulation() {
		java.util.Collections.sort(transactionHistories, new Comparator<TransactionHistory>() {
			@Override
			public int compare(TransactionHistory history1, TransactionHistory history2) {					
				return history2.getInsertionDate().compareTo(history1.getInsertionDate());
			}			
		});
		
		for (TransactionHistory history : transactionHistories) {
			if(TransactionStatus.REPLACED.equals(history.getStatus().getCode())){
				return history;
			}
		}
		return null;
	}

	private void orderHistoryByDateAsc() {
	}

	/**
	 * Método de recuperação do campo unregisteredMedia
	 *
	 * @return valor do campo unregisteredMedia
	 */
	public Media getUnregisteredMedia() {
		return unregisteredMedia;
	}

	/**
	 * Valor de unregisteredMedia atribuído a unregisteredMedia
	 *
	 * @param unregisteredMedia Atributo da Classe
	 */
	public void setUnregisteredMedia(Media unregisteredMedia) {
		this.unregisteredMedia = unregisteredMedia;
	}

	/**
	 * Método de recuperação do campo typeMediaRegistry
	 *
	 * @return valor do campo typeMediaRegistry
	 */
	public Integer getTypeMediaRegistry() {
		return typeMediaRegistry;
	}

	/**
	 * Valor de typeMediaRegistry atribuído a typeMediaRegistry
	 *
	 * @param typeMediaRegistry Atributo da Classe
	 */
	public void setTypeMediaRegistry(Integer typeMediaRegistry) {
		this.typeMediaRegistry = typeMediaRegistry;
	}

	/**
	 * Método de recuperação do campo supervisorCode
	 *
	 * @return valor do campo supervisorCode
	 */
	public Long getSupervisorCode() {
		return supervisorCode;
	}

	/**
	 * Valor de supervisorCode atribuído a supervisorCode
	 *
	 * @param supervisorCode Atributo da Classe
	 */
	public void setSupervisorCode(Long supervisorCode) {
		this.supervisorCode = supervisorCode;
	}

	/** 
	 * Criado apenas para mostrar a quantidade de imagens existentes nesta transaction
	 * @return retorno o images.size não é possivel dar set neste atributo pois ele é apenas virtual
	 */
	public Integer getQuantityImages(){
		return quantityImages;
	}
	
	/**
	 * Valor de quantityImages atribuído a quantityImages
	 *
	 * @param quantityImages Atributo da Classe
	 */
	public void setQuantityImages(Integer quantityImages) {
		this.quantityImages = quantityImages;
	}

	/** 
	 * Atribuindo Operator
	 * @param operator
	 */
	public void setOperator(SecurityOperator operator) {
		this.operator = operator;
	}
	
	/** 
     * Método que recupera o operator
	 * @return
	 */
	public SecurityOperator getOperator() {
		return operator;
	}
    
	/**
     * 
     * Método que recupera a observação
     * @return
     */
	public String getNote() {
		return note;
	}

	
	/**
	 * 
	 * Método que recupera a observação
	 * @param note
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/** 
	 * 
	 * Lote corrente associado a transação
	 * Método será removido quando o componente BatchTransmission for criado.
	 */
	@Deprecated
	public Batch getCurrentBatch() {
		return currentBatch;
	}

	/**
	 * 
	 * Atribuindo  lote corrente
	 *  Método será removido quando o componente BatchTransmission for criado.
	 * @param currentBatch
	 */
	@Deprecated
	public void setCurrentBatch(Batch currentBatch) {
		this.currentBatch = currentBatch;
	}

	/**
	 * Método de recuperação do campo seqTransactionInBatch
	 *
	 * @return valor do campo seqTransactionInBatch
	 */
	public Long getExternalCode() {
		return externalCode;
	}

	/**
	 * Valor de seqTransactionInBatch atribuído a seqTransactionInBatch
	 *
	 * @param externalCode Atributo da Classe
	 */
	public void setExternalCode(Long externalCode) {
		this.externalCode = externalCode;
	}

	/**
	 * Método de recuperação do campo transactionHistories
	 *
	 * @return valor do campo transactionHistories
	 */
	@Navigable
	public MacadameList<TransactionHistory> getTransactionHistories() {
		return this.transactionHistories;
	}

	/**
	 * Valor de transactionHistories atribuído a transactionHistories
	 *
	 * @param transactionHistories Atributo da Classe
	 */
	public void setTransactionHistories(
		 MacadameList<TransactionHistory> transactionHistories) {
		this.transactionHistories = transactionHistories;
	}
	/**
	 * @return the transactionType
	 */
	public TransactionType getTransactionType() {
		return transactionType;
	}
	/**
	 * @param transactionType the transactionType to set
	 */
	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	/**
	 * Valor de privilege atribuído a privilege
	 *
	 * @param privilege Atributo da Classe
	 */
	public void setPrivilege(Privilege privilege) {
		this.privilege = privilege;
	}
	
	/**
	 * Método de recuperação do campo privilege
	 *
	 * @return valor do campo privilege
	 */
	public Privilege getPrivilege() {
		return privilege;
	}
	
	/**
	 * Valor de transactionDay atribuído a transactionDay
	 *
	 * @param transactionDay Atributo da Classe
	 */
	public void setTransactionDay(Calendar transactionDay) {
		this.transactionDay = transactionDay;
	}
	
	/**
	 * Método de recuperação do campo transactionDay
	 *
	 * @return valor do campo transactionDay
	 */
	public Calendar getTransactionDay() {
		return transactionDay;
	}
	/**
	 * Método de recuperação do campo transactionVersion
	 *
	 * @return valor do campo transactionVersion
	 */
	public int getTransactionVersion() {
		return transactionVersion;
	}
	/**
	 * Valor de transactionVersion atribuído a transactionVersion
	 *
	 * @param transactionVersion Atributo da Classe
	 */
	public void setTransactionVersion(int transactionVersion) {
		this.transactionVersion = transactionVersion;
	}

	/**
	 * Valor de numericTransactionDate atribuído a numericTransactionDate
	 *
	 * @param numericTransactionDate Atributo da Classe
	 */
	public void setNumericTransactionDate(Long numericTransactionDate) {
		this.numericTransactionDate = numericTransactionDate;
	}
	
	/**
	 * Método de recuperação do campo numericTransactionDate
	 *
	 * @return valor do campo numericTransactionDate
	 */
	public Long getNumericTransactionDate() {
		return numericTransactionDate;
	}
	
	/**
	 * Criado para facilitar o retorno do primeiro TransactionHistory, ou seja, 
	 * aquele cuja data de operação (operation date) mais antiga
	 *  
	 * @return o primeiro TransactionHistory
	 */
	public TransactionHistory getFirstTransactionHistory(){
		
		boolean isMessage = this.getInteropConfig().getProtocol().equals(Protocol.MESSAGE_ARTESP);
		
		if(isMessage) { // Regra para pegar o primeiro do protocolo mensageria
			
			MacadameList<TransactionHistory> aux = new MacadameArrayList<TransactionHistory>();
			
			for(TransactionHistory transactionHistory : transactionHistories) {
				
				if(transactionHistory.getSentNumber().equals(new Integer(1))) {
					
					String status = transactionHistory.getStatus().getCode();
					
					if(status.equals(AWAITING_RETURN) || status.equals(REPLACED)) {
						aux.add(transactionHistory);
					}
				}
			}
			
			orderListByTransactionStatus(aux);
			return aux.firstItem();
			
		} else { // Regra para pegar o primeiro outros protocolos
			orderByOperationDate();
			return transactionHistories.firstItem();
		}
		
	}

	private void orderByOperationDate() {
		Collections.sort(transactionHistories, new Comparator<TransactionHistory>() {					
			public int compare(TransactionHistory first, TransactionHistory second) {				
				return first.getOperationDate().compareTo(second.getOperationDate());
			}
		});
	}
	
	private void orderListByTransactionStatus(MacadameList<TransactionHistory> aux) {
		Collections.sort(aux, new Comparator<TransactionHistory>() {

			@Override
			public int compare(TransactionHistory first, TransactionHistory o2) {
				if(first.getStatus().getCode().equals(AWAITING_RETURN)) {
					return 1;
				}
				
				if(o2.getStatus().getCode().equals(AWAITING_RETURN)) {
					return 1;
				}
				
				return -1;
			}
		});
	}


	@SuppressWarnings("deprecation")
	public Batch getFirstBatchSend(){
		Batch batch = null;
		if(transactionHistories!=null){
			Collections.sort(transactionHistories, new Comparator<TransactionHistory>() {					
				public int compare(TransactionHistory first, TransactionHistory second) {				
					return first.getOperationDate().compareTo(second.getOperationDate());
				}
			});
			for(TransactionHistory history:transactionHistories){
				if( history.getTransaction()!=null &&
					history.getStatus().getCode().equals(TransactionStatus.AWAITING_SUBMISSION)){
					batch =  history.getBatch();
					break;
				}
			}
		}
		return batch;
	}

/**
	 * Valor de sentNumber atribuído a sentNumber
	 *
	 * @param sentNumber Atributo da Classe
	 */
	public void setSentNumber(Integer sentNumber) {
		this.sentNumber = sentNumber;
	}
	
	/**
	 * Método de recuperação do campo sentNumber
	 *
	 * @return valor do campo sentNumber
	 */
	public Integer getSentNumber() {
		return sentNumber;
	}

	/**
	 * Método de recuperação do campo clearing
	 * @return valor do campo clearing
	 */
	public Clearing getClearing() {
		return clearing;
	}

	/**
	 * Valor de clearing atribuído a clearing
	 * @param clearing Atributo da Classe
	 */
	public void setClearing(Clearing clearing) {
		this.clearing = clearing;
		if(this.clearing != null){			
			this.idClearing = clearing.getId();
		}else{
			this.idClearing = null;
		}
	}

	public ClearingType getClearingType() {
		return clearingType;
	}

	public void setClearingType(ClearingType clearingType) {
		this.clearingType = clearingType;
	}

	public ImageSendStatus getImageSendStatus() {
		return imageSendStatus;
	}

	public void setImageSendStatus(ImageSendStatus imageSendStatus) {
		this.imageSendStatus = imageSendStatus;
	}
	
	/**
	 * Método de recuperação do campo transactionHistories
	 *
	 * @return valor do campo transactionHistories
	 */
	public TransactionHistory getTransactionHistory(Integer sentNumber) {
		for (TransactionHistory history : this.transactionHistories) {
			if(history.getSentNumber() != null && history.getSentNumber().equals(sentNumber)){
				return history;
			}
		}
		return null;
	}
	
	/**
	 * Método de recuperação do campo transactionHistories
	 *
	 * @return valor do campo transactionHistories
	 */
	public TransactionHistory getTransactionHistoryOfSentClearing(Integer sentNumber) {
		for (TransactionHistory history : this.transactionHistories) {
			if(history.getSentNumber() != null && history.getSentNumber().equals(sentNumber)){
				if(TransactionStatus.AWAITING_RETURN.equals(history.getStatus().getCode())){
					return history;
				}
			}
		}
		return null;
	}

	/**
	 * Método de recuperação do campo idClearing
	 *
	 * @return valor do campo idClearing
	 */
	public Long getIdClearing() {
		return idClearing;
	}

	/**
	 * Valor de idClearing atribuído a idClearing
	 *
	 * @param idClearing Atributo da Classe
	 */
	public void setIdClearing(Long idClearing) {
		this.idClearing = idClearing;
	}

	/**
	 * Valor de system atribuído a system
	 * @param system Atributo da Classe
	 */
	public void setSystem(MacadameSystem system) {
		this.system = system;
	}
	
	/**
	 * Método de recuperação do campo system
	 * @return valor do campo system
	 */
	public MacadameSystem getSystem() {
		return system;
	}
	
	/**
	 * Valor de manualInsertion atribuído a manualInsertion
	 * @param manualInsertion Atributo da Classe
	 */
	public void setManualInsertion(Boolean manualInsertion) {
		this.manualInsertion = manualInsertion;
	}
	/**
	 * Método de recuperação do campo manualInsertion
	 * @return valor do campo manualInsertion
	 */
	public Boolean getManualInsertion() {
		return manualInsertion;
	}

	/**
	 * Método de recuperação do campo transactionIdentification
	 * @return valor do campo transactionIdentification
	 */
	public String getTransactionIdentification() {
		return transactionIdentification;
	}

	/**
	 * Valor de transactionIdentification atribuído a transactionIdentification
	 * @param transactionIdentification Atributo da Classe
	 */
	public void setTransactionIdentification(String transactionIdentification) {
		this.transactionIdentification = transactionIdentification;
	}

	public Long getNumericTransactionDay() {
		return numericTransactionDay;
	}

	public void setNumericTransactionDay(Long numericTransactionDay) {
		this.numericTransactionDay = numericTransactionDay;
	}
	
	
}
