/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 24/09/2012<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import java.util.Calendar;

import com.compsis.macadame.domain.Filter;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 24/09/2012 - @author ntrivedi - Primeiva versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class ConfigTaskFilter extends Filter {

	ExecutionMode executionMode;
	Calendar date;
	
	/**
	 * Método de recuperação do campo executionMode
	 *
	 * @return valor do campo executionMode
	 */
	public ExecutionMode getExecutionMode() {
		return executionMode;
	}
	/**
	 * Valor de aExecutionMode atribuído a executionMode
	 *
	 * @param aExecutionMode Atributo da Classe
	 */
	public void setExecutionMode(ExecutionMode aExecutionMode) {
		executionMode = aExecutionMode;
	}
	/**
	 * Método de recuperação do campo date
	 *
	 * @return valor do campo date
	 */
	public Calendar getDate() {
		return date;
	}
	/**
	 * Valor de aDate atribuído a date
	 *
	 * @param aDate Atributo da Classe
	 */
	public void setDate(Calendar aDate) {
		date = aDate;
	}
	
}
