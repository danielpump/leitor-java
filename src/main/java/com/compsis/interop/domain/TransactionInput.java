/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto SGAP - Interop<br>
 *
 * Data de Cria��o: 24/09/2012<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Enum para definir o tipo de entrada da transa��o <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 24/09/2012 - @author Lucas Israel - Primeiva vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public enum TransactionInput {
	/** Leitura do TAG foi autom�tica, ou seja, foi informado pela antena **/
	AUTOMATIC,
	/** Leitura do TAG foi manual, ou seja, foi informado manualmente pelo operador **/
	MANUAL;
}
