package com.compsis.interop.domain;

import com.compsis.macadame.navigator.annotation.Navigable;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Classe que representa os dados de execução de tarefa quando esta for relacionada a lotes
 * Não é persistida, usada apenas na visualização de detalhes da execução
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 07/06/2013 - @author Andre Novais - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
@SuppressWarnings("serial")
public class BatchExecutionDetail extends FileExecutionDetail{
	
	
	private Batch batch;
	
	/**
	 * Construtor padrão da classe
	 */
	public BatchExecutionDetail(){ }
	
	/**
	 * Construtor alternativo da classe 
	 * @param aBatch o lote deste detalhe de execução
	 */
	public BatchExecutionDetail(final Batch aBatch){
		this.batch = aBatch;
		batch.setCurrentExecutionDetail(this);
	}
	
	public BatchExecutionDetail(ExecutionDetail detail) {
		super();
		super.setInsertionDate(detail.getInsertionDate());
	    super.setId(detail.getId());
		super.setCodeTask(detail.getCodeTask());
		super.setSequential(detail.getSequential());
		super.setExecutionDate(detail.getExecutionDate());
		super.setGenerateDate(detail.getGenerateDate());
		super.setLaneVersion(detail.getLaneVersion());
		super.setQuantity(detail.getQuantity());
		super.setStatus(detail.getStatus());
		super.setErrorMessage(detail.getErrorMessage());
		super.setLogFile(detail.getLogFile());
	}

	@Navigable
	public Batch getBatch() {
		return batch;
	}

	public void setBatch(Batch batch) {
		this.batch = batch;
	}
}
