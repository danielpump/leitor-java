package com.compsis.interop.domain;

import java.util.Calendar;

import org.aspectj.lang.annotation.Before;

import com.compsis.macadame.collections.Collections;
import com.compsis.macadame.collections.MacadameList;
import com.compsis.macadame.domain.DomainEntity;
import com.compsis.macadame.navigator.annotation.Navigable;
import com.compsis.macadame.security.domain.SecurityOperator;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Representa a execução de uma task de recebimento ou de envio. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 21/05/2013 - @author bcoan - Primeira versão da classe. <br>
 * 22/05/2013 - @author Daniel Ferraz - Atualizado para a versão atual do modelo<br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
@SuppressWarnings("serial")
public class TaskExecution extends DomainEntity {
	
	private SecurityOperator operator;
	
	private InteropTask taskConfig;
			
	/**
	 * Data da execução da tarefa
	 */
	private Calendar executionDate;
	/**
	 * Data de fim da execução da tarefa
	 */
	private Calendar endExecutionDate;
	
	/**
	 * Estado de execução da tarefa.
	 */
	private TaskExecutionStatus status;
	
	private Integer quantityDetail;
	private int quantityDetailFail;
	private int quantityDetailSuccess;
	private int quantityDetailPartialOk;
	
	/**
	 * Lista de registros dos detalhes da execução
	 */
	private MacadameList<ExecutionDetail> executionDetails;

	/** 
	 * Recupera o operador responsável por executar a task
	 * @return operador responsável por executar a task
	 */
	public SecurityOperator getOperator() {
		return operator;
	}

	/** 
	 * 
	 * Atribui o operador responsável por executar a task
	 * @param operator operador responsável por executar a task
	 */
	public void setOperator(SecurityOperator operator) {
		this.operator = operator;
	}

	/** 
	 * Recupera a configuração da task que foi executada 
	 * @return configuração da task que foi executada
	 */
	public InteropTask getTaskConfig() {
		return taskConfig;
	}

	/** 
	 * Atribui a configuração da task que foi executada
	 * @param taskConfig configuraçãos da task que foi executada
	 */
	public void setTaskConfig(InteropTask taskConfig) {
		this.taskConfig = taskConfig;
	}

	/**
	 * Método de recuperação do campo executionDate
	 *
	 * @return valor do campo executionDate
	 */
	public Calendar getExecutionDate() {
		return executionDate;
	}

	/**
	 * Valor de executionDate atribuído a executionDate
	 *
	 * @param executionDate Atributo da Classe
	 */
	public void setExecutionDate(Calendar executionDate) {
		this.executionDate = executionDate;
	}

	/**
	 * Método de recuperação do campo executionDetails <br>
	 * Lista de registros dos detalhes da execução
	 * @return valor do campo executionDetails
	 */
	@Navigable
	public MacadameList<ExecutionDetail> getExecutionDetails() {
		
		if(executionDetails == null) {
			executionDetails = Collections.newArrayList();
		}
		
		return executionDetails;
	}
	
	public void addDetail(final ExecutionDetail executinDetail){
		getExecutionDetails().add(executinDetail);
	}

	/**
	 * Valor de executionDetails atribuído a executionDetails <br>
	 * Lista de registros dos detalhes da execução
	 * @param executionDetails Atributo da Classe
	 */
	public void setExecutionDetails(MacadameList<ExecutionDetail> executionDetails) {
		this.executionDetails = executionDetails;		
	}

	/**
	 * Método de recuperação do campo endExecutionDate<br>
	 * Data de fim da execução da tarefa
	 * @return valor do campo endExecutionDate
	 */
	public Calendar getEndExecutionDate() {
		return endExecutionDate;
	}

	/**
	 * Valor de endExecutionDate atribuído a endExecutionDate <br>
	 * Data de fim da execução da tarefa
	 * @param endExecutionDate Atributo da Classe
	 */
	public void setEndExecutionDate(Calendar endExecutionDate) {
		this.endExecutionDate = endExecutionDate;
	}
	
	/**
	 * Método de recuperação do campo status
	 * @return {@link TaskExecutionStatus}: Estado de execução da tarefa
	 */
	public TaskExecutionStatus getStatus() {
		return status;
	}

	/**
	 * Set do estado de execução da tarefa
	 * @param status - {@link TaskExecutionStatus}:Estado de execução da tarefa
	 */
	public void setStatus(TaskExecutionStatus status) {
		this.status = status;
	}

	public Integer getQuantityDetail() {
		return executionDetails.size();
	}

	public void setQuantityDetail(Integer quantityDetail) {
		this.quantityDetail = quantityDetail;
	}

	/**
	 * Método de recuperação do campo quantityDetailFail
	 *
	 * @return valor do campo quantityDetailFail
	 */
	public int getQuantityDetailFail() {
		return quantityDetailFail;
	}

	/**
	 * Valor de quantityDetailFail atribuído a quantityDetailFail
	 *
	 * @param quantityDetailFail Atributo da Classe
	 */
	public void setQuantityDetailFail(int quantityDetailFail) {
		this.quantityDetailFail = quantityDetailFail;
	}

	/**
	 * Método de recuperação do campo quantityDetailSuccess
	 *
	 * @return valor do campo quantityDetailSuccess
	 */
	public int getQuantityDetailSuccess() {
		return quantityDetailSuccess;
	}

	/**
	 * Valor de quantityDetailSuccess atribuído a quantityDetailSuccess
	 *
	 * @param quantityDetailSuccess Atributo da Classe
	 */
	public void setQuantityDetailSuccess(int quantityDetailSuccess) {
		this.quantityDetailSuccess = quantityDetailSuccess;
	}
	
	public void separateDetailsOnSuccessAndFail(){
		
		int quantityDetailSuccess = 0;
		int quantityDetailFail = 0;
		int quantityDetailPartialOk = 0;
		
		if(executionDetails != null){
			
			for (ExecutionDetail executionDetail : executionDetails) {			
				
				if(executionDetail.getStatus().equals(ExecutionDetail.STATUS_OK)){
					quantityDetailSuccess++;
				} else if(executionDetail.getStatus().equals(ExecutionDetail.STATUS_PARTIAL_OK)) {
					quantityDetailPartialOk++;
				} else {
					quantityDetailFail++;
				}
			}
		}
		
		setQuantityDetailSuccess(quantityDetailSuccess);
		setQuantityDetailFail(quantityDetailFail);
		setQuantityDetailPartialOk(quantityDetailPartialOk);
		
	}

	public int getQuantityDetailPartialOk() {
		return quantityDetailPartialOk;
	}

	public void setQuantityDetailPartialOk(int quantityDetailPartialOk) {
		this.quantityDetailPartialOk = quantityDetailPartialOk;
	}

	
	
}
