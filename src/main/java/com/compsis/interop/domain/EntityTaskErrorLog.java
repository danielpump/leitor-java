/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 23/10/2014<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.domain.MacadameObject;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 23/10/2014 - @author fpereira - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class EntityTaskErrorLog extends MacadameObject {
	
	/**
	 * Atributo que representa Code da Task que ocorreu o erro
	 */
	private String taskCode;
	/**
	 * Atributo que representa a mensagem gerada pela excessão que ocorreu na execução da tarefa
	 */
	private String exceptionMessage;
	
	/**
	 * Representa a identificação da OSA
	 */
	private String osaCode;
	
	/** 
	 * retorna o code da task onde ocorreu o erro
	 * @return
	 */
	public String getTaskCode() {
		return taskCode;
	}
	/** 
	 * seta  o code da task onde ocorreu o erro
	 * @return
	 */
	public void setTaskCode(String taskCode) {
		this.taskCode = taskCode;
	}
	/** 
	 * retorna a mensagem da excessão que ocorreu
	 * @return
	 */
	public String getExceptionMessage() {
		return exceptionMessage;
	}
	/** 
	 *  Metodo para fazer o set da mensagem da excessão que ocorreu
	 * @param exceptionMessage
	 */
	public void setExceptionMessage(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}

	/**
	 * Valor de osaCode atribuído a osaCode
	 * Representa a identificação da OSA
	 * @param osaCode Atributo da Classe
	 */
	public void setOsaCode(String osaCode) {
		this.osaCode = osaCode;
	}
	
	
	/**
	 * Método de recuperação do campo osaCode
	 * Representa a identificação da OSA
	 * @return valor do campo osaCode
	 */
	public String getOsaCode() {
		return osaCode;
	}
	
}
