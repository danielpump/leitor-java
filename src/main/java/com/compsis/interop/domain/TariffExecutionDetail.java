/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * SICAT - Interop<br>
 *
 * Data de Criação: 07/11/2013<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import java.io.File;

import com.compsis.macadame.collections.MacadameArrayList;
import com.compsis.macadame.collections.MacadameList;
import com.compsis.macadame.domain.MacadameEntity;
import com.compsis.macadame.domain.MacadameFile;
import com.compsis.protocol.domain.FileInformation;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * ExecutionDetail para tarefas de tarifas. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 07/11/2013 - @author Vinícius O. Ueda - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class TariffExecutionDetail extends FileExecutionDetail {
	private MacadameList<File> filesToSend;
	private MacadameList<? extends MacadameEntity> entities;
	private FileInformation tafFile;
	private MacadameFile fileWithError;
	private String externalConcessionaire;
	private InteropConversion classificationConverter;
	private InteropConversion vehicleClassConverter;
	
	
	/**
	 * Método de recuperação do campo classificationConverter
	 *
	 * @return valor do campo classificationConverter
	 */
	public InteropConversion getClassificationConverter() {
		return classificationConverter;
	}

	/**
	 * Valor de classificationConverter atribuído a classificationConverter
	 *
	 * @param classificationConverter Atributo da Classe
	 */
	public void setClassificationConverter(InteropConversion classificationConverter) {
		this.classificationConverter = classificationConverter;
	}

	/**
	 * Método de recuperação do campo fileWithError
	 *
	 * @return valor do campo fileWithError
	 */
	public MacadameFile getFileWithError() {
		return fileWithError;
	}

	/**
	 * Valor de fileWithError atribuído a fileWithError
	 *
	 * @param fileWithError Atributo da Classe
	 */
	public void setFileWithError(MacadameFile fileWithError) {
		this.fileWithError = fileWithError;
	}

	/**
	 * Método de recuperação do campo externalConcessionaire
	 *
	 * @return valor do campo externalConcessionaire
	 */
	public String getExternalConcessionaire() {
		return externalConcessionaire;
	}

	/**
	 * Valor de externalConcessionaire atribuído a externalConcessionaire
	 *
	 * @param externalConcessionaire Atributo da Classe
	 */
	public void setExternalConcessionaire(String externalConcessionaire) {
		this.externalConcessionaire = externalConcessionaire;
	}

	/**
	 * Método de recuperação do campo entities
	 *
	 * @return valor do campo entities
	 */
	public MacadameList<? extends MacadameEntity> getEntities() {
		return entities;
	}

	/**
	 * Valor de entities atribuído a entities
	 *
	 * @param entities Atributo da Classe
	 */
	public void setEntities(MacadameList<? extends MacadameEntity> entities) {
		this.entities = entities;
	}

	/**
	 * Método de recuperação do campo tafFile
	 *
	 * @return valor do campo tafFile
	 */
	public FileInformation getTafFile() {
		return tafFile;
	}

	/**
	 * Valor de tafFile atribuído a tafFile
	 *
	 * @param tafFile Atributo da Classe
	 */
	public void setTafFile(FileInformation tafFile) {
		this.tafFile = tafFile;
	}

	/**
	 * Método de recuperação do campo filesToSend
	 *
	 * @return valor do campo filesToSend
	 */
	public MacadameList<File> getFilesToSend() {
		if(filesToSend == null){
			filesToSend = new MacadameArrayList<File>();
		}
		return filesToSend;
	}

	/**
	 * Valor de filesToSend atribuído a filesToSend
	 *
	 * @param filesToSend Atributo da Classe
	 */
	public void setFilesToSend(MacadameList<File> filesToSend) {
		this.filesToSend = filesToSend;
	}
	
	/**
	 * Método de recuperação do campo vehicleClassConverter
	 *
	 * @return valor do campo vehicleClassConverter
	 */
	public InteropConversion getVehicleClassConverter() {
		return vehicleClassConverter;
	}

	/**
	 * Valor de vehicleClassConverter atribuído a vehicleClassConverter
	 *
	 * @param vehicleClassConverter Atributo da Classe
	 */
	public void setVehicleClassConverter(InteropConversion vehicleClassConverter) {
		this.vehicleClassConverter = vehicleClassConverter;
	}

}
