/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 19/09/2012<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.domain.PersistentEnum;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 19/09/2012 - @author ntrivedi - Primeiva vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public enum ExecutionMode implements PersistentEnum{
	AUTOMATIC(0), MANUAL(1), SCHEDULED(2);


	private int id;
	
	
	 ExecutionMode(int id){
		this.id=id;
	}
	
	 /**
	  * 
	  * Retorna o id da Constante
	  * @return
	  */
	@Override
	public Integer getId(){
		return id;
	}
	
}
