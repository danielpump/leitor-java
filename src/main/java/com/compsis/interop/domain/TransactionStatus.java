/*
 * COMPSIS - Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Criação: 17/10/2012<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.domain.DomainSpecificEntity;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * Status da transação na interoperabilidade
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 17/10/2012 - @author ntrivedi - Primeiva versão da classe. <br>
 * 07/01/2013 - @author Lucas Israel - Adicionado constantes para códigos dos status padrões <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
@SuppressWarnings("serial")
public class TransactionStatus extends DomainSpecificEntity {
	
	/** 
	 * Construtor padrão da classe
	 */
	public TransactionStatus() {
	}
	/** 
	 * Construtor alternativo da classe para instancia objetos com o <code>code</code> já especificado
	 */
	public TransactionStatus(final String aCode) {
		super.setCode(aCode);
	}
	
	/**
	 * Em Espera - Valor: ESPR
	 */
	public static final String WATING = "ESPR";

	/**
	 * Aguardando Envio - Valor: AENV
	 */
	public static final String AWAITING_SUBMISSION = "AENV";
	
	/**
	 * Aguardando Retorno - Valor: ARET
	 */
	public static final String AWAITING_RETURN = "ARET"; // este
	
	/**
	 * Aprovada - Valor: APRV
	 */
	public static final String APPROVED = "APRV";
	
	/**
	 * Rejeitada - Valor: RJTD
	 */
	public static final String REJECTED = "RJTD";
	
	/**
	 * Prejuízo - Valor: PRJZ
	 */
	public static final String LOSS = "PRJZ";
	
	/**
	 * Cobrança cancelada - Valor: NCOB
	 */
	public static final String NO_CHARGE = "NCOB";
	
	/**
	 * Substituida - Valor: SUBT
	 */
	public static final String REPLACED = "SUBT"; // ou este
	
	/**
	 * Paga - Valor: PAGA
	 * Este status está sendo utilizado pelo recebimento de quitação e ETP como 'Pago após passagem'
	 * Está traduzido no TMCM_MESSAGEVALUE e sendo exibido no filtro de manipulação de transação como 'Pago após passagem'
	 */
	public static final String PAID = "PAGA";
	
	/**
	 * Paga após passagem - Valor: PAPS
	 * Aparentemente este status não está sendo utilizado (olhar status PAGA)
	 * (com exceção de uma classe de teste unitário #MarkTransactionStatusPaidAfterPassageActivityTest)
	 */
	public static final String PAID_AFTER_PASSAGE = "PAPS";
	
	/**
	 * Aprovada com outro valor (Compensado outro valor) - Valor: PGVD
	 */
	public static final String PAID_OTHER_VALUE = "PGVD";
	
	/**
	 * Provisionado - Valor: PROV
	 */
	public static final String PROVISIONED = "PROV";
}
