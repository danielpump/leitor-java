/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto ${product_name} - ${product_description}<br>
 *
 * Data de Criação: 30/09/2014<br>
 * <br>
 * Todos os direitos reservados.
 */

package com.compsis.interop.domain;

import com.compsis.macadame.domain.DomainSpecificEntity;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 30/09/2014 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("serial")
public class TransactionTemporary extends DomainSpecificEntity {
	private String plate;
	private String tag;
	/**
	 * @return the tag
	 */
	public String getTag() {
		return tag;
	}
	/**
	 * @param tag the tag to set
	 */
	public void setTag(String tag) {
		this.tag = tag;
	}
	/**
	 * @return the plate
	 */
	public String getPlate() {
		return plate;
	}
	/**
	 * @param plate the plate to set
	 */
	public void setPlate(String plate) {
		this.plate = plate;
	}
}
